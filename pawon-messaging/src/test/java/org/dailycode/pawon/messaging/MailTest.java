/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.messaging;

import org.dailycode.pawon.core.entity.Purchase;
import org.dailycode.pawon.messaging.entity.Mail;
import org.dailycode.pawon.messaging.message.BackOffice;
import org.dailycode.pawon.messaging.message.FrontDesk;
import org.dailycode.pawon.messaging.message.PurchaseMessage;
import org.dailycode.pawon.messaging.message.service.PurchaseMessageService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author kris
 * @created Mar 18, 2013
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:message-context.xml"})
public class MailTest {

    @Autowired
    private FrontDesk frontDesk;
    @Autowired
    private BackOffice backOffice;
    @Autowired
    private PurchaseMessage purchaseMessage;

    @Test
    public void test() {
        Mail mail = new Mail();
        mail.setMailId("TEST MAIL");
        mail.setCountry("INDONESIA");
        mail.setWeight(2);
        frontDesk.sendMail(mail);

        System.out.println(frontDesk.receiveMail());
    }

    @Test
    public void test1() {
        Purchase purchase = new Purchase();
        purchaseMessage.sendPurchase(2l, 2l, null);
        System.out.println(purchaseMessage.receivePurchase());
    }
}
