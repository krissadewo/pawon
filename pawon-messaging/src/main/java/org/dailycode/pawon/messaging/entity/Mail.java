/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.messaging.entity;

/**
 *
 * @author kris
 * @created Mar 18, 2013
 */
public class Mail {

    private String mailId;
    private String country;
    private double weight;

    public String getMailId() {
        return mailId;
    }

    public void setMailId(String mailId) {
        this.mailId = mailId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Mail{" + "mailId=" + mailId + ", country=" + country + ", weight=" + weight + '}';
    }
}
