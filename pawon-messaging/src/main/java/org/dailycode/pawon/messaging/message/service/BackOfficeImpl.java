/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.messaging.message.service;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import org.dailycode.pawon.messaging.entity.Mail;
import org.dailycode.pawon.messaging.message.BackOffice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.support.JmsGatewaySupport;
import org.springframework.jms.support.JmsUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kris
 * @created Mar 18, 2013
 */
@Component
public class BackOfficeImpl implements BackOffice {

    @Autowired
    private JmsTemplate jmsTemplate;

    @Transactional
    public Mail receiveMail() {
        MapMessage message = (MapMessage) jmsTemplate.receive();
        try {
            if (message == null) {
                return null;
            }
            Mail mail = new Mail();
            mail.setMailId(message.getString("mailId"));
            mail.setCountry(message.getString("country"));
            mail.setWeight(message.getDouble("weight"));
            return mail;
        } catch (Exception e) {
           
        }
        return null;
    }
}
