/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.messaging.message;

import java.util.List;
import org.dailycode.pawon.core.entity.Purchase;

/**
 *
 * @author kris
 * @created Mar 18, 2013
 */
public interface PurchaseMessage {

    void sendPurchase(final Long itemId, final Long tableId, final List<Long> optionId);

    Purchase receivePurchase();
}
