/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.messaging.message.service;

import java.util.List;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.Session;
import org.dailycode.pawon.core.entity.Purchase;
import org.dailycode.pawon.messaging.message.PurchaseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kris
 * @created Mar 18, 2013
 */
@Component
public class PurchaseMessageService implements PurchaseMessage {

    @Autowired
    private JmsTemplate jmsTemplate;

    @Override
    @Transactional
    public Purchase receivePurchase() {
        try {
            MapMessage message = (MapMessage) jmsTemplate.receive();
            if (message == null) {
                return null;
            }
            Purchase purchase = new Purchase();
            purchase.setId(message.getLong("itemId"));
            return purchase;
        } catch (JMSException ex) {
            System.out.println("Error : " + ex.getMessage());
        }
        return null;
    }

    @Override
    @Transactional
    public void sendPurchase(final Long itemId, final Long tableId, final List<Long> optionId) {
        jmsTemplate.send(new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                MapMessage message = session.createMapMessage();
                message.setLong("itemId", itemId);
                message.setLong("tableId", tableId);
                message.setObject("optionId", optionId);
                System.out.println("send message : " + message.toString());
                return message;
            }
        });
    }
}
