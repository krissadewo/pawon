package org.dailycode.pawon.messaging;

import org.dailycode.pawon.messaging.entity.Mail;
import org.dailycode.pawon.messaging.message.BackOffice;
import org.dailycode.pawon.messaging.message.FrontDesk;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("message-context.xml");
        FrontDesk frontDesk = context.getBean(FrontDesk.class);
        Mail mail = new Mail();
        mail.setMailId("TEST MAIL");
        mail.setCountry("INDONESIA");
        mail.setWeight(2);
        frontDesk.sendMail(mail);

        BackOffice backOffice = context.getBean(BackOffice.class);
        System.out.println(backOffice.receiveMail().toString());
    }
}
