/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.messaging.message;

import org.dailycode.pawon.messaging.entity.Mail;

/**
 *
 * @author kris
 * @created Mar 18, 2013
 */
public interface BackOffice {

    public Mail receiveMail();
}
