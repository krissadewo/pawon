/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.rest.controller;

import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.dailycode.pawon.backend.rest.AppRest;
import org.dailycode.pawon.core.dao.service.BaseService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author kris
 * @created Mar 22, 2013
 */
@Controller
@RequestMapping("/room")
public class RoomRest extends BaseService {

    @RequestMapping(value = "/roomName/{roomName}", method = RequestMethod.GET)
    public Map<String, Object> getPurchaseByRoom(@PathVariable String roomName) {
        return AppRest.model(getRoomService().getByName(roomName), String.valueOf(HttpServletResponse.SC_OK));
    }
}
