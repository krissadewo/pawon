/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import org.joda.time.DateMidnight;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.PieChartModel;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author kris
 */
@Controller("purchaseReport")
@Scope("view")
public class PurchaseReportController extends BaseController implements Serializable {

    private String chartModel = "barChart";
    private Date startDate = new DateMidnight().minusDays(6).toDate();
    private Date endDate = new DateMidnight().toDate();
    private CartesianChartModel barModel;
    private PieChartModel pieModel;
    private boolean pieChartRendered;
    private boolean barChartRendered = true;
    private Date maxDate = new DateMidnight().toDate();
    private String reportType;

    @Override
    public void init() {
        createBarChart();
        createPieChart();
    }

    public void changeStartDate() {
        if (getChartModel() != null) {
            if (getChartModel().equals("pieChart")) {
                createPieChart();
            } else {
                createBarChart();
            }
        }
    }

    public void changeEndDate() {
        if (getChartModel() != null) {
            if (getChartModel().equals("pieChart")) {
                createPieChart();
            } else {
                createBarChart();
            }
        }
    }

    public void changeReportType() {
        // createBarChart();
    }

    public void changeChartModel() {
        //barChart = (BarChart) FacesContext.getCurrentInstance().getViewRoot().findComponent("reportForm:panelPieChart");
        //pieChartRetain = (PieChart) FacesContext.getCurrentInstance().getViewRoot().findComponent("reportForm:panelBarChart");

        if (getChartModel().equals("pieChart")) {
            setBarChartRendered(false);
            setPieChartRendered(true);
        } else {
            setBarChartRendered(true);
            setPieChartRendered(false);
        }
    }

    public CartesianChartModel createBarChart() {
        barModel = new CartesianChartModel();
        ChartSeries chartSeries = new ChartSeries();
        chartSeries.setLabel("Purchase Item");

        for (Object object : getService().getReportService().getPurchaseReport(startDate, endDate)) {
            Map map = (Map) object;
            Double priceOption = 0d;
            if (map.get("total_purchase_option") != null) {
                priceOption = ((BigDecimal) map.get("total_purchase_option")).doubleValue();
            }

            chartSeries.set(map.get("date_purchase").toString().substring(0, 5),
                    ((Double) map.get("total_purchase_item") + priceOption));
        }
        if (chartSeries.getData().isEmpty()) {
            chartSeries.set("no data on this current time", 0);
        }

        barModel.addSeries(chartSeries);
        return barModel;
    }

    public PieChartModel createPieChart() {
        pieModel = new PieChartModel();
        for (Object object : getService().getReportService().getPurchaseReport(startDate, endDate)) {
            Map map = (Map) object;
            Double priceOption = 0d;
            if (map.get("total_purchase_option") != null) {
                priceOption = ((BigDecimal) map.get("total_purchase_option")).doubleValue();
            }
            pieModel.set(map.get("date_purchase").toString(), (Number) ((Double) map.get("total_purchase_item") + priceOption));
        }
        if (pieModel.getData().isEmpty()) {
            pieModel.set("no data on this current time", 0);
        }
        return pieModel;
    }

    public String getChartModel() {
        return chartModel;
    }

    public void setChartModel(String chartModel) {
        this.chartModel = chartModel;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public CartesianChartModel getBarModel() {
        return barModel;
    }

    public void setBarModel(CartesianChartModel barModel) {
        this.barModel = barModel;
    }

    public boolean isPieChartRendered() {
        return pieChartRendered;
    }

    public void setPieChartRendered(boolean pieChartRendered) {
        this.pieChartRendered = pieChartRendered;
    }

    public boolean isBarChartRendered() {
        return barChartRendered;
    }

    public void setBarChartRendered(boolean barChartRendered) {
        this.barChartRendered = barChartRendered;
    }

    public Date getMaxDate() {
        return maxDate;
    }

    public void setMaxDate(Date maxDate) {
        this.maxDate = maxDate;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public PieChartModel getPieModel() {
        return pieModel;
    }

    public void setPieModel(PieChartModel pieModel) {
        this.pieModel = pieModel;
    }
}
