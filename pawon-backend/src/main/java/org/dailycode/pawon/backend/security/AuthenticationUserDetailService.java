package org.dailycode.pawon.backend.security;

import org.dailycode.pawon.core.dao.service.UserService;
import org.dailycode.pawon.core.entity.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 *
 * @author kris
 */
@Service
public class AuthenticationUserDetailService implements UserDetailsService {

    @Autowired
    private UserService userService;
    
    @Override
    public UserDetails loadUserByUsername(String username) {
        Users users = userService.findByUsername(username);

        if (users == null) {
            throw new UsernameNotFoundException("User not found !");
        }
        return new AuthenticationUserDetails(users);
    }
}
