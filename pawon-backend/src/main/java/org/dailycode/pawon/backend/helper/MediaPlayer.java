/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.helper;

import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.media.Manager;
import javax.media.NoPlayerException;
import javax.media.Player;
import org.dailycode.pawon.backend.AppBackend;
import org.dailycode.pawon.core.AppCore;

/**
 *
 * @author kris
 * @created Mar 19, 2013
 */
public class MediaPlayer {

    private static MediaPlayer instance;

    public MediaPlayer() {
    }

    public static MediaPlayer getInstance() {
        if (instance == null) {
            instance = new MediaPlayer();
        }
        return instance;
    }

    public void playMedia(URL url) {
        Player player;
        try {
            player = Manager.createPlayer(url);
            player.start();
        } catch (IOException | NoPlayerException ex) {
            AppBackend.getLogger(this).error(ex);
        }
    }
}
