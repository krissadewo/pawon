/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.controller;

import org.dailycode.pawon.backend.AppBackend;
import org.dailycode.pawon.datamodel.LazyItemDataModel;
import org.dailycode.pawon.backend.listener.ActionListListener;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.dailycode.pawon.core.common.Section;
import org.dailycode.pawon.core.entity.Item;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author Kris Sadewo
 * @date Feb 8, 2013
 */
@Controller("itemList")
@Scope("view")
public class ItemListController extends BaseController implements ActionListListener, Serializable {

    private Section purchaseSection;
    private List<String> purchaseSections = new ArrayList<>();
    private LazyDataModel<Item> dataModel;
    private List<Item> selectedItems;
    private String searchCodeKey;
    private String searchNameKey;

    @Override
    public void init() {
        purchaseSections = createdSections();
        purchaseSection = getCurrentSection();
        if (purchaseSection == null) {
            purchaseSection = Section.RESTAURANT;
        }
        changeSelectedPurchaseSections();
    }

    @Override
    public void actionForm() throws IOException {
        redirectView("item_form", AppBackend.SN_ENTITY, true);
    }

    @Override
    public void actionEdit() throws IOException {
        if (selectedItems.size() == 1) {
            Item item = selectedItems.get(0);
            redirectView("item_form", AppBackend.SN_ENTITY, item);
        } else if (getSelectedItems().size() > 1) {
            showMessageEditMoreThanOne();
        } else if (getSelectedItems().isEmpty()) {
            showMessageEditEmpty();
        }
    }

    @Override
    public void actionDelete() throws IOException {
        if (selectedItems.size() > 0) {
            getService().getItemService().delete(selectedItems);
        } else {
            showMessageDeleteEmpty();
        }
    }

    @Override
    public void actionSearch() throws IOException {
        Item item = new Item();
        item.setName(searchNameKey);

        Map<String, String> filters = new HashMap<>();
        filters.put("code", searchCodeKey);
        filters.put("name", searchNameKey);

        DataTable dataTable = (DataTable) getFacesContext().getViewRoot().findComponent("form:dataTable");
        dataTable.setFilters(filters);

        RequestContext.getCurrentInstance().update("form:dataTable");
    }

    public void changeSelectedPurchaseSections() {
        AppBackend.getInstance().setSessionMapValue(AppBackend.SN_SECTION, purchaseSection);
        dataModel = new LazyItemDataModel(getService().getItemService());
    }

    public Section getPurchaseSection() {
        return purchaseSection;
    }

    public void setPurchaseSection(Section purchaseSection) {
        this.purchaseSection = purchaseSection;
    }

    public LazyDataModel<Item> getDataModel() {
        return dataModel;
    }

    public void setDataModel(LazyDataModel<Item> dataModel) {
        this.dataModel = dataModel;
    }

    public List<Item> getSelectedItems() {
        return selectedItems;
    }

    public void setSelectedItems(List<Item> selectedItems) {
        this.selectedItems = selectedItems;
    }

    public String getSearchCodeKey() {
        return searchCodeKey;
    }

    public void setSearchCodeKey(String searchCodeKey) {
        this.searchCodeKey = searchCodeKey;
    }

    public String getSearchNameKey() {
        return searchNameKey;
    }

    public void setSearchNameKey(String searchNameKey) {
        this.searchNameKey = searchNameKey;
    }

    public List<String> getPurchaseSections() {
        return purchaseSections;
    }
}
