/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.controller;

import org.dailycode.pawon.backend.AppBackend;
import org.dailycode.pawon.backend.listener.ActionFormListener;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.event.PhaseId;
import org.dailycode.pawon.core.common.Section;
import org.dailycode.pawon.core.entity.ItemCategory;
import org.dailycode.pawon.core.entity.Language;
import org.dailycode.pawon.core.entity.LocalizedInformation;
import org.dailycode.pawon.core.helper.ImageHandler;
import org.primefaces.component.editor.Editor;
import org.primefaces.component.panelgrid.PanelGrid;
import org.primefaces.component.tabview.Tab;
import org.primefaces.component.tabview.TabView;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.primefaces.model.UploadedFile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author kris
 */
@Controller("itemCategoryForm")
@Scope("view")
public class ItemCategoryFormController extends BaseController implements ActionFormListener, Serializable {

    private ItemCategory itemCategory;
    private ItemCategory parentItemCategory;
    private TabView tabView;
    private TreeNode root;
    private TreeNode selectedNode;
    private HtmlInputText inputTextTitle;
    private HtmlOutputText outputTextTitle;
    private UploadedFile uploadedImage;
    private UploadedFile uploadedLogo;
    private Editor editor;
    private List<Language> languages = new ArrayList<>();
    private Long currentParentCategoryId;
    private Section purchaseSection;

    @Override
    public void init() {
        languages = getService().getLanguageService().getActiveLanguage();
        purchaseSection = getCurrentSection();

        //Set default item category
        itemCategory = (ItemCategory) getCurrentEntity();
        if (itemCategory == null) {
            itemCategory = new ItemCategory();
            itemCategory.setOrderNumber(1);
        } else {
            parentItemCategory = getService().getItemCategoryService().getById(itemCategory.getParentId());
        }

        currentParentCategoryId = itemCategory.getParentId();

        createDynamicTabView();

        if (getFacesContext().getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            createTree();
        }
    }

    @Override
    public void actionCancel() throws IOException {
        redirectView("category_list", AppBackend.SN_ENTITY, true);
    }

    @Override
    public void actionSave() throws IOException {
        if (uploadedImage != null) {
            itemCategory.setImage(new Date().getTime() + "_" + uploadedImage.getFileName());
            itemCategory.setImageData(uploadedImage.getContents());
        }

        if (uploadedLogo != null) {
            itemCategory.setLogo(new Date().getTime() + "_" + uploadedLogo.getFileName());
            itemCategory.setLogoData(uploadedLogo.getContents());
        }

        itemCategory.setSection(getCurrentSection());

        for (Language language : languages) {
            inputTextTitle = (HtmlInputText) getFacesContext().getViewRoot().findComponent("form:tabView:inputTextTitle" + language.getId());
            editor = (Editor) getFacesContext().getViewRoot().findComponent("form:tabView:editor" + language.getId());

            LocalizedInformation localizedInformation = getService().getLocalizedInformationService().getByItemCategoryAndLanguage(itemCategory, language);
            if (localizedInformation == null) {
                localizedInformation = new LocalizedInformation();
            }

            if (inputTextTitle.getValue() != null) {
                localizedInformation.setTitle(inputTextTitle.getValue().toString());
            }

            if (editor.getValue() != null) {
                localizedInformation.setDescription(editor.getValue().toString());
            }

            localizedInformation.setItemCategory(itemCategory);
            localizedInformation.setLanguage(language);
            itemCategory.getLocalizedInformations().add(localizedInformation);
        }

        if (parentItemCategory != null) {
            itemCategory.setParentId(parentItemCategory.getId());
        }

        if (getService().getItemCategoryService().saveOrUpdate(itemCategory) != null) {
            redirectView("category_list", AppBackend.SN_ENTITY, true);
        } else {
            showGeneralErrorMessage();
        }
    }

    private void createTree() {
        root = new DefaultTreeNode("root", null);

        List<ItemCategory> itemCategorys = getService().getItemCategoryService().getBySection(purchaseSection);
        for (ItemCategory ic : itemCategorys) {
            if (ic.getParentId() == 0) {
                createTree(ic, root);
            }
        }
    }

    private TreeNode createTree(ItemCategory pItemCategory, TreeNode rootNode) {
        TreeNode parentNode = new DefaultTreeNode(pItemCategory, rootNode);
        parentNode.setExpanded(true);

        for (ItemCategory parent : getService().getItemCategoryService().getByParent(pItemCategory.getId())) {
            TreeNode childNode = createTree(parent, parentNode);
            childNode.setExpanded(true);

            if (parent.getParentId() == currentParentCategoryId) {
                parentNode.setSelected(true);
            }
        }
        return parentNode;
    }

    private void createDynamicTabView() {
        // tabView = (TabView) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:tabView");
        tabView = new TabView();
        for (Language language : languages) {
            LocalizedInformation information = getService().getLocalizedInformationService().getByItemCategoryAndLanguage(itemCategory, language);

            Tab tab = new Tab();
            tab.setId("tab" + language.getId());
            tab.setTitle(language.getName());

            outputTextTitle = new HtmlOutputText();
            outputTextTitle.setId("output" + language.getId());
            outputTextTitle.setValue("Title :");

            inputTextTitle = new HtmlInputText();
            inputTextTitle.setId("inputTextTitle" + language.getId());
            inputTextTitle.setStyle("width:50%");

            editor = new Editor();
            editor.setId("editor" + language.getId());
            editor.setWidth(700);

            if (information != null) {
                inputTextTitle.setValue(information.getTitle());
                editor.setValue(information.getDescription());
            }

            PanelGrid panelGrid = new PanelGrid();
            panelGrid.setId("grid" + language.getId());
            panelGrid.setStyle("border-style: hidden !important;");
            panelGrid.setColumns(1);
            panelGrid.getChildren().add(outputTextTitle);
            panelGrid.getChildren().add(inputTextTitle);
            panelGrid.getChildren().add(editor);

            tab.getChildren().add(panelGrid);

            tabView.getChildren().add(tab);
        }
    }

    public void onNodeSelect(NodeSelectEvent event) {
        parentItemCategory = (ItemCategory) selectedNode.getData();
        if (parentItemCategory.getId() != currentParentCategoryId) {
            itemCategory.setOrderNumber(suggestOrderNumber(getService().getItemCategoryService().getByParentAndSection(parentItemCategory.getId(), purchaseSection)));
        } else {
            itemCategory.setOrderNumber(getService().getItemCategoryService().getById(itemCategory.getId()).getOrderNumber());
        }
    }

    private int suggestOrderNumber(List<ItemCategory> categorys) {
        if (categorys.isEmpty()) {
            return 1;
        }
        return categorys.size() + 1;
    }

    public TreeNode getRoot() {
        return root;
    }

    public void deleteImage() {
        ImageHandler.deleteImageOnServer(getService().getSystemParameterService().getImageSource(), itemCategory.getImage());
        itemCategory.setImage(null);
    }

    public void deleteLogo() {
        ImageHandler.deleteImageOnServer(getService().getSystemParameterService().getImageSource(), itemCategory.getLogo());
        itemCategory.setLogo(null);
    }

    public TabView getTabView() {
        return tabView;
    }

    public void setTabView(TabView tabView) {
        this.tabView = tabView;
    }

    public ItemCategory getItemCategory() {
        return itemCategory;
    }

    public void setItemCategory(ItemCategory itemCategory) {
        this.itemCategory = itemCategory;
    }

    public UploadedFile getUploadedImage() {
        return uploadedImage;
    }

    public void setUploadedImage(UploadedFile uploadedImage) {
        this.uploadedImage = uploadedImage;
    }

    public UploadedFile getUploadedLogo() {
        return uploadedLogo;
    }

    public void setUploadedLogo(UploadedFile uploadedLogo) {
        this.uploadedLogo = uploadedLogo;
    }

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    public TreeNode getSelectedNode() {
        for (int i = 0; i < root.getChildren().size(); i++) {
            removeDefaultSelectedTree(root.getChildren().get(i));
        }
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public ItemCategory getParentItemCategory() {
        return parentItemCategory;
    }

    public void setParentItemCategory(ItemCategory parentItemCategory) {
        this.parentItemCategory = parentItemCategory;
    }

    public ItemCategory getCurrentItemCategory() {
        return getService().getItemCategoryService().getOrganizedParent(purchaseSection, itemCategory, true);
    }
}
