/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.rest.controller;

import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.dailycode.pawon.backend.rest.AppRest;
import org.dailycode.pawon.core.common.Section;
import org.dailycode.pawon.core.dao.service.BaseService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author kris
 * @created Mar 12, 2013
 */
@Controller
@RequestMapping("/itemCategory")
public class ItemCategoryRest extends BaseService {

    @RequestMapping(value = "/section/{section}/parentId/{parentId}", method = RequestMethod.GET)
    public Map<String, Object> getByParentAndSection(@PathVariable String section, @PathVariable Long parentId) {
        return AppRest.model(getItemCategoryService().getByParentAndSection(parentId, Section.getSection(section)), String.valueOf(HttpServletResponse.SC_OK));
    }

    @RequestMapping(value = "/section/{section}/languageId/{languageId}", method = RequestMethod.GET)
    public Map<String, Object> getBySection(@PathVariable String section, @PathVariable Long languageId) {
        return AppRest.model(getItemCategoryService().getBySection(Section.getSection(section), languageId), String.valueOf(HttpServletResponse.SC_OK));
    }

    @RequestMapping(value = "/id/{id}", method = RequestMethod.GET)
    public Map<String, Object> getById(@PathVariable Long id) {
        return AppRest.model(getItemCategoryService().getById(id), String.valueOf(HttpServletResponse.SC_OK));
    }

    @RequestMapping(value = "/parentId/{id}/languageId/{languageId}", method = RequestMethod.GET)
    public Map<String, Object> getByParent(@PathVariable Long id, @PathVariable Long languageId) {
        return AppRest.model(getItemCategoryService().getByParent(id, languageId), String.valueOf(HttpServletResponse.SC_OK));
    }

    @RequestMapping(value = "/hasChild/{id}", method = RequestMethod.GET)
    public Map<String, Object> hasChild(@PathVariable Long id) {
        return AppRest.model(getItemCategoryService().hasChild(id), String.valueOf(HttpServletResponse.SC_OK));
    }
}
