/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.dailycode.pawon.core.entity.Galleria;
import org.primefaces.component.poll.Poll;
import org.primefaces.context.RequestContext;
import org.primefaces.push.PushContext;
import org.primefaces.push.PushContextFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author kris
 */
@Controller("playerController")
@Scope("view")
public class PlayerController extends BaseController implements Serializable {

    private int count = 0;
    private int interval;
    private Poll pollGalleria;
    private Poll pollMessage;
    private String mediaUrl;
    private Galleria selectedGalleria;
    private String message;
    private String image;
    private List<Galleria> gallerias = new ArrayList<>();

    @Override
    public void init() {
        gallerias = getService().getGalleriaService().getAll(true);
        mediaUrl = getApplicationMediaURL();
    }

    public synchronized void showGalleria() {
        System.out.println("show Galleria");
        pollGalleria = (Poll) getFacesContext().getViewRoot().findComponent("formGalleria:pollGalleria");

        if (count >= gallerias.size()) {
            gallerias = getService().getGalleriaService().getAll(true);
            count = 0;
        }

        selectedGalleria = gallerias.get(count);
        count++;

        pollGalleria.setInterval(selectedGalleria.getInterval());

        PushContext context = PushContextFactory.getDefault().getPushContext();
        context.push("/player", mediaUrl + selectedGalleria.getImage());
        // context.push("/text", selectedPromo.getRunningText());

        //poll = new Poll();

        //poll.setAutoStart(true);
        System.out.println(selectedGalleria.toString());
        RequestContext.getCurrentInstance().update("formGalleria");
    }

    public String getMessage() {
        System.out.println("update message");
        return getService().getTickerService().getTicker().getMessage();
    }

    public String getImage() {
       
        if (count >= gallerias.size()) {
            gallerias = getService().getGalleriaService().getAll(true);
            count = 0;
        }

        selectedGalleria = gallerias.get(count);
        count++;
        interval = selectedGalleria.getInterval();

        //System.out.println(selectedGalleria.toString());

        return mediaUrl + selectedGalleria.getImage();
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public Galleria getSelectedGalleria() {
        return selectedGalleria;
    }

    public void setSelectedGalleria(Galleria selectedGalleria) {
        this.selectedGalleria = selectedGalleria;
    }

    public Poll getPollGalleria() {
        return pollGalleria;
    }

    public void setPollGalleria(Poll pollGalleria) {
        this.pollGalleria = pollGalleria;
    }

    public Poll getPollMessage() {
        return pollMessage;
    }

    public void setPollMessage(Poll pollMessage) {
        this.pollMessage = pollMessage;
    }
}
