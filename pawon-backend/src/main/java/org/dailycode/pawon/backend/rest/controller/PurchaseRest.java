/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.rest.controller;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.dailycode.pawon.backend.AppBackend;
import org.dailycode.pawon.backend.helper.MediaPlayer;
import org.dailycode.pawon.backend.rest.AppRest;
import org.dailycode.pawon.core.common.PurchaseStatus;
import org.dailycode.pawon.core.dao.service.BaseService;
import org.dailycode.pawon.core.entity.Item;
import org.dailycode.pawon.core.entity.Purchase;
import org.dailycode.pawon.core.entity.Room;
import org.primefaces.push.PushContext;
import org.primefaces.push.PushContextFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author kris
 * @created Mar 20, 2013
 */
@Controller
@RequestMapping("/purchase")
public class PurchaseRest extends BaseService {

    @RequestMapping(value = "/roomName/{roomName}", method = RequestMethod.GET)
    public Map<String, Object> processPurchase(@PathVariable String roomName, HttpServletRequest request) throws MalformedURLException {

        Purchase purchase = getPurchaseService().getByRoom(roomName, PurchaseStatus.ORDERED);
        if (purchase != null) {
            purchase.setPurchaseStatus(PurchaseStatus.PROCESS);
            if (getPurchaseService().saveOrUpdate(purchase) != null) {
                PushContext context = PushContextFactory.getDefault().getPushContext();
                context.push("/notifications", new FacesMessage("Order Receive", "From table " + roomName));
                AppBackend.getLogger(this).info("Purchase receive from " + roomName + " Send notification to client");
//                URL url = new URL("http", request.getServerName(), request.getServerPort(), request.getContextPath() + "/media/schoolbell.wav");
//                MediaPlayer.getInstance().playMedia(url);
                return AppRest.model(true, String.valueOf(HttpServletResponse.SC_OK));
            }
        }

        return AppRest.model(false, String.valueOf(HttpServletResponse.SC_OK));
    }

    /**
     * Order purchase one by one<br>
     *
     * @param itemId
     * @param options
     * @param roomName
     * @param request
     * @return
     * @throws MalformedURLException
     */
    @RequestMapping(value = "/itemId/{itemId}/options/{options}/roomName/{roomName}/languageId/{languageId}", method = RequestMethod.GET)
    public Map<String, Object> orderPurchase(@PathVariable Long itemId, @PathVariable String[] options, @PathVariable String roomName, @PathVariable Long languageId, HttpServletRequest request) throws MalformedURLException {
        Item item = getItemService().getById(itemId, languageId);
        Room room = getRoomService().getByName(roomName);

        if (getPurchaseService().saveOrUpdate(item, room, options, languageId) != null) {
            return AppRest.model(true, String.valueOf(HttpServletResponse.SC_OK));
        }

        return AppRest.model(false, String.valueOf(HttpServletResponse.SC_OK));
    }

    @RequestMapping(value = "/show/roomName/{roomName}", method = RequestMethod.GET)
    public Map<String, Object> getPurchaseByRoom(@PathVariable String roomName) {
        return AppRest.model(getPurchaseService().getByRoom(roomName, PurchaseStatus.ORDERED), String.valueOf(HttpServletResponse.SC_OK));
    }
}
