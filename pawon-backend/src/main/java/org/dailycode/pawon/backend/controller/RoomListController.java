/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.controller;

import java.io.IOException;
import java.io.Serializable;
import org.dailycode.pawon.backend.AppBackend;
import org.dailycode.pawon.backend.listener.ActionListListener;
import org.dailycode.pawon.core.entity.Room;
import org.dailycode.pawon.datamodel.RoomSelectableDataModel;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author kris
 */
@Controller("roomList")
@Scope("view")
public class RoomListController extends BaseController implements ActionListListener, Serializable {

    private Room selectedRoom;
    private RoomSelectableDataModel roomSelectableDataModel;

    @Override
    public void init() {
        roomSelectableDataModel = new RoomSelectableDataModel(getService().getRoomService().getAll());
    }

    @Override
    public void actionForm() throws IOException {
        redirectView("room_form", AppBackend.SN_ENTITY, null);
    }

    @Override
    public void actionDelete() throws IOException {
        if (selectedRoom != null) {
            getService().getRoomService().delete(selectedRoom.getId());
            redirectView("room_list", AppBackend.SN_ENTITY, null);
        } else {
            showMessageDeleteEmpty();
        }
    }

    @Override
    public void actionEdit() throws IOException {
        if (selectedRoom != null) {
            redirectView("room_form", AppBackend.SN_ENTITY, selectedRoom);
        } else {
            showMessageEditEmpty();
        }
    }

    @Override
    public void actionSearch() throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Room getSelectedRoom() {
        return selectedRoom;
    }

    public void setSelectedRoom(Room selectedRoom) {
        this.selectedRoom = selectedRoom;
    }

    public RoomSelectableDataModel getRoomSelectableDataModel() {
        return roomSelectableDataModel;
    }

    public void setRoomSelectableDataModel(RoomSelectableDataModel roomSelectableDataModel) {
        this.roomSelectableDataModel = roomSelectableDataModel;
    }
}
