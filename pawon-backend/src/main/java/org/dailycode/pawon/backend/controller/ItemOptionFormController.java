/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.controller;

import org.dailycode.pawon.backend.AppBackend;
import org.dailycode.pawon.backend.listener.ActionFormListener;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.event.PhaseId;
import org.dailycode.pawon.core.common.Section;
import org.dailycode.pawon.core.entity.ItemOption;
import org.dailycode.pawon.core.entity.Language;
import org.dailycode.pawon.core.entity.LocalizedInformation;
import org.primefaces.component.editor.Editor;
import org.primefaces.component.panelgrid.PanelGrid;
import org.primefaces.component.tabview.Tab;
import org.primefaces.component.tabview.TabView;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author kris
 * @created Mar 7, 2013
 */
@Controller("itemOptionForm")
@Scope("view")
public class ItemOptionFormController extends BaseController implements ActionFormListener, Serializable {

    private ItemOption itemOption;
    private ItemOption parentItemOption;
    private TabView tabView;
    private TreeNode root;
    private TreeNode selectedNode;
    private HtmlInputText inputTextTitle;
    private HtmlOutputText outputTextTitle;
    private Editor editor;
    private List<Language> languages = new ArrayList<>();
    private Long currentParentOptionId;
    private Section purchaseSection;

    @Override
    public void init() {
        languages = getService().getLanguageService().getActiveLanguage();
        purchaseSection = getCurrentSection();

        //Set default item option
        itemOption = (ItemOption) getCurrentEntity();
        if (itemOption == null) {
            itemOption = new ItemOption();
            itemOption.setOrderNumber(1);
        } else {
            parentItemOption = getService().getItemOptionService().getById(itemOption.getParentId());
        }

        currentParentOptionId = itemOption.getParentId();

        createDynamicTabView();

        if (getFacesContext().getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            createTree();
        }
    }

    private void createTree() {
        root = new DefaultTreeNode("root", null);

        List<ItemOption> itemOptions = getService().getItemOptionService().getByPurchaseSection(purchaseSection);
        for (ItemOption ic : itemOptions) {
            if (ic.getParentId() == 0) {
                createTree(ic, root);
            }
        }
    }

    private TreeNode createTree(ItemOption pItemOption, TreeNode rootNode) {
        TreeNode parentNode = new DefaultTreeNode(pItemOption, rootNode);

        for (ItemOption parent : getService().getItemOptionService().getByParent(pItemOption.getId())) {
            TreeNode childNode = createTree(parent, parentNode);
            childNode.setSelectable(false);

            if (parent.getParentId() == currentParentOptionId) {
                parentNode.setSelected(true);
                parentNode.setExpanded(true);
                childNode.setExpanded(true);
            }
        }
        return parentNode;
    }

    private void createDynamicTabView() {
        // tabView = (TabView) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:tabView");
        tabView = new TabView();
        for (Language language : languages) {
            LocalizedInformation information = getService().getLocalizedInformationService().getByItemOptionAndLanguage(itemOption, language);

            Tab tab = new Tab();
            tab.setId("tab" + language.getId());
            tab.setTitle(language.getName());

            outputTextTitle = new HtmlOutputText();
            outputTextTitle.setId("output" + language.getId());
            outputTextTitle.setValue("Title :");

            inputTextTitle = new HtmlInputText();
            inputTextTitle.setId("inputTextTitle" + language.getId());
            inputTextTitle.setStyle("width:50%");

            editor = new Editor();
            editor.setId("editor" + language.getId());
            editor.setWidth(700);

            if (information != null) {
                inputTextTitle.setValue(information.getTitle());
                editor.setValue(information.getDescription());
            }

            PanelGrid panelGrid = new PanelGrid();
            panelGrid.setId("grid" + language.getId());
            panelGrid.setStyle("border-style: hidden !important;");
            panelGrid.setColumns(1);
            panelGrid.getChildren().add(outputTextTitle);
            panelGrid.getChildren().add(inputTextTitle);
            panelGrid.getChildren().add(editor);

            tab.getChildren().add(panelGrid);

            tabView.getChildren().add(tab);
        }
    }

    public void onNodeSelect(NodeSelectEvent event) {
        parentItemOption = (ItemOption) selectedNode.getData();
        if (parentItemOption.getId() != currentParentOptionId) {
            itemOption.setOrderNumber(suggestOrderNumber(getService().getItemOptionService().getByParentAndPurchaseSection(parentItemOption.getId(), purchaseSection)));
        } else {
            itemOption.setOrderNumber(getService().getItemOptionService().getById(itemOption.getId()).getOrderNumber());
        }
    }

    private int suggestOrderNumber(List<ItemOption> options) {
        if (options.isEmpty()) {
            return 1;
        }
        return options.size() + 1;
    }

    @Override
    public void actionCancel() throws IOException {
        redirectView("option_list", AppBackend.SN_ENTITY, true);
    }

    @Override
    public void actionSave() throws IOException {
        itemOption.setSection(purchaseSection);

        for (Language language : languages) {
            inputTextTitle = (HtmlInputText) getFacesContext().getViewRoot().findComponent("form:tabView:inputTextTitle" + language.getId());
            editor = (Editor) getFacesContext().getViewRoot().findComponent("form:tabView:editor" + language.getId());

            LocalizedInformation localizedInformation = getService().getLocalizedInformationService().getByItemOptionAndLanguage(itemOption, language);
            if (localizedInformation == null) {
                localizedInformation = new LocalizedInformation();
            }

            if (inputTextTitle.getValue() != null) {
                localizedInformation.setTitle(inputTextTitle.getValue().toString());
            }

            if (editor.getValue() != null) {
                localizedInformation.setDescription(editor.getValue().toString());
            }

            localizedInformation.setItemOption(itemOption);
            localizedInformation.setLanguage(language);
            itemOption.getLocalizedInformations().add(localizedInformation);
        }

        if (parentItemOption != null) {
            itemOption.setParentId(parentItemOption.getId());
        }

        if (getService().getItemOptionService().saveOrUpdate(itemOption) != null) {
            redirectView("option_list", AppBackend.SN_ENTITY, true);
        } else {
            redirectView(DB_ERROR_VIEW, AppBackend.SN_ENTITY, true);
        }
    }

    public ItemOption getItemOption() {
        return itemOption;
    }

    public void setItemOption(ItemOption itemOption) {
        this.itemOption = itemOption;
    }

    public TreeNode getRoot() {
        return root;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public TreeNode getSelectedNode() {
        for (int i = 0; i < root.getChildren().size(); i++) {
            removeDefaultSelectedTree(root.getChildren().get(i));
        }
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    public TabView getTabView() {
        return tabView;
    }

    public void setTabView(TabView tabView) {
        this.tabView = tabView;
    }

    public ItemOption getParentItemOption() {
        return parentItemOption;
    }

    public void setParentItemOption(ItemOption parentItemOption) {
        this.parentItemOption = parentItemOption;
    }

    public ItemOption getCurrentItemOption() {
        return getService().getItemOptionService().getOrganizedParent(purchaseSection, itemOption, true);
    }
}
