/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.controller;

import org.dailycode.pawon.backend.AppBackend;
import org.dailycode.pawon.backend.listener.ActionFormListener;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import org.dailycode.pawon.core.common.Section;
import org.dailycode.pawon.core.entity.Item;
import org.dailycode.pawon.core.entity.ItemCategory;
import org.dailycode.pawon.core.entity.ItemOption;
import org.dailycode.pawon.core.entity.Language;
import org.dailycode.pawon.core.entity.LocalizedInformation;
import org.dailycode.pawon.core.helper.ImageHandler;
import org.primefaces.component.editor.Editor;
import org.primefaces.component.panelgrid.PanelGrid;
import org.primefaces.component.tabview.Tab;
import org.primefaces.component.tabview.TabView;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.primefaces.model.UploadedFile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author kris
 * @created Feb 26, 2013
 */
@Controller("itemForm")
@Scope("view")
public class ItemFormController extends BaseController implements ActionFormListener, Serializable {

    private Item item;
    private TabView tabView;
    private TreeNode rootCategory;
    private TreeNode selectedCategoryNode;
    private TreeNode rootOption;
    private TreeNode[] selectedOptionNodes;
    private HtmlInputText inputTextTitle;
    private HtmlOutputText outputTextTitle;
    private UploadedFile uploadedImage;
    private Editor editor;
    private List<Language> languages = new ArrayList<>();
    private Section purchaseSection;
    private List<ItemOption> itemOptions = new ArrayList<>();
    private String currentCode;

    @Override
    public void init() {
        languages = getService().getLanguageService().getActiveLanguage();
        item = (Item) getCurrentEntity();
        purchaseSection = getCurrentSection();

        if (item == null) {
            item = new Item();
            item.setItemCategory(new ItemCategory());
            item.setCode("");
        }

        currentCode = item.getCode();

        createDynamicTabView();

        if (getFacesContext().getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            itemOptions = getService().getItemOptionService().getByItem(item);
            createCategoryTree();
            createOptionTree();
        }
    }

    @Override
    public void actionCancel() throws IOException {
        redirectView("item_list", AppBackend.SN_ENTITY, true);
    }

    @Override
    public void actionSave() throws IOException {

        if (uploadedImage != null) {
            item.setImage(new Date().getTime() + "_" + uploadedImage.getFileName());
            item.setImageData(uploadedImage.getContents());
        }

        for (Language language : languages) {
            inputTextTitle = (HtmlInputText) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:tabView:inputTextTitle" + language.getId());
            editor = (Editor) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:tabView:editor" + language.getId());

            LocalizedInformation localizedInformation = getService().getLocalizedInformationService().getByItemAndLanguage(item, language);
            if (localizedInformation == null) {
                localizedInformation = new LocalizedInformation();
            }

            if (inputTextTitle.getValue() != null) {
                localizedInformation.setTitle(inputTextTitle.getValue().toString());
            }

            if (editor.getValue() != null) {
                localizedInformation.setDescription(editor.getValue().toString());
            }

            localizedInformation.setItem(item);
            localizedInformation.setLanguage(language);
            item.getLocalizedInformations().add(localizedInformation);
        }



        if (selectedOptionNodes != null) {
            for (TreeNode node : selectedOptionNodes) {
                //System.out.println(((ItemOption) node.getData()).toString());
                item.getItemOptions().add((ItemOption) node.getData());
            }
        }

        if (getService().getItemService().saveOrUpdate(item) != null) {
            redirectView("item_list", AppBackend.SN_ENTITY, true);
        } else {
            showGeneralErrorMessage();
        }
    }

    private void createCategoryTree() {
        rootCategory = new DefaultTreeNode("rootCategory", null);
        Section ps = ((Section) AppBackend.getInstance().getSessionMapValue(AppBackend.SN_SECTION));
        if (ps == null) {
            ps = Section.RESTAURANT;
        }

        for (ItemCategory ic : getService().getItemCategoryService().getBySection(ps)) {
            if (ic.getParentId() == 0) {
                createCategoryTree(ic, rootCategory);
            }
        }
    }

    private TreeNode createCategoryTree(ItemCategory pItemCategory, TreeNode rootNode) {
        TreeNode parentNode = new DefaultTreeNode(pItemCategory, rootNode);
        parentNode.setExpanded(true);
        if (pItemCategory.getId() == item.getItemCategory().getId()) {
            parentNode.setSelected(true);
        }

        for (ItemCategory parent : getService().getItemCategoryService().getByParent(pItemCategory.getId())) {
            TreeNode childNode = createCategoryTree(parent, parentNode);
            childNode.setExpanded(true);

            if (item.getItemCategory() != null) {
                if (parent.getId() == item.getItemCategory().getId()) {
                    childNode.setSelected(true);
                }
            }
        }
        return parentNode;
    }

    private void createOptionTree() {
        rootOption = new DefaultTreeNode("rootOption", null);

        for (ItemOption ic : getService().getItemOptionService().getByPurchaseSection(purchaseSection)) {
            if (ic.getParentId() == 0) {
                createOptionTree(ic, rootOption);
            }
        }
    }

    private TreeNode createOptionTree(ItemOption pItemOption, TreeNode rootNode) {
        TreeNode parentNode = new DefaultTreeNode(pItemOption, rootNode);
        for (ItemOption itemOption : itemOptions) {
            if (itemOption.getId() == pItemOption.getId()) {
                parentNode.setSelected(true);
            }
        }

        for (ItemOption parent : getService().getItemOptionService().getByParent(pItemOption.getId(), true)) {
            TreeNode childNode = createOptionTree(parent, parentNode);
            childNode.setSelectable(false);
        }

        return parentNode;
    }

    private void createDynamicTabView() {
        // tabView = (TabView) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:tabView");
        tabView = new TabView();
        for (Language language : languages) {
            LocalizedInformation information = getService().getLocalizedInformationService().getByItemAndLanguage(item, language);

            Tab tab = new Tab();
            tab.setId("tab" + language.getId());
            tab.setTitle(language.getName());

            outputTextTitle = new HtmlOutputText();
            outputTextTitle.setId("output" + language.getId());
            outputTextTitle.setValue("Title :");

            inputTextTitle = new HtmlInputText();
            inputTextTitle.setId("inputTextTitle" + language.getId());
            inputTextTitle.setStyle("width:50%");

            editor = new Editor();
            editor.setId("editor" + language.getId());
            editor.setWidth(700);

            if (information != null) {
                inputTextTitle.setValue(information.getTitle());
                editor.setValue(information.getDescription());
            }

            PanelGrid panelGrid = new PanelGrid();
            panelGrid.setId("grid" + language.getId());
            panelGrid.setStyle("border-style: hidden !important;");
            panelGrid.setColumns(1);
            panelGrid.getChildren().add(outputTextTitle);
            panelGrid.getChildren().add(inputTextTitle);
            panelGrid.getChildren().add(editor);

            tab.getChildren().add(panelGrid);

            tabView.getChildren().add(tab);
        }
    }

    public void onCategoryNodeSelect(NodeSelectEvent event) {
        item.setItemCategory((ItemCategory) selectedCategoryNode.getData());
    }

    public void deleteImage() {
        ImageHandler.deleteImageOnServer(getService().getSystemParameterService().getImageSource(), item.getImage());
        item.setImage(null);
    }

    public void checkCode() {
        //Allow to save same code for edit 
        if (!item.getCode().equals(currentCode)) {
            if (getService().getItemService().getByCode(item.getCode()) != null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "code is exist !", ""));
            }
        }
    }

    public TreeNode getRootCategory() {
        return rootCategory;
    }

    public TreeNode getRootOption() {
        return rootOption;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public TreeNode getSelectedCategoryNode() {
        for (int i = 0; i < rootCategory.getChildren().size(); i++) {
            removeDefaultSelectedTree(rootCategory.getChildren().get(i));
        }
        return selectedCategoryNode;
    }

    public void setSelectedCategoryNode(TreeNode selectedCategoryNode) {
        this.selectedCategoryNode = selectedCategoryNode;
    }

    public TreeNode[] getSelectedOptionNodes() {
        return selectedOptionNodes;
    }

    public void setSelectedOptionNodes(TreeNode[] selectedOptionNodes) {
        this.selectedOptionNodes = selectedOptionNodes;
    }

    public TabView getTabView() {
        return tabView;
    }

    public void setTabView(TabView tabView) {
        this.tabView = tabView;
    }

    public UploadedFile getUploadedImage() {
        return uploadedImage;
    }

    public void setUploadedImage(UploadedFile uploadedImage) {
        this.uploadedImage = uploadedImage;
    }

    public ItemCategory getCurrentItemCategory() {
        return getService().getItemCategoryService().getOrganizedParent(purchaseSection, item.getItemCategory(), false);
    }
}
