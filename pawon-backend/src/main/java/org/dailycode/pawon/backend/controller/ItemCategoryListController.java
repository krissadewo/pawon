/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.dailycode.pawon.backend.AppBackend;
import org.dailycode.pawon.backend.listener.ActionListListener;
import org.dailycode.pawon.core.common.Section;
import org.dailycode.pawon.core.entity.Item;
import org.dailycode.pawon.core.entity.ItemCategory;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author Kris Sadewo
 * @date Feb 8, 2013
 */
@Controller("itemCategoryList")
@Scope("view")
public class ItemCategoryListController extends BaseController implements ActionListListener, Serializable {

    private TreeNode selectedNode;
    private ItemCategory selectedItemCategory;
    private String searchCodeKey;
    private String searchNameKey;
    private String searchCategoryKey;
    private TreeNode root;
    private Section section;
    private List<String> sections = new ArrayList<>();
    List<Item> items = new ArrayList<>();

    @Override
    public void init() {
        sections = createdSections();
        section = getCurrentSection();
        if (section == null) {
            section = Section.RESTAURANT;
        }
        changeSelectedSection();
    }

    private void createTree() {
        root = new DefaultTreeNode("root", null);
        for (ItemCategory itemCategory : getService().getItemCategoryService().getBySection(section)) {
            if (itemCategory.getParentId() == 0) {
                createTree(itemCategory, root);
            }
        }
    }

    private TreeNode createTree(ItemCategory itemCategory, TreeNode rootNode) {
        TreeNode parentNode = new DefaultTreeNode(itemCategory, rootNode);
        parentNode.setExpanded(true);

        for (ItemCategory parent : getService().getItemCategoryService().getByParent(itemCategory.getId())) {
            TreeNode childNode = createTree(parent, parentNode);
            childNode.setExpanded(true);
        }
        return parentNode;
    }

    public TreeNode getRoot() {
        return root;
    }

    @Override
    public void actionForm() {
        redirectView("category_form", AppBackend.SN_ENTITY, true);
    }

    @Override
    public void actionEdit() throws IOException {
        if (selectedNode != null) {
            redirectView("category_form", AppBackend.SN_ENTITY,
                    getService().getItemCategoryService().getById(((ItemCategory) selectedNode.getData()).getId()));
        } else {
            showMessageEditEmpty();
        }
    }

    @Override
    public void actionDelete() throws IOException {
        if (selectedNode != null) {
            getService().getItemCategoryService().delete(((ItemCategory) selectedNode.getData()).getId());
            changeSelectedSection();           
        } else {
            showMessageDeleteEmpty();
        }
    }

    @Override
    public void actionSearch() throws IOException {
       
    }

    public void changeSelectedSection() {
        AppBackend.getInstance().setSessionMapValue(AppBackend.SN_SECTION, section);
        createTree();
    }

    public Section getPurchaseSection() {
        return section;
    }

    public void setPurchaseSection(Section purchaseSection) {
        this.section = purchaseSection;
    }

    public List<String> getPurchaseSections() {
        return sections;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public String getSearchCodeKey() {
        return searchCodeKey;
    }

    public void setSearchCodeKey(String searchCodeKey) {
        this.searchCodeKey = searchCodeKey;
    }

    public String getSearchNameKey() {
        return searchNameKey;
    }

    public void setSearchNameKey(String searchNameKey) {
        this.searchNameKey = searchNameKey;
    }

    public String getSearchCategoryKey() {
        return searchCategoryKey;
    }

    public void setSearchCategoryKey(String searchCategoryKey) {
        this.searchCategoryKey = searchCategoryKey;
    }

    public ItemCategory getSelectedItemCategory() {
        return selectedItemCategory;
    }

    public void setSelectedItemCategory(ItemCategory selectedItemCategory) {
        items = getService().getItemService().getByCategory(selectedItemCategory.getId());
        this.selectedItemCategory = selectedItemCategory;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
