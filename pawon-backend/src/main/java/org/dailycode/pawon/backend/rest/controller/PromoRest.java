/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.rest.controller;

import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.dailycode.pawon.backend.rest.AppRest;
import org.dailycode.pawon.core.dao.service.BaseService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author kris
 */
@Controller
public class PromoRest extends BaseService {
    
    @RequestMapping(value = "/promo", method = RequestMethod.GET)
    public Map<String, Object> getLanguage() {
        return AppRest.model(getGalleriaService().getAll(true), String.valueOf(HttpServletResponse.SC_OK));
    }
}
