package org.dailycode.pawon.backend.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Iterator;
import javax.servlet.http.HttpSession;
import org.dailycode.pawon.backend.AppBackend;
import org.dailycode.pawon.core.common.Role;
import org.joda.time.DateTime;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author kris
 */
public class LoginSuccessHandler implements AuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        HttpSession session = httpServletRequest.getSession(true);
        String auth = "";
        for (Iterator<? extends GrantedAuthority> it = authentication.getAuthorities().iterator(); it.hasNext();) {
            GrantedAuthority authority = it.next();
            auth = authority.getAuthority();
        }
        
        if (auth.equals(Role.ROLE_ADMIN.toString())) {
            session.setMaxInactiveInterval(60 * 10);
        } else {
            session.setMaxInactiveInterval(-1);
        }
        
        AppBackend.getLogger(this).info("User " + authentication.getName() + " was log in at " + new DateTime().toString("dd-MM-yyyy HH:mm:ss") + "  as " + auth);
        httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/ui/purchase/notification.jsf");
    }
}
