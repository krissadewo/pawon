/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.controller;

import java.io.IOException;
import java.io.Serializable;
import org.dailycode.pawon.backend.AppBackend;
import org.dailycode.pawon.backend.listener.ActionListListener;
import org.dailycode.pawon.core.entity.Users;
import org.dailycode.pawon.datamodel.UserSelectableDataModel;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author kris
 */
@Controller("userList")
@Scope("view")
public class UserListController extends BaseController implements ActionListListener, Serializable {

    private Users selectedUser;
    private UserSelectableDataModel userSelectableDataModel;

    @Override
    public void init() {
        userSelectableDataModel = new UserSelectableDataModel(getService().getUserService().getAll());
    }

    @Override
    public void actionForm() throws IOException {
        redirectView("user_form", AppBackend.SN_ENTITY, null);
    }

    @Override
    public void actionEdit() throws IOException {
        if (selectedUser != null) {
            redirectView("user_form", AppBackend.SN_ENTITY, selectedUser);
        } else {
            showMessageEditEmpty();
        }
    }

    @Override
    public void actionDelete() throws IOException {
        if (selectedUser != null) {
            getService().getUserService().delete(selectedUser.getId());
            redirectView("user_list", AppBackend.SN_ENTITY, null);
        } else {
            showMessageDeleteEmpty();
        }
    }

    @Override
    public void actionSearch() throws IOException {
    }

    public Users getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(Users selectedUser) {
        this.selectedUser = selectedUser;
    }

    public UserSelectableDataModel getUserSelectableDataModel() {
        return userSelectableDataModel;
    }

    public void setUserSelectableDataModel(UserSelectableDataModel userSelectableDataModel) {
        this.userSelectableDataModel = userSelectableDataModel;
    }
}
