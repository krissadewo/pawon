/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.controller;

import java.io.IOException;
import java.io.Serializable;
import org.dailycode.pawon.backend.AppBackend;
import org.dailycode.pawon.backend.listener.ActionFormListener;
import org.dailycode.pawon.core.entity.Room;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author kris
 */
@Controller("roomForm")
@Scope("view")
public class RoomFormController extends BaseController implements ActionFormListener, Serializable {

    public Room room;

    @Override
    public void init() {
        room = (Room) getCurrentEntity();
        if (room == null) {
            room = new Room();
        }
    }

    @Override
    public void actionCancel() throws IOException {
        redirectView("room_list", AppBackend.SN_ENTITY, true);
    }

    @Override
    public void actionSave() throws IOException {
        if (getService().getRoomService().saveOrUpdate(room) != null) {
            redirectView("room_list", AppBackend.SN_ENTITY, true);
        } else {
            redirectView(DB_ERROR_VIEW, AppBackend.SN_ENTITY, true);
        }
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }
}
