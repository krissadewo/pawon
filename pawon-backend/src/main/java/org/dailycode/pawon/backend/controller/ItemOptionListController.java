/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.controller;

import org.dailycode.pawon.backend.AppBackend;
import org.dailycode.pawon.backend.listener.ActionListListener;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.dailycode.pawon.core.common.Section;
import org.dailycode.pawon.core.entity.ItemOption;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author kris
 * @created Mar 5, 2013
 */
@Controller("itemOptionList")
@Scope("view")
public class ItemOptionListController extends BaseController implements ActionListListener, Serializable {

    private Section purchaseSection;
    private List<String> purchaseSections = new ArrayList<>();
    private ItemOption selectedItemOption;
    private TreeNode selectedNode;
    private TreeNode root;

    @Override
    public void init() {
        purchaseSections = createdSections();
        purchaseSection = getCurrentSection();
        if (purchaseSection == null) {
            purchaseSection = Section.RESTAURANT;
        }
        changeSelectedPurchaseSections();
    }

    private void createTree() {
        root = new DefaultTreeNode("root", null);
        for (ItemOption itemOption : getService().getItemOptionService().getByPurchaseSection(purchaseSection)) {
            if (itemOption.getParentId() == 0) {
                createTree(itemOption, root);
            }
        }
    }

    private TreeNode createTree(ItemOption itemOption, TreeNode rootNode) {
        TreeNode parentNode = new DefaultTreeNode(itemOption, rootNode);
        for (ItemOption parent : getService().getItemOptionService().getByParent(itemOption.getId())) {
            createTree(parent, parentNode);
        }
        return parentNode;
    }

    public TreeNode getRoot() {
        return root;
    }

    public void changeSelectedPurchaseSections() {
        AppBackend.getInstance().setSessionMapValue(AppBackend.SN_SECTION, purchaseSection);
        createTree();
    }

    @Override
    public void actionForm() throws IOException {
        redirectView("option_form", AppBackend.SN_ENTITY, true);
    }

    @Override
    public void actionEdit() throws IOException {
        if (selectedNode != null) {
            redirectView("option_form", AppBackend.SN_ENTITY,
                    getService().getItemOptionService().getById(((ItemOption) selectedNode.getData()).getId()));
        } else {
            showMessageEditEmpty();
        }
    }

    @Override
    public void actionDelete() throws IOException {
        if (selectedNode != null) {
            getService().getItemOptionService().delete(((ItemOption) selectedNode.getData()).getId());
            changeSelectedPurchaseSections();
        } else {
            showMessageDeleteEmpty();
        }
    }

    @Override
    public void actionSearch() throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Section getPurchaseSection() {
        return purchaseSection;
    }

    public void setPurchaseSection(Section purchaseSection) {
        this.purchaseSection = purchaseSection;
    }

    public List<String> getPurchaseSections() {
        return purchaseSections;
    }

    public void setPurchaseSections(List<String> purchaseSections) {
        this.purchaseSections = purchaseSections;
    }

    public ItemOption getSelectedItemOption() {
        return selectedItemOption;
    }

    public void setSelectedItemOption(ItemOption selectedItemOption) {
        this.selectedItemOption = selectedItemOption;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }
}
