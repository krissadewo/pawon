/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.rest.controller;

import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.dailycode.pawon.backend.rest.AppRest;
import org.dailycode.pawon.core.dao.service.BaseService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author kris
 * @created Mar 21, 2013
 */
@Controller
@RequestMapping("/purchaseOption")
public class PurchaseOptionRest extends BaseService {

    @RequestMapping(value = "/purchaseItemId/{purchaseItemId}", method = RequestMethod.GET)
    public Map<String, Object> getPurchaseOptionByPurchaseItem(@PathVariable Long purchaseItemId) {
        return AppRest.model(getPurchaseOptionService().getByPurchaseItem(purchaseItemId), String.valueOf(HttpServletResponse.SC_OK));
    }
}
