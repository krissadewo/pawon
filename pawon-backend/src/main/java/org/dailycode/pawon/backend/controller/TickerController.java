/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.controller;

import java.io.IOException;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import org.dailycode.pawon.backend.listener.ActionFormListener;
import org.dailycode.pawon.core.entity.Ticker;
import org.primefaces.component.poll.Poll;
import org.primefaces.context.RequestContext;
import org.primefaces.push.PushContext;
import org.primefaces.push.PushContextFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author kris
 */
@Controller("tickerController")
@Scope("view")
public class TickerController extends BaseController implements ActionFormListener, Serializable {

    private Ticker ticker;
    private String message;

    @Override
    public void init() {
        ticker = getService().getTickerService().getTicker();
    }

    @Override
    public void actionCancel() throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actionSave() throws IOException {
        if (getService().getTickerService().saveOrUpdate(ticker) != null) {
            showMessage(FacesMessage.SEVERITY_INFO, "Message has been save", "");
        }
    }

    public String getMessage() {
        return getService().getTickerService().getTicker().getMessage();
    }

    public Ticker getTicker() {
        return ticker;
    }

    public void setTicker(Ticker ticker) {
        this.ticker = ticker;
    }
}
