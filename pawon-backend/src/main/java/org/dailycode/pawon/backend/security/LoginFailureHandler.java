package org.dailycode.pawon.backend.security;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;


import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.dailycode.pawon.backend.AppBackend;
import org.joda.time.DateTime;

/**
 *
 * @author kris
 */
public class LoginFailureHandler implements AuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        AppBackend.getLogger(this).info("User "+e.getAuthentication().getName() + " was trying to log in at " + new DateTime().toString("dd-MM-yyyy HH:mm:ss")+" but failed");
        httpServletRequest.getSession().setAttribute("message", "User and password not registered");
        httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/ui/login.jsf");
    }
}
