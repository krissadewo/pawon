/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.helper;

import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.print.PrintServiceLookup;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.MediaPrintableArea;
import javax.print.attribute.standard.MediaSize;
import javax.print.attribute.standard.MediaSizeName;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.engine.export.JRPrintServiceExporterParameter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.dailycode.pawon.backend.AppBackend;
import org.dailycode.pawon.core.dao.service.SystemParameterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
@Component
public class ReportManager {

    @Autowired
    private SystemParameterService systemParameterService;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private JasperPrint jasperPrint;

    /**
     *
     * @param jrDataSource null allowed
     * @param fileName
     * @param params
     * @return
     * @throws JRException
     */
    public ReportManager createReport(JRDataSource jrDataSource, String fileName, Map params) {
        Connection connection = null;
        try {
            connection = jdbcTemplate.getDataSource().getConnection();
            String reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/report/");
            params.put("SUBREPORT_DIR", reportPath);
            JasperDesign jasperDesign = JRXmlLoader.load(reportPath + "/" + fileName);
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            if (jrDataSource == null) {
                jasperPrint = JasperFillManager.fillReport(jasperReport, params, connection);
            } else {
                jasperPrint = JasperFillManager.fillReport(jasperReport, params, jrDataSource);
            }
        } catch (SQLException | JRException ex) {
            AppBackend.getLogger(this).error(ex);
        } finally {
            try {
                if (!connection.isClosed()) {
                    connection.close();
                }
            } catch (SQLException ex) {
                AppBackend.getLogger(this).error(ex);
            }
        }
        return this;
    }

    public void exportReport() throws JRException {
        ServletOutputStream servletOutputStream = null;
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();

        try {
            response.setContentType("application/pdf");
            response.addHeader("Content-disposition", "attachment; filename=report.pdf");
            servletOutputStream = response.getOutputStream();
            JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
            FacesContext.getCurrentInstance().responseComplete();
        } catch (IOException ex) {
            AppBackend.getLogger(this).error(ex);
        } finally {
            try {
                servletOutputStream.flush();
                servletOutputStream.close();
            } catch (IOException ex) {
                AppBackend.getLogger(this).error(ex);
            }
        }
    }

    public void printReport() {
        try {
//            DocFlavor docFlavor = DocFlavor.INPUT_STREAM.POSTSCRIPT;
//            URI uri = new URI("ipp://192.168.155.51/pawon");
            //IppPrintService printService = new IppPrintService(uri);
            //IppPrintServiceLookup lookup = new IppPrintServiceLookup(uri, "kris", "1234");

            PrinterJob job = PrinterJob.getPrinterJob();

//            /* Create an array of PrintServices */
//            PrintService[] services = Win32PrintServiceLookup.lookupPrintServices(null, null);
//            int selectedService = 0;
//            /* Scan found services to see if anyone suits our needs */
//            for (int i = 0; i < services.length; i++) {
//                if (services[i].getName().toUpperCase().contains("pawon")) {
//                    /*If the service is named as what we are querying we select it */
//                    selectedService = i;
//                }
//            }

            //job.setPrintService(services[selectedService]);
            job.setPrintService(PrintServiceLookup.lookupDefaultPrintService());
            //job.setPrintService(lookup.getDefaultPrintService());
            PrintRequestAttributeSet printRequestAttributeSet = new HashPrintRequestAttributeSet();
            MediaSizeName mediaSizeName = MediaSize.findMedia(4, 4, MediaPrintableArea.MM);
            printRequestAttributeSet.add(mediaSizeName);
            printRequestAttributeSet.add(new Copies(systemParameterService.getPrinterCopy()));

            JRPrintServiceExporter exporter;
            exporter = new JRPrintServiceExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);

            /* We set the selected service and pass it as a paramenter */
            exporter.setParameter(JRPrintServiceExporterParameter.PRINT_SERVICE, job.getPrintService());
            exporter.setParameter(JRPrintServiceExporterParameter.PRINT_SERVICE_ATTRIBUTE_SET, job.getPrintService().getAttributes());
            exporter.setParameter(JRPrintServiceExporterParameter.PRINT_REQUEST_ATTRIBUTE_SET, printRequestAttributeSet);
            exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PAGE_DIALOG, Boolean.FALSE);
            exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PRINT_DIALOG, Boolean.FALSE);
            exporter.exportReport();
        } catch (PrinterException | JRException ex) {
            AppBackend.getLogger(this).error(ex);
        }
    }
}
