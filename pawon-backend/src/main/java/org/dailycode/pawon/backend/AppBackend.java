/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend;

import java.util.HashMap;
import java.util.Map;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;

/**
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
public class AppBackend {

    private static AppBackend instance;
    //SN mean session name
    public static final String SN_ENTITY = "entity";
    public static final String SN_SECTION = "section";
    public static final String SN_USER = "user";
  
    private AppBackend() {
    }

    public static Logger getLogger(Object o) {
        return Logger.getLogger(o.getClass());
    }

    public static AppBackend getInstance() {
        if (instance == null) {
            instance = new AppBackend();
        }
        return instance;
    }

    public int countMaxYAxis(int totalMaxYAxis) {
        if (totalMaxYAxis < 6) {
            return 6;
        } else {
            return (totalMaxYAxis - (totalMaxYAxis % 6)) + 6;
        }
    }

    public void setSessionMapValue(String sessionName, Object param) {
        clearSessionParamValue(sessionName);
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(sessionName, param);
    }

    public Object getSessionMapValue(String sessionName) {
        return FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(sessionName);
    }

    public void clearSessionParamValue(String sessionName) {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove(sessionName);
    }

    public static Map model(Object data, String status) {
        Map model = new HashMap();
        model.put("data", data);
        model.put("status", status);
        return model;
    }
}
