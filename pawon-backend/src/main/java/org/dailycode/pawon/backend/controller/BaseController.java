/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.controller;

import org.dailycode.pawon.backend.AppBackend;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.dailycode.pawon.core.common.Section;
import org.dailycode.pawon.core.dao.service.BaseService;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Jan 13, 2013
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
@Component
public abstract class BaseController {

    protected static final String DB_ERROR_VIEW = "../errors/db_error";
    @Autowired
    private BaseService service;
    private String applicationMediaURL;

    @PostConstruct
    public abstract void init();

    public BaseController() {
    }

    public BaseService getService() {
        return service;
    }

    public FacesContext getFacesContext() {
        return FacesContext.getCurrentInstance();
    }

    /**
     *
     * @param severity level (warn, info ...)
     * @param message message to show
     * @param detail message detail
     */
    protected void showMessage(FacesMessage.Severity severity, String message, String detail) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(severity, message, detail));
    }

    protected void showMessageEditEmpty() {
        FacesContext.getCurrentInstance().
                addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Please, choose the data before edit", ""));
    }

    protected void showMessageDeleteEmpty() {
        FacesContext.getCurrentInstance().
                addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Please, choose the data before delete", ""));
    }

    protected void showMessageEditMoreThanOne() {
        FacesContext.getCurrentInstance().
                addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Only one data could be edit", ""));
    }

    protected void showGeneralErrorMessage() {
        FacesContext.getCurrentInstance().
                addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Application error, please check your data before save !", ""));
    }

    /**
     *
     * @param url destination URL
     * @param sessionName name of session, can be null
     * @param isClearSession can be null
     */
    protected void redirectView(String url, String sessionName, boolean isClearSession) {
        if (isClearSession) {
            AppBackend.getInstance().clearSessionParamValue(sessionName);
        }
        redirectView(url);
    }

    /**
     *
     * @param url
     * @param sessionName
     * @param param
     */
    protected void redirectView(String url, String sessionName, Object param) {
        AppBackend.getInstance().setSessionMapValue(sessionName, param);
        redirectView(url);
    }

    protected final void redirectView(String url) {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url.concat(".jsf"));
        } catch (IOException ex) {
            AppBackend.getLogger(this).error(ex);
        }
    }

    public String getApplicationMediaURL() {
        return service.getSystemParameterService().getImageURL();
    }

    protected List<String> createdSections() {
        List<String> purchaseSections = new ArrayList<>();
        Section[] sections = {
            Section.EVENTS,
            Section.FACILITIES,
            Section.MISC,
            Section.PHOTOGALLERY,
            Section.PROMO,
            Section.RESTAURANT};
        for (Section section : sections) {
            purchaseSections.add(section.name());
        }
        return purchaseSections;
    }

    /**
     * Kemungkinan ada bug di Primefaces ketika sebuah tree telah disetting
     * single selection. Tetapi ketika ada node yang diawal secara default
     * diselect maka jika client memilih lagi akan ada 2 selection yang tampak
     * Oleh karena itu kita perlu melakukan remove manual node yang terpilih.
     *
     * @param child
     * @return
     */
    protected TreeNode removeDefaultSelectedTree(TreeNode child) {
        TreeNode node = child;
        node.setSelected(false);

        if (child.getChildren().size() > 0) {
            for (int i = 0; i < child.getChildren().size(); i++) {
                node = removeDefaultSelectedTree(child.getChildren().get(i));
                node.setSelected(false);
            }
        }

        return node;
    }

    /**
     * Get current active purchase section from session
     *
     * @return
     */
    protected Section getCurrentSection() {
        return (Section) AppBackend.getInstance().getSessionMapValue(AppBackend.SN_SECTION);
    }

    /**
     * Get current active entity from session
     *
     * @return
     */
    protected Object getCurrentEntity() {
        return AppBackend.getInstance().getSessionMapValue(AppBackend.SN_ENTITY);
    }
}
