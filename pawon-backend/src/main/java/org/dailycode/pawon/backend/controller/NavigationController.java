/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.controller;

import java.io.Serializable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author kris
 * @created Feb 25, 2013
 */
@Controller
@Scope("session")
public class NavigationController extends BaseController implements Serializable {

    private String menuItemCategories = "ui/master/category_list.jsf";
    private String menuItems = "ui/master/item_list.jsf";
    private String menuOptions = "ui/master/option_list.jsf";
    private String menuRooms = "ui/master/room_list.jsf";
    private String menuUsers = "ui/master/user_list.jsf";
    private String menuLanguages = "ui/master/language_list.jsf";
    private String menuSystemParameters = "ui/master/system_parameter.jsf";
    private String menuGalleria = "ui/master/galleria_list.jsf";
    private String menuTicker = "ui/master/news_ticker.jsf";
    private String menuPurchases = "ui/purchase/purchase_list.jsf";
    private String menuNotification = "ui/purchase/notification.jsf";
    private String menuReportPurchase = "ui/report/purchase_report.jsf";

    @Override
    public void init() {
    }

    public String getMenuTicker() {
        return menuTicker;
    }

    public String getMenuGalleria() {
        return menuGalleria;
    }

    public String getMenuLanguages() {
        return menuLanguages;
    }

    public String getMenuReportPurchase() {
        return menuReportPurchase;
    }

    public String getMenuRooms() {
        return menuRooms;
    }

    public String getMenuUsers() {
        return menuUsers;
    }

    public String getMenuSystemParameters() {
        return menuSystemParameters;
    }

    public String getMenuPurchases() {
        return menuPurchases;
    }

    public String getMenuOptions() {
        return menuOptions;
    }

    public String getMenuItemCategories() {
        return menuItemCategories;
    }

    public String getMenuItems() {
        return menuItems;
    }

    public String getMenuNotification() {
        return menuNotification;
    }
}
