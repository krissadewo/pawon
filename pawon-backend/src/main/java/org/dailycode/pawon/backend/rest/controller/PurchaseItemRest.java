/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.rest.controller;

import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.dailycode.pawon.backend.rest.AppRest;
import org.dailycode.pawon.core.dao.service.BaseService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author kris
 * @created Mar 21, 2013
 */
@Controller
@RequestMapping("/purchaseItem")
public class PurchaseItemRest extends BaseService {

    @RequestMapping(value = "/purchaseId/{purchaseId}", method = RequestMethod.GET)
    public Map<String, Object> getPurchaseByPurchase(@PathVariable Long purchaseId) {
        return AppRest.model(getPurchaseItemService().getByPurchase(purchaseId), String.valueOf(HttpServletResponse.SC_OK));
    }

    @RequestMapping(value = "/purchaseItemId/{purchaseItemId}/purchaseId/{purchaseId}", method = RequestMethod.GET)
    public Map<String, Object> deletePurchaseItem(@PathVariable Long purchaseItemId, @PathVariable Long purchaseId) {
        if (getPurchaseItemService().delete(purchaseItemId, purchaseId)) {
            return AppRest.model(true, String.valueOf(HttpServletResponse.SC_OK));
        }
        return AppRest.model(false, String.valueOf(HttpServletResponse.SC_OK));
    }
}
