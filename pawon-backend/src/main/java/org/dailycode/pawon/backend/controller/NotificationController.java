/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.dailycode.pawon.backend.helper.ReportManager;
import org.dailycode.pawon.backend.listener.ActionFormListener;
import org.dailycode.pawon.core.common.PurchaseStatus;
import org.dailycode.pawon.core.entity.Purchase;
import org.dailycode.pawon.core.entity.PurchaseItem;
import org.dailycode.pawon.core.entity.PurchaseOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author kris
 * @created Mar 20, 2013
 */
@Controller("notification")
@Scope("view")
public class NotificationController extends BaseController implements ActionFormListener, Serializable {

    @Autowired
    private ReportManager reportManager;
    private Purchase selectedPurchase;
    private List<Purchase> purchases = new ArrayList<>();

    @Override
    public void init() {
        showPurchase();
    }

    private void showPurchase() {
        purchases.clear();
        for (Purchase purchase : getService().getPurchaseService().getActivePurchase(PurchaseStatus.PROCESS)) {
            List<PurchaseItem> purchaseItems = new ArrayList<>();
            Double totalPurchase = 0d;
            Double totalDiscount = 0d;
            for (PurchaseItem purchaseItem : getService().getPurchaseItemService().getByPurchase(purchase.getId())) {
                purchaseItems.add(purchaseItem);
                totalPurchase += purchaseItem.getPrice();
                totalDiscount += (purchaseItem.getDiscount() / 100) * purchaseItem.getPrice();

                List<PurchaseOption> purchaseOptions = new ArrayList<>();
                for (PurchaseOption purchaseOption : getService().getPurchaseOptionService().getByPurchaseItem(purchaseItem.getId())) {
                    purchaseOptions.add(purchaseOption);
                    totalPurchase += purchaseOption.getPrice();
                }

                purchaseItem.setPurchaseOptions(purchaseOptions);
                purchase.setPurchaseItems(purchaseItems);
                purchase.setTotalDiscount(totalDiscount);
                purchase.setTotalPrice(totalPurchase - totalDiscount);
            }
            purchases.add(purchase);
        }
    }

    @Override
    public void actionCancel() throws IOException {
        getService().getPurchaseItemService().delete(Long.parseLong(getFacesContext().getExternalContext().getRequestParameterMap().get("purchaseItemId")),
                Long.parseLong(getFacesContext().getExternalContext().getRequestParameterMap().get("purchaseId")));
        redirectView("notification", null, false);
    }

    @Override
    public void actionSave() throws IOException {
        Purchase purchase = new Purchase();
        purchase.setId(Long.parseLong(getFacesContext().getExternalContext().getRequestParameterMap().get("purchaseId")));
        purchase.setPurchaseStatus(PurchaseStatus.COMPLETE);
        getService().getPurchaseService().saveOrUpdate(purchase);
        //Print to printer
        Map<String, Object> params = new HashMap<>();
        params.put("PURCHASE_ID", purchase.getId());
        reportManager.createReport(null, "bill_print.jrxml", params).printReport();
        redirectView("notification", null, false);
    }

    public void actionDelete() throws IOException {
        getService().getPurchaseService().delete(Long.parseLong(getFacesContext().getExternalContext().getRequestParameterMap().get("purchaseId")));
        redirectView("notification", null, false);
    }

    public void updateRoom() {
        showPurchase();
    }

    public List<Purchase> getPurchases() {
        return purchases;
    }

    public void setPurchases(List<Purchase> purchases) {
        this.purchases = purchases;
    }

    public Purchase getSelectedPurchase() {
        return selectedPurchase;
    }

    public void setSelectedPurchase(Purchase selectedPurchase) {
        this.selectedPurchase = selectedPurchase;
    }
}
