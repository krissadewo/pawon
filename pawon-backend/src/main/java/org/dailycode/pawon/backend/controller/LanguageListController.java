/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.controller;

import java.io.IOException;
import java.io.Serializable;
import org.dailycode.pawon.backend.AppBackend;
import org.dailycode.pawon.backend.listener.ActionListListener;
import org.dailycode.pawon.core.entity.Language;
import org.dailycode.pawon.datamodel.LanguageSelectableDataModel;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author kris
 */
@Controller("languageList")
@Scope("view")
public class LanguageListController extends BaseController implements ActionListListener, Serializable {

    private Language selectedLanguage;
    private LanguageSelectableDataModel languageSelectableDataModel;

    @Override
    public void init() {
        languageSelectableDataModel = new LanguageSelectableDataModel(getService().getLanguageService().getAll());
    }

    @Override
    public void actionForm() throws IOException {
        redirectView("language_form", AppBackend.SN_ENTITY, null);
    }

    @Override
    public void actionEdit() throws IOException {
        if (selectedLanguage != null) {
            redirectView("language_form", AppBackend.SN_ENTITY, selectedLanguage);
        } else {
            showMessageEditEmpty();
        }
    }

    @Override
    public void actionDelete() throws IOException {
        if (selectedLanguage != null) {
            getService().getLanguageService().delete(selectedLanguage.getId());
            redirectView("language_list", AppBackend.SN_ENTITY, null);
        } else {
            showMessageDeleteEmpty();
        }
    }

    @Override
    public void actionSearch() throws IOException {
    }

    public Language getSelectedLanguage() {
        return selectedLanguage;
    }

    public void setSelectedLanguage(Language selectedLanguage) {
        this.selectedLanguage = selectedLanguage;
    }

    public LanguageSelectableDataModel getLanguageSelectableDataModel() {
        return languageSelectableDataModel;
    }

    public void setLanguageSelectableDataModel(LanguageSelectableDataModel languageSelectableDataModel) {
        this.languageSelectableDataModel = languageSelectableDataModel;
    }
}
