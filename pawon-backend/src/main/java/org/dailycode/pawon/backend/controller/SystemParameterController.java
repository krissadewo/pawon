/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.controller;

import java.io.IOException;
import java.io.Serializable;
import org.dailycode.pawon.backend.listener.ActionFormListener;
import org.dailycode.pawon.core.entity.SystemParameter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author kris
 */
@Controller("systemParameterForm")
@Scope("view")
public class SystemParameterController extends BaseController implements ActionFormListener, Serializable {

    private SystemParameter systemParameter;

    @Override
    public void init() {
        systemParameter = getService().getSystemParameterService().getAll().get(0);
    }

    @Override
    public void actionCancel() throws IOException {
    }

    @Override
    public void actionSave() throws IOException {
        getService().getSystemParameterService().saveOrUpdate(systemParameter);
    }

    public SystemParameter getSystemParameter() {
        return systemParameter;
    }

    public void setSystemParameter(SystemParameter systemParameter) {
        this.systemParameter = systemParameter;
    }
}
