package org.dailycode.pawon.backend.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;
import org.dailycode.pawon.core.entity.Users;

/**
 *
 * @author kris
 */
public class AuthenticationUserDetails implements UserDetails {

    private final String username;
    private final String passwordHash;
    private String salt;
    private final Set<GrantedAuthority> grantedAuthorities = new HashSet<>();

    public AuthenticationUserDetails(final Users users) {
        this.username = users.getUsername();
        this.passwordHash = users.getPassword();
        this.salt = users.getSalt();
        this.grantedAuthorities.add(new GrantedAuthority() {
            @Override
            public String getAuthority() {
                return users.getRole().name();
            }
        });
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }
//
//    private Collection convertAuthority(final List<Authority> authoritieses) {
//        Collection collection = new ArrayList();
//        for (final Authority authorities : authoritieses) {
//            GrantedAuthority grantedAuthority = new GrantedAuthority() {
//                @Override
//                public String getAuthority() {
//                    return authorities.getRole();
//                }
//            };
//            collection.add(grantedAuthority);
//        }
//        return collection;
//    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return grantedAuthorities;
    }

    @Override
    public String getPassword() {
        return passwordHash;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
