/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.rest.controller;

import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.dailycode.pawon.backend.rest.AppRest;
import org.dailycode.pawon.core.dao.service.BaseService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author kris
 * @created Mar 14, 2013
 */
@Controller
@RequestMapping("/option")
public class ItemOptionRest extends BaseService {

    @RequestMapping(value = "/itemId/{itemId}/languageId/{languageId}", method = RequestMethod.GET)
    public Map<String, Object> getByItem(@PathVariable Long itemId, @PathVariable Long languageId) {
        return AppRest.model(getItemOptionService().getByItem(itemId, languageId), String.valueOf(HttpServletResponse.SC_OK));
    }

    @RequestMapping(value = "/optionId/{optionId}/languageId/{languageId}", method = RequestMethod.GET)
    public Map<String, Object> getByParent(@PathVariable Long optionId, @PathVariable Long languageId) {
        return AppRest.model(getItemOptionService().getByParent(optionId, languageId, true), String.valueOf(HttpServletResponse.SC_OK));
    }
}
