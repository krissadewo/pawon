/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.rest.controller;

import java.net.MalformedURLException;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.dailycode.pawon.backend.rest.AppRest;
import org.dailycode.pawon.core.dao.service.BaseService;
import org.dailycode.pawon.core.entity.Item;
import org.dailycode.pawon.core.entity.Room;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author kris
 * @created Mar 14, 2013
 */
@Controller
@RequestMapping("/item")
public class ItemRest extends BaseService {

    @RequestMapping(value = "/categoryId/{categoryId}/languageId/{languageId}", method = RequestMethod.GET)
    public Map<String, Object> getByCategory(@PathVariable Long categoryId, @PathVariable Long languageId) {
        return AppRest.model(getItemService().getByCategory(categoryId, languageId), String.valueOf(HttpServletResponse.SC_OK));
    }

    @RequestMapping(value = "/itemId/{itemId}/languageId/{languageId}", method = RequestMethod.GET)
    public Map<String, Object> getById(@PathVariable Long itemId, @PathVariable Long languageId) {
        return AppRest.model(getItemService().getById(itemId, languageId), String.valueOf(HttpServletResponse.SC_OK));
    }
}
