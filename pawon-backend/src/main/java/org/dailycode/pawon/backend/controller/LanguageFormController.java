/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.dailycode.pawon.backend.AppBackend;
import org.dailycode.pawon.backend.listener.ActionFormListener;
import org.dailycode.pawon.core.common.LanguageCode;
import org.dailycode.pawon.core.entity.Language;
import org.dailycode.pawon.core.helper.ImageHandler;
import org.primefaces.model.UploadedFile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author kris
 */
@Controller("languageForm")
@Scope("view")
public class LanguageFormController extends BaseController implements ActionFormListener, Serializable {

    private Language language;
    private List<LanguageCode> languageCodes = new ArrayList<>();
    private UploadedFile uploadedImage;

    @Override
    public void init() {
        language = (Language) getCurrentEntity();

        if (language == null) {
            language = new Language();
        }

        createLanguageCode();
    }

    private void createLanguageCode() {
        languageCodes.addAll(Arrays.asList(LanguageCode.values()));
        for (Language lang : getService().getLanguageService().getAll()) {
            for (LanguageCode languageCode : LanguageCode.values()) {
                if (lang.getLanguageCode() == languageCode) {
                    languageCodes.remove(languageCode);
                }
            }
        }
    }

    @Override
    public void actionCancel() throws IOException {
        redirectView("language_list", AppBackend.SN_ENTITY, true);
    }

    @Override
    public void actionSave() throws IOException {
        if (uploadedImage != null) {
            language.setImage(new Date().getTime() + "_" + uploadedImage.getFileName());
            language.setImageData(uploadedImage.getContents());
        }

        if (getService().getLanguageService().saveOrUpdate(language) != null) {
            redirectView("language_list", AppBackend.SN_ENTITY, true);
        } else {
            redirectView(DB_ERROR_VIEW, AppBackend.SN_ENTITY, true);
        }
    }

    public void deleteImage() {
        ImageHandler.deleteImageOnServer(getApplicationMediaURL(), language.getImage());
        language.setImage(null);
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public List<LanguageCode> getLanguageCodes() {
        return languageCodes;
    }

    public void setLanguageCodes(List<LanguageCode> languageCodes) {
        this.languageCodes = languageCodes;
    }

    public UploadedFile getUploadedImage() {
        return uploadedImage;
    }

    public void setUploadedImage(UploadedFile uploadedImage) {
        this.uploadedImage = uploadedImage;
    }
}
