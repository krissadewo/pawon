/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.controller;

import com.lowagie.text.DocumentException;
import java.awt.print.PrinterException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import org.dailycode.pawon.backend.helper.ReportManager;
import org.dailycode.pawon.backend.listener.ActionListListener;
import org.dailycode.pawon.core.entity.Purchase;
import org.dailycode.pawon.core.entity.PurchaseItem;
import org.dailycode.pawon.core.entity.PurchaseOption;
import org.dailycode.pawon.datamodel.LazyPurchaseDataModel;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author kris
 * @created Mar 18, 2013
 */
@Controller("purchaseList")
@Scope("view")
public class PurchaseListController extends BaseController implements ActionListListener, Serializable {

    @Autowired
    private ReportManager reportManager;
    private Date startDate = new DateMidnight().minusDays(6).toDate();
    private Date endDate = new DateMidnight().toDate();
    private Date maxDate = new DateMidnight().toDate();
    private LazyDataModel<Purchase> dataModel;
    private Purchase selectedPurchase;
    private String searchRoomNameKey;
    private String searchOrderNoKey;
    private StreamedContent streamedContent;

    @Override
    public void init() {
        dataModel = new LazyPurchaseDataModel(getService().getPurchaseService());
    }

    @Override
    public void actionForm() throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actionEdit() throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actionDelete() throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actionSearch() throws IOException {

        Map<String, String> filters = new HashMap<>();
        filters.put("startDate", new DateTime(startDate).toString());
        filters.put("endDate", new DateTime(endDate).toString());
        filters.put("roomName", searchRoomNameKey);
        filters.put("orderNo", searchOrderNoKey);

        DataTable dataTable = (DataTable) getFacesContext().getViewRoot().findComponent("form:dataTable");
        dataTable.setFilters(filters);

        RequestContext.getCurrentInstance().update("form:dataTable");
    }

    public void actionPrint() throws IOException, JRException, PrinterException, DocumentException {
        Map<String, Object> params = new HashMap<>();
        params.put("PURCHASE_ID", selectedPurchase.getId());
        reportManager.createReport(null, "bill_print.jrxml", params).printReport();
    }

    public void actionExport() throws IOException, JRException, PrinterException, DocumentException {
        Map<String, Object> params = new HashMap<>();
        params.put("PURCHASE_ID", selectedPurchase.getId());
        reportManager.createReport(null, "bill_report.jrxml", params).exportReport();
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getMaxDate() {
        return maxDate;
    }

    public void setMaxDate(Date maxDate) {
        this.maxDate = maxDate;
    }

    public LazyDataModel<Purchase> getDataModel() {
        return dataModel;
    }

    public void setDataModel(LazyDataModel<Purchase> dataModel) {
        this.dataModel = dataModel;
    }

    public String getSearchRoomNameKey() {
        return searchRoomNameKey;
    }

    public void setSearchRoomNameKey(String searchRoomNameKey) {
        this.searchRoomNameKey = searchRoomNameKey;
    }

    public Purchase getSelectedPurchase() {
        return selectedPurchase;
    }

    public String getSearchOrderNoKey() {
        return searchOrderNoKey;
    }

    public void setSearchOrderNoKey(String searchOrderNoKey) {
        this.searchOrderNoKey = searchOrderNoKey;
    }

    public void setSelectedPurchase(Purchase selectedPurchase) {
        if (selectedPurchase != null) {
            List<PurchaseItem> purchaseItems = new ArrayList<>();
            double totalPrice = 0d;
            double totalDiscount = 0d;
            for (PurchaseItem purchaseItem : getService().getPurchaseItemService().getByPurchase(selectedPurchase.getId())) {
                List<PurchaseOption> purchaseOptions = getService().getPurchaseOptionService().getByPurchaseItem(purchaseItem.getId());
                for (PurchaseOption purchaseOption : purchaseOptions) {
                    totalPrice += purchaseOption.getPrice();
                }
                purchaseItem.setPurchaseOptions(purchaseOptions);
                totalDiscount += (purchaseItem.getDiscount() / 100) * purchaseItem.getPrice();
                totalPrice += purchaseItem.getPrice();
                purchaseItems.add(purchaseItem);
            }
            selectedPurchase.setTotalDiscount(totalDiscount);
            selectedPurchase.setTotalPrice(totalPrice - totalDiscount);
            selectedPurchase.setPurchaseItems(purchaseItems);
        }
        this.selectedPurchase = selectedPurchase;
    }

    public StreamedContent getStreamedContent() {
        return streamedContent;
    }

    public void setStreamedContent(StreamedContent streamedContent) {
        this.streamedContent = streamedContent;
    }
}
