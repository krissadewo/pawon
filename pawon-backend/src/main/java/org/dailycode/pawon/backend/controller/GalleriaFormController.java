/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import org.dailycode.pawon.backend.AppBackend;
import org.dailycode.pawon.backend.listener.ActionFormListener;
import org.dailycode.pawon.core.entity.Galleria;
import org.dailycode.pawon.core.helper.ImageHandler;
import org.primefaces.model.UploadedFile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author kris
 */
@Controller("galleriaForm")
@Scope("view")
public class GalleriaFormController extends BaseController implements ActionFormListener, Serializable {

    private Galleria galleria;
    private UploadedFile uploadedImage;

    @Override
    public void init() {
        galleria = (Galleria) getCurrentEntity();
        if (galleria == null) {
            galleria = new Galleria();
        }
    }

    @Override
    public void actionCancel() throws IOException {
        redirectView("galleria_list");
    }

    @Override
    public void actionSave() throws IOException {
        if (uploadedImage != null) {
            galleria.setImage(new Date().getTime() + "_" + uploadedImage.getFileName());
            galleria.setImageData(uploadedImage.getContents());
        }

        if (getService().getGalleriaService().saveOrUpdate(galleria) != null) {
            redirectView("galleria_list", AppBackend.SN_ENTITY, true);
        } else {
            showGeneralErrorMessage();
        }
    }

    public void deleteImage() {
        ImageHandler.deleteImageOnServer(getService().getSystemParameterService().getImageSource(), galleria.getImage());
        galleria.setImage(null);
    }

    public UploadedFile getUploadedImage() {
        return uploadedImage;
    }

    public void setUploadedImage(UploadedFile uploadedImage) {
        this.uploadedImage = uploadedImage;
    }

    public Galleria getGalleria() {
        return galleria;
    }

    public void setGalleria(Galleria galleria) {
        this.galleria = galleria;
    }
}
