/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.controller;

import java.io.IOException;
import java.io.Serializable;
import org.dailycode.pawon.backend.AppBackend;
import org.dailycode.pawon.backend.listener.ActionListListener;
import org.dailycode.pawon.core.entity.Galleria;
import org.dailycode.pawon.datamodel.GalleriaSelectableDataModel;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author kris
 */
@Controller("galleriaList")
@Scope("view")
public class GalleriaListController extends BaseController implements ActionListListener, Serializable {

    private Galleria selectedGalleria;
    private GalleriaSelectableDataModel galleriaSelectableDataModel;

    @Override
    public void init() {
        galleriaSelectableDataModel = new GalleriaSelectableDataModel(getService().getGalleriaService().getAll(false));
    }

    @Override
    public void actionForm() throws IOException {
        redirectView("galleria_form", AppBackend.SN_ENTITY, true);
    }

    @Override
    public void actionEdit() throws IOException {
        if (selectedGalleria != null) {
            redirectView("galleria_form", AppBackend.SN_ENTITY, selectedGalleria);
        } else {
            showMessageEditEmpty();
        }
    }

    @Override
    public void actionDelete() throws IOException {
        if (selectedGalleria != null) {
            getService().getUserService().delete(selectedGalleria.getId());
            redirectView("galleria_form", AppBackend.SN_ENTITY, null);
        } else {
            showMessageDeleteEmpty();
        }
    }

    @Override
    public void actionSearch() throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Galleria getSelectedPromo() {
        return selectedGalleria;
    }

    public void setSelectedPromo(Galleria selectedPromo) {
        this.selectedGalleria = selectedPromo;
    }

    public GalleriaSelectableDataModel getGalleriaSelectableDataModel() {
        return galleriaSelectableDataModel;
    }

    public void setGalleriaSelectableDataModel(GalleriaSelectableDataModel galleriaSelectableDataModel) {
        this.galleriaSelectableDataModel = galleriaSelectableDataModel;
    }
}
