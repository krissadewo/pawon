/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.dailycode.pawon.backend.AppBackend;
import org.dailycode.pawon.backend.listener.ActionFormListener;
import org.dailycode.pawon.core.common.Role;
import org.dailycode.pawon.core.entity.Users;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author kris
 */
@Controller("userForm")
@Scope("view")
public class UserFormController extends BaseController implements ActionFormListener, Serializable {

    private Users user;
    private List<String> roles = new ArrayList<>();

    @Override
    public void init() {
        user = (Users) getCurrentEntity();

        //Adding role
        for (Role role : Role.values()) {
            roles.add(role.name());
        }

        if (user == null) {
            user = new Users();
        }
    }

    @Override
    public void actionCancel() throws IOException {
        redirectView("user_list", AppBackend.SN_ENTITY, true);
    }

    @Override
    public void actionSave() throws IOException {
        if (getService().getUserService().saveOrUpdate(user) != null) {
            redirectView("user_list", AppBackend.SN_ENTITY, true);
        } else {
            redirectView(DB_ERROR_VIEW, AppBackend.SN_ENTITY, true);
        }
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }
}
