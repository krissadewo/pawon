/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.backend.controller;

import java.io.IOException;
import java.io.Serializable;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.dailycode.pawon.backend.listener.ActionFormListener;
import org.dailycode.pawon.core.entity.Users;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Controller;

/**
 *
 * @author kris
 */
@Controller("loginForm")
@Scope("view")
public class LoginController extends BaseController implements ActionFormListener, Serializable {

    private Users user;

    @Override
    public void init() {
        if (user == null) {
            user = new Users();
        }
    }

    @Override
    public void actionCancel() throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actionSave() throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void actionLogin() throws ServletException, IOException {

        HttpServletRequest request = (HttpServletRequest) getFacesContext().getExternalContext().getRequest();

        ServletResponse response = ((ServletResponse) getFacesContext().getExternalContext().getResponse());
        getFacesContext().responseComplete();

        RequestDispatcher dispatcher = request.getRequestDispatcher("/security_check");
        dispatcher.forward(request, response);

    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }
}
