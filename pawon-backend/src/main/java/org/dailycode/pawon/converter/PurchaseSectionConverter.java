/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.dailycode.pawon.core.common.Section;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Kris Sadewo
 */
@Component
@Scope("request")
public class PurchaseSectionConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return Section.getSection(value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {       
        return String.valueOf(value);
    }
}
