/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.converter;

import java.io.Serializable;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.dailycode.pawon.core.entity.ItemCategory;
import org.dailycode.pawon.core.dao.service.BaseService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author kris
 * @created Feb 19, 2013
 */
@Component
@Scope("request")
public class OrderedItemCategoryConverter extends BaseService implements Converter, Serializable {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return getItemCategoryService().getById(Long.valueOf(value));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return String.valueOf(((ItemCategory) value).getId());
    }
}
