/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.datamodel;

import java.util.List;
import javax.faces.model.ListDataModel;
import org.dailycode.pawon.core.entity.Room;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
public class RoomSelectableDataModel extends ListDataModel<Room> implements SelectableDataModel<Room> {

    public RoomSelectableDataModel() {
    }

    public RoomSelectableDataModel(List<Room> rooms) {
        super(rooms);
    }

    @Override
    public Object getRowKey(Room object) {
        return object.getName();
    }

    @Override
    public Room getRowData(String rowKey) {
        List<Room> rooms = (List<Room>) getWrappedData();
        for (Room category : rooms) {
            if (category.getName().equals(rowKey)) {
                return category;
            }
        }
        return null;
    }
}
