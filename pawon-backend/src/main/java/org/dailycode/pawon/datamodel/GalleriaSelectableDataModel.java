/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.datamodel;

import java.util.List;
import javax.faces.model.ListDataModel;
import org.dailycode.pawon.core.entity.Galleria;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author kris
 */
public class GalleriaSelectableDataModel extends ListDataModel<Galleria> implements SelectableDataModel<Galleria> {

    public GalleriaSelectableDataModel() {
    }

    public GalleriaSelectableDataModel(List<Galleria> promos) {
        super(promos);
    }

    @Override
    public Object getRowKey(Galleria object) {
        return object.getImage();
    }

    @Override
    public Galleria getRowData(String rowKey) {
        List<Galleria> promos = (List<Galleria>) getWrappedData();
        for (Galleria promo : promos) {
            if (promo.getImage().equals(rowKey)) {
                return promo;
            }
        }
        return null;
    }
}
