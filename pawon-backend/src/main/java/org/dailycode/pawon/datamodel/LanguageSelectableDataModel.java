/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.datamodel;

import java.util.List;
import javax.faces.model.ListDataModel;
import org.dailycode.pawon.core.entity.Language;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
public class LanguageSelectableDataModel extends ListDataModel<Language> implements SelectableDataModel<Language> {

    public LanguageSelectableDataModel() {
    }

    public LanguageSelectableDataModel(List<Language> languages) {
        super(languages);
    }

    @Override
    public Object getRowKey(Language object) {
        return object.getName();
    }

    @Override
    public Language getRowData(String rowKey) {
        List<Language> languages = (List<Language>) getWrappedData();
        for (Language language : languages) {
            if (language.getName().equals(rowKey)) {
                return language;
            }
        }
        return null;
    }
}
