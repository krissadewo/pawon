/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.datamodel;

import java.util.List;
import javax.faces.model.ListDataModel;
import org.dailycode.pawon.core.entity.Users;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author kris
 */
public class UserSelectableDataModel extends ListDataModel<Users> implements SelectableDataModel<Users> {

    public UserSelectableDataModel() {
    }

    public UserSelectableDataModel(List<Users> users) {
        super(users);
    }

    @Override
    public Object getRowKey(Users object) {
        return object.getUsername();
    }

    @Override
    public Users getRowData(String rowKey) {
        List<Users> users = (List<Users>) getWrappedData();
        for (Users user : users) {
            if (user.getUsername().equals(rowKey)) {
                return user;
            }
        }
        return null;
    }
}
