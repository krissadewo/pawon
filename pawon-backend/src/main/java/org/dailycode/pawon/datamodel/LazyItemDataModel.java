/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.datamodel;

import org.dailycode.pawon.backend.AppBackend;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.dailycode.pawon.core.AppCore;
import org.dailycode.pawon.core.common.Section;
import org.dailycode.pawon.core.entity.Item;
import org.dailycode.pawon.core.dao.service.ItemService;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SelectableDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
public class LazyItemDataModel extends LazyDataModel<Item> implements SelectableDataModel<Item>, Serializable {

    private List<Item> items = new ArrayList<>();
    private ItemService itemService;

    public LazyItemDataModel(ItemService itemService) {
        this.itemService = itemService;
    }

    @Override
    public Item getRowData(String rowKey) {
        for (Item item : items) {
            if (item.getCode().equals(rowKey)) {
                return item;
            }
        }
        return null;
    }

    @Override
    public Object getRowKey(Item item) {
        return item.getCode();
    }

    @Override
    public void setRowIndex(final int rowIndex) {
        if (rowIndex == -1 || getPageSize() == 0) {
            super.setRowIndex(-1);
        } else {
            super.setRowIndex(rowIndex % getPageSize());
        }
    }

    @Override
    public List<Item> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, String> filters) {
        Section section = (Section) AppBackend.getInstance().getSessionMapValue(AppBackend.SN_SECTION);
        if (section == null) {
            section = Section.RESTAURANT;
        }
        //filter
        if (!filters.isEmpty()) {
            Item item = new Item();
            item.setCode("");
            item.setName("");

            for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                try {
                    String filterProperty = it.next();
                    String filterValue = filters.get(filterProperty);

                    switch (filterProperty) {
                        case "code":
                            if (filterValue != null) {
                                item.setCode(filterValue);
                            }
                            break;
                        case "name":
                            if (filterValue != null) {
                                item.setName(filterValue);
                            }
                            break;
                    }
                } catch (Exception exception) {
                    AppBackend.getLogger(this).error(exception);
                }
            }
            items = itemService.search(section, first, pageSize, item);
            this.setRowCount(itemService.count(section, item));
        } else {
            if (sortField != null) {
                String orderBy = "ASC";
                if (sortOrder.equals(SortOrder.DESCENDING)) {
                    orderBy = "DESC";
                }
                items = itemService.getBySection(section, first, pageSize, sortField, orderBy);
            } else {
                items = itemService.getBySection(section, first, pageSize, "id", "DESC");
            }
            this.setRowCount(itemService.count(section));
        }

        //sort


        //Add callback if data found or not
        boolean found = false;
        if (items.size() > 0) {
            found = true;
        }
        RequestContext.getCurrentInstance().addCallbackParam("found", found);

        return items;
    }
}
