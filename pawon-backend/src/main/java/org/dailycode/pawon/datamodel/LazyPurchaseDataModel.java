/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.datamodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.dailycode.pawon.backend.AppBackend;
import org.dailycode.pawon.core.AppCore;
import org.dailycode.pawon.core.dao.service.PurchaseService;
import org.dailycode.pawon.core.entity.Purchase;
import org.joda.time.DateTime;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SelectableDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
public class LazyPurchaseDataModel extends LazyDataModel<Purchase> implements SelectableDataModel<Purchase>, Serializable {
    
    private List<Purchase> purchases = new ArrayList<>();
    private PurchaseService purchaseService;
    
    public LazyPurchaseDataModel(PurchaseService purchaseService) {
        this.purchaseService = purchaseService;
    }
    
    @Override
    public Purchase getRowData(String rowKey) {
        for (Purchase purchase : purchases) {
            if (purchase.getRoom().getName().equals(rowKey)) {
                return purchase;
            }
        }
        return null;
    }
    
    @Override
    public Object getRowKey(Purchase purchase) {
        return purchase.getRoom().getName();
    }
    
    @Override
    public void setRowIndex(final int rowIndex) {
        if (rowIndex == -1 || getPageSize() == 0) {
            super.setRowIndex(-1);
        } else {
            super.setRowIndex(rowIndex % getPageSize());
        }
    }
    
    @Override
    public List<Purchase> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, String> filters) {

        //filter
        if (!filters.isEmpty()) {
            Date startDate = new Date();
            Date endDate = new Date();
            String roomName = "";
            String orderNo = "";
            
            for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                try {
                    String filterProperty = it.next();
                    String filterValue = filters.get(filterProperty);
//
                    switch (filterProperty) {
                        case "startDate":
                            if (filterValue != null) {
                                startDate = new DateTime(filterValue).toDate();
                            }
                            break;
                        case "endDate":
                            if (filterValue != null) {
                                endDate = new DateTime(filterValue).toDate();
                            }
                            break;
                        case "roomName":
                            if (filterValue != null) {
                                roomName = filterValue;
                            }
                            break;
                        case "orderNo":
                            if (filterValue != null) {
                                orderNo = filterValue;
                            }
                            break;
                    }
                } catch (Exception exception) {
                    AppBackend.getLogger(this).error(exception);
                }
            }
            if (StringUtils.isNotBlank(roomName) || StringUtils.isNotBlank(orderNo)) {
                purchases = purchaseService.search(first, pageSize, startDate, endDate, roomName, orderNo);
                this.setRowCount(purchaseService.count(startDate, endDate, roomName, orderNo));
            } else {
                purchases = purchaseService.search(first, pageSize, startDate, endDate);
                this.setRowCount(purchaseService.count(startDate, endDate));
            }
        } else {
            if (sortField != null) {
                if (sortField.equals("datePurchase")) {
                    sortField = "date_purchase";
                }
                String orderBy = "ASC";
                if (sortOrder.equals(SortOrder.DESCENDING)) {
                    orderBy = "DESC";
                }
                purchases = purchaseService.getAll(first, pageSize, sortField, orderBy);
            } else {
                purchases = purchaseService.getAll(first, pageSize, "id", "DESC");
            }
            
            this.setRowCount(purchaseService.count());
        }

        //sort


        //Add callback if data found or not
        boolean found = false;
        if (purchases.size() > 0) {
            found = true;
        }
        RequestContext.getCurrentInstance().addCallbackParam("found", found);
        
        return purchases;
    }
}
