/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.entity;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

/**
 *
 * @author Kris Sadewo
 * @date Feb 8, 2013
 */
public class ItemCategory extends ItemObject {

    private Long parentId = 0l;

    public ItemCategory() {
    }

    public ItemCategory(Long id, String name) {
        super(id, name);
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (this.parentId != null ? this.parentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ItemCategory other = (ItemCategory) obj;
        if (this.parentId != other.parentId && (this.parentId == null || !this.parentId.equals(other.parentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
