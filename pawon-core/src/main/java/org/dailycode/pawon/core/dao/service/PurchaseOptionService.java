/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.service;

import java.util.List;
import org.dailycode.pawon.core.dao.PurchaseOptionDAO;
import org.dailycode.pawon.core.entity.PurchaseOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author kris
 * @created Mar 20, 2013
 */
@Service
public class PurchaseOptionService   {

    @Autowired
    private PurchaseOptionDAO purchaseOptionDAO;

    public List<PurchaseOption> getByPurchaseItem(Long id) {
        return purchaseOptionDAO.getByPurchaseItem(id);
    }
}
