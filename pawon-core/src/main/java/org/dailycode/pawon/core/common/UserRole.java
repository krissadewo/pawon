package org.dailycode.pawon.core.common;

/**
 * At least each user has to have one UserRole. 
 * They determine if the user will be granted access to a specific source or not.
 * Each role contains a field to its sub role:
 * <ol>
 *  <li> LEC Administrator
 *  <li> Administrator
 *  <li> Advanced User
 *  <li> User
 * </ol>
 * If a users Role is Administrator the user can also access sources that are free 
 * for users with role "Advanced User" and "User"
 * @author Jens Vogel
 *
 */
public enum UserRole {
	USER("User", null), 
	ADMINISTRATOR("Administrator", null), 
	LEC_ADMINISTRATOR("LEC Administrator", null);

	private String description;

	private UserRole nextLowerLevel;

	private UserRole(String pDescription, UserRole pNextLowerLevel) {
		this.description = pDescription;
		this.nextLowerLevel = pNextLowerLevel;
	}

	public String getDescription() {
		return description;
	}

	public UserRole getNextLowerLevel() {
		return nextLowerLevel;
	}

	/**
	 * for use in web mvc
	 * 
	 * @return
	 */
	public int getOrdinal() {
		return this.ordinal();
	}

	public String toString() {
		return description;
	}

	public static UserRole getRoleForOrdinal(int pOrdinal) {
		for (UserRole role : UserRole.values()) {
			if (pOrdinal == role.ordinal()) {
				return role;
			}
		}
		return null;
	}
	
	public static UserRole getUserRole(String pName){
		if (pName == null) {
			return null;
		}
		for (UserRole role : UserRole.values()) {
			if (pName.toLowerCase().replace(" ", "_").equals(role.name().toLowerCase())) {
				return role;
			}
		}
		return null;
	}
}