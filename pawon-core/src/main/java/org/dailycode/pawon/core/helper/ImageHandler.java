/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.helper;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;
import org.dailycode.pawon.core.AppCore;

/**
 *
 * @author kris
 * @created Feb 20, 2013
 */
public class ImageHandler {

    private static final int IMG_WIDTH = 310;
    private static final int IMG_HEIGHT = 400;

    /**
     *
     * @param data
     * @param name
     * @param imageSource physical location of image
     * @param resizeMedia
     */
    public static void saveImageOnServer(byte[] data, String name, String imageSource, boolean resizeMedia) {
        try {
            InputStream in = new ByteArrayInputStream(data);
            BufferedImage bufferedImage = ImageIO.read(in);
            if (resizeMedia) {
                int type = bufferedImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : bufferedImage.getType();
                BufferedImage resizeImage = resizeImageWithHint(bufferedImage, type);
                ImageIO.write(resizeImage, getFormat(name), new File(imageSource + name));
            } else {
                ImageIO.write(bufferedImage, getFormat(name), new File(imageSource + name));
            }

        } catch (IOException ex) {
            AppCore.getLogger(ImageHandler.class).error(ex);
        }
    }

    private static BufferedImage resizeImage(BufferedImage originalImage, int type) {
        BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
        g.dispose();
        return resizedImage;
    }

    private static BufferedImage resizeImageWithHint(BufferedImage originalImage, int type) {
        BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
        g.dispose();
        g.setComposite(AlphaComposite.Src);

        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        return resizedImage;
    }

    public static void deleteImageOnServer(String imageSource, String name) {
        try {
            File file = new File(imageSource + name);
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception exception) {
        }
    }

    public static String getFormat(String name) {
        String[] result = name.split("\\.");
        if (result.length > 0) {
            return result[result.length - 1];
        }
        return ".jpeg";
    }
}
