/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.entity;

import java.io.Serializable;

/**
 *
 * @author kris
 */
public class Galleria implements Serializable {

    private Long id;
    private boolean active = true;
    private String name;
    private String image;
    private boolean statusDelete;
    private Integer interval;
    private byte[] imageData;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public byte[] getImageData() {
        return imageData;
    }

    public void setImageData(byte[] imageData) {
        this.imageData = imageData;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isStatusDelete() {
        return statusDelete;
    }

    public void setStatusDelete(boolean statusDelete) {
        this.statusDelete = statusDelete;
    }

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    @Override
    public String toString() {
        return "Galleria{" + "id=" + id + ", active=" + active + ", name=" + name + ", image=" + image + ", statusDelete=" + statusDelete + ", interval=" + interval + ", imageData=" + imageData + '}';
    }
}
