/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.impl;

import org.dailycode.pawon.core.dao.LocalizedInformationDAO;
import org.dailycode.pawon.core.entity.Item;
import org.dailycode.pawon.core.entity.ItemCategory;
import org.dailycode.pawon.core.entity.ItemOption;
import org.dailycode.pawon.core.entity.Language;
import org.dailycode.pawon.core.entity.LocalizedInformation;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.dailycode.pawon.core.AppCore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kris
 * @created Feb 21, 2013
 */
@Repository
public class LocalizedInformationDAOImpl implements LocalizedInformationDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     *
     * @param entity
     * @return
     */
    @Override
    public LocalizedInformation save(LocalizedInformation entity) {
        String sql = "INSERT INTO local_information("
                + "title,"
                + "description,"
                + "language_id,"
                + "item_id,"
                + "item_category_id,"
                + "item_option_id) "
                + "VALUES(?,?,?,?,?,?)";
        jdbcTemplate.update(sql, new Object[]{
            entity.getTitle(),
            entity.getDescription(),
            entity.getLanguage().getId(),
            entity.getItem() == null ? null : entity.getItem().getId(),
            entity.getItemCategory() == null ? null : entity.getItemCategory().getId(),
            entity.getItemOption() == null ? null : entity.getItemOption().getId()});

        return entity;
    }

    @Override
    public LocalizedInformation update(LocalizedInformation entity) {
        String sql = "UPDATE local_information SET "
                + "title = ?,"
                + "description = ? "
                + "WHERE id=? ";
        jdbcTemplate.update(sql, new Object[]{
            entity.getTitle(),
            entity.getDescription(),
            entity.getId()});

        return entity;
    }

    @Override
    public void save(final List<LocalizedInformation> localizedInformations) {
        String sql = "INSERT INTO local_information("
                + "title,"
                + "description,"
                + "item_id,"
                + "item_category_id) "
                + "VALUES(?,?,?,?)";

        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                LocalizedInformation localizedInformation = localizedInformations.get(i);
                ps.setString(1, localizedInformation.getTitle());
                ps.setString(2, localizedInformation.getDescription());
                ps.setLong(3, localizedInformation.getItem().getId());
                ps.setLong(4, localizedInformation.getItemCategory().getId());
            }

            @Override
            public int getBatchSize() {
                return localizedInformations.size();
            }
        });
    }

    @Override
    public void update(final List<LocalizedInformation> localizedInformations) {
        String sql = "UPDATE local_information SET "
                + "title = ?, "
                + "description = ? "
                + "WHERE id=?";

        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                LocalizedInformation localizedInformation = localizedInformations.get(i);
                ps.setString(1, localizedInformation.getTitle());
                ps.setString(1, localizedInformation.getDescription());
                ps.setLong(2, localizedInformation.getId());
            }

            @Override
            public int getBatchSize() {
                return localizedInformations.size();
            }
        });
    }

    @Override
    public LocalizedInformation getByItemCategoryAndLanguage(ItemCategory itemCategory, Language language) {
        String sql = "SELECT *FROM local_information WHERE item_category_id = ? AND language_id = ?";
        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{itemCategory.getId(), language.getId()}, new LocalizedInformationRowMapper());
        } catch (EmptyResultDataAccessException exception) {
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;
    }

    @Override
    public LocalizedInformation getByItemAndLanguage(Item item, Language language) {
        String sql = "SELECT *FROM local_information WHERE item_id = ? AND language_id = ?";
        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{item.getId(), language.getId()}, new LocalizedInformationRowMapper());
        } catch (EmptyResultDataAccessException exception) {
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;
    }

    @Override
    public LocalizedInformation getByItemOptionAndLanguage(ItemOption itemOption, Language language) {
        String sql = "SELECT *FROM local_information WHERE item_option_id = ? AND language_id = ?";
        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{itemOption.getId(), language.getId()}, new LocalizedInformationRowMapper());
        } catch (EmptyResultDataAccessException exception) {
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;
    }

    @Override
    public List<LocalizedInformation> getAll() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public LocalizedInformation getById(Long id) {
        String sql = "SELECT *FROM local_information WHERE id = ?";
        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{id}, new LocalizedInformationRowMapper());
        } catch (EmptyResultDataAccessException exception) {
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;
    }

    @Override
    public int count() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean delete(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean deleteLocalItem(Long id) {
        String sql = "DELETE local_information WHERE item_id = ?";
        try {
            jdbcTemplate.update(sql, new Object[]{id});
            return true;
        } catch (EmptyResultDataAccessException exception) {
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return false;
    }

    @Override
    public boolean deleteLocalItemCategory(Long id) {
        String sql = "DELETE local_information WHERE item_category_id = ?";
        try {
            jdbcTemplate.update(sql, new Object[]{id});
            return true;
        } catch (EmptyResultDataAccessException exception) {
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return false;
    }

    @Override
    public boolean deleteLocalItemOptionCategory(Long id) {
        String sql = "DELETE local_information WHERE item_option_d = ?";
        try {
            jdbcTemplate.update(sql, new Object[]{id});
            return true;
        } catch (EmptyResultDataAccessException exception) {
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return false;
    }

    class LocalizedInformationRowMapper implements RowMapper<LocalizedInformation> {

        @Override
        public LocalizedInformation mapRow(ResultSet rs, int rowNum) throws SQLException {
            LocalizedInformation localizedInformation = new LocalizedInformation();
            localizedInformation.setId(rs.getLong("id"));
            localizedInformation.setTitle(rs.getString("title"));
            localizedInformation.setDescription(rs.getString("description"));

            Language language = new Language();
            language.setId(rs.getLong("language_id"));
            localizedInformation.setLanguage(language);

            return localizedInformation;
        }
    }
}
