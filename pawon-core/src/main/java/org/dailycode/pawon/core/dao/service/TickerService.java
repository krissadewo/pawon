/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.service;

import org.dailycode.pawon.core.dao.TickerDAO;
import org.dailycode.pawon.core.entity.Ticker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author kris
 */
@Service
public class TickerService {

    @Autowired
    private TickerDAO tickerDAO;

    public Ticker saveOrUpdate(Ticker ticker) {
        if (ticker.getId() == null) {
            return tickerDAO.save(ticker);
        }
        return tickerDAO.update(ticker);
    }

    public boolean delete(Ticker ticker) {
        return tickerDAO.delete(ticker.getId());
    }

    public Ticker getTicker() {
        if (tickerDAO.getAll().isEmpty()) {
            return new Ticker();
        }
        return tickerDAO.getAll().get(0);
    }
}
