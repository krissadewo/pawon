package org.dailycode.pawon.core.entity;

import java.util.Date;

public class Customer {

    public static enum CustomerStatus {

        INACTIVE, OPEN_BILL, NO_COMSUME;

        public static Customer.CustomerStatus getCustomerStatus(String pName) {
            if (pName == null) {
                return null;
            }
            for (CustomerStatus status : CustomerStatus.values()) {
                if (status.name().toLowerCase().equals(pName.toLowerCase())) {
                    return status;
                }
            }
            return null;
        }
    }
    private Room room;
    private String firstName;
    private String title;
    private String vipCode;
    private Date arrivalDate = new Date();
    private Date departureDate = null;
    private CustomerStatus status;
    private String language;
    private Long created;

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVipCode() {
        return vipCode;
    }

    public void setVipCode(String vipCode) {
        this.vipCode = vipCode;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public CustomerStatus getStatus() {
        return status;
    }

    public void setStatus(CustomerStatus status) {
        this.status = status;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }
}
