/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.dailycode.pawon.core.AppCore;
import org.dailycode.pawon.core.common.Role;
import org.dailycode.pawon.core.dao.UsesrDAO;
import org.dailycode.pawon.core.entity.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kris
 */
@Repository
public class UsersDAOImpl implements UsesrDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Users save(Users entity) {
        String sql = "INSERT INTO users(username,password,salt,role) VALUES(?,?,?,?)";
        try {
            jdbcTemplate.update(sql, new Object[]{
                entity.getUsername(),
                entity.getPassword(),
                entity.getSalt(),
                entity.getRole().ordinal()});
            return entity;
        } catch (Exception exception) {
            AppCore.getLogger(this).error(exception);
        }
        return null;
    }

    @Override
    public Users update(Users entity) {
        String sql = "UPDATE users SET username=?, password=?, salt=?, role=? WHERE id=?";
        try {
            jdbcTemplate.update(sql, new Object[]{
                entity.getUsername(),
                entity.getPassword(),
                entity.getSalt(),
                entity.getRole().ordinal(),
                entity.getId()});
            return entity;
        } catch (Exception exception) {
            AppCore.getLogger(this).error(exception);
        }
        return null;
    }

    @Override
    public boolean delete(Long id) {
        String sql = "DELETE FROM users WHERE id=?";
        try {
            jdbcTemplate.update(sql, new Object[]{id});
            return true;
        } catch (Exception exception) {
            AppCore.getLogger(this).error(exception);
        }
        return false;
    }

    @Override
    public List<Users> getAll() {
        String sql = "SELECT *FROM users";
        return jdbcTemplate.query(sql, new UserRowMapper());
    }

    @Override
    public Users getById(Long id) {
        String sql = "SELECT *FROM users WHERE id = ?";
        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{id}, new UserRowMapper());
        } catch (EmptyResultDataAccessException exception) {
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;
    }

    @Override
    public int count() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Users getUserByUsernameAndPassword(Users user) {
        String sql = "SELECT *FROM users WHERE username = ? AND password = ?";
        try {
            return jdbcTemplate.queryForObject(sql, new UserRowMapper());
        } catch (EmptyResultDataAccessException exception) {
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;

    }

    @Override
    public Users findByUsername(String username) {
        String sql = "SELECT *FROM users WHERE username = ? ";
        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{username}, new UserRowMapper());
        } catch (EmptyResultDataAccessException exception) {
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;
    }

    class UserRowMapper implements RowMapper<Users> {

        @Override
        public Users mapRow(ResultSet rs, int rowNum) throws SQLException {
            Users user = new Users();
            user.setId(rs.getLong("id"));
            user.setUsername(rs.getString("username"));
            user.setPassword(rs.getString("password"));
            user.setSalt(rs.getString("salt"));
            user.setRole(Role.values()[rs.getInt("role")]);
            return user;
        }
    }
}
