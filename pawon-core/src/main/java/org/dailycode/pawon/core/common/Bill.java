package org.dailycode.pawon.core.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Bill implements Serializable {

	/** */
	private static final long serialVersionUID = 6650755156492594853L;

	private String customerReservationNumber;
	private String roomName;
	/** total bill from MYOH **/
	private double total;
	private List<BillItem> items = new ArrayList<BillItem>();
	
	public List<BillItem> getItems() {
		return items;
	}
	
	public String getCustomerReservationNumber() {
		return customerReservationNumber;
	}

	public void setCustomerReservationNumber(String customerReservationNumber) {
		this.customerReservationNumber = customerReservationNumber;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public double getTotal(){
		return this.total;
	}
	
	public void setTotal(double total){
		this.total = total;
	}
	
	public double getSum() {
		Double sum = 0d;
		for(BillItem item : this.getItems()) {
			sum += item.getPrice();
		}
		return sum;
	}
	
}
