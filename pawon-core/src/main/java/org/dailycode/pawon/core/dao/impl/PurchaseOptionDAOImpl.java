/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.dailycode.pawon.core.AppCore;
import org.dailycode.pawon.core.dao.PurchaseOptionDAO;
import org.dailycode.pawon.core.entity.ItemOption;
import org.dailycode.pawon.core.entity.PurchaseOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kris
 * @created Mar 19, 2013
 */
@Repository
public class PurchaseOptionDAOImpl implements PurchaseOptionDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public PurchaseOption save(PurchaseOption entity) {
        String sql = "INSERT INTO purchase_option("
                + "name,"
                + "price,"
                + "item_option_id,"
                + "purchase_item_id) "
                + "VALUES(?,?,?,?)";
        try {
            jdbcTemplate.update(sql, new Object[]{
                entity.getName(),
                entity.getPrice(),
                entity.getItemOption().getId(),
                entity.getPurchaseItem().getId()
            });
            return entity;
        } catch (Exception exception) {
            AppCore.getLogger(this).error(exception);
        }
        return null;
    }

    @Override
    public PurchaseOption update(PurchaseOption entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteByPurchaseItem(Long purchaseItemId) {
        String sql = "DELETE FROM purchase_option WHERE purchase_item_id = ?";
        try {
            jdbcTemplate.update(sql, new Object[]{purchaseItemId});
        } catch (Exception exception) {
            AppCore.getLogger(this).error(exception);
        }
    }

    @Override
    public List<PurchaseOption> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PurchaseOption getById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int count() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<PurchaseOption> getByPurchaseItem(Long id) {
        String sql = "SELECT po.*, io.* FROM purchase_option po "
                + "INNER JOIN item_option io ON io.id = po.item_option_id "
                + "WHERE po.purchase_item_id = ?";
        return jdbcTemplate.query(sql, new Object[]{id}, new PurchaseOptionRowMapper());
    }

    @Override
    public boolean delete(Long entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    class PurchaseOptionRowMapper implements RowMapper<PurchaseOption> {

        @Override
        public PurchaseOption mapRow(ResultSet rs, int rowNum) throws SQLException {
            ItemOption itemOption = new ItemOption();
            itemOption.setId(rs.getLong("item_option_id"));
            itemOption.setName(rs.getString("name"));

            PurchaseOption purchaseOption = new PurchaseOption();
            purchaseOption.setId(rs.getLong("id"));
            purchaseOption.setName(rs.getString("name"));
            purchaseOption.setPrice(rs.getDouble("price"));
            purchaseOption.setItemOption(itemOption);

            return purchaseOption;
        }
    }
}
