/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao;

import org.dailycode.pawon.core.entity.Item;
import org.dailycode.pawon.core.entity.ItemCategory;
import org.dailycode.pawon.core.entity.ItemOption;
import org.dailycode.pawon.core.entity.Language;
import org.dailycode.pawon.core.entity.LocalizedInformation;
import java.util.List;

/**
 *
 * @author kris
 * @created Feb 21, 2013
 */
public interface LocalizedInformationDAO extends BaseDAO<LocalizedInformation> {

    void save(List<LocalizedInformation> localizedInformations);

    void update(List<LocalizedInformation> localizedInformations);

    LocalizedInformation getByItemCategoryAndLanguage(ItemCategory itemCategory, Language language);

    LocalizedInformation getByItemAndLanguage(Item item, Language language);

    public LocalizedInformation getByItemOptionAndLanguage(ItemOption itemOption, Language language);

    boolean deleteLocalItem(Long id);

    boolean deleteLocalItemCategory(Long id);

    boolean deleteLocalItemOptionCategory(Long id);
}
