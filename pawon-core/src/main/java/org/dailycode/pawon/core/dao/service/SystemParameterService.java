/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.service;

import java.util.List;
import org.dailycode.pawon.core.dao.SystemParameterDAO;
import org.dailycode.pawon.core.entity.SystemParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kris
 */
@Service
public class SystemParameterService {

    @Autowired
    private SystemParameterDAO systemParameterDAO;

    @Transactional
    public SystemParameter saveOrUpdate(SystemParameter systemParameter) {
        if (systemParameter.getId() == null) {
            return systemParameterDAO.save(systemParameter);
        }
        return systemParameterDAO.update(systemParameter);
    }

    @Transactional
    public boolean delete(Long id) {
        return systemParameterDAO.delete(id);
    }

    public List<SystemParameter> getAll() {
        return systemParameterDAO.getAll();
    }

    public String getImageURL() {
        return systemParameterDAO.getImageURL();
    }

    public String getImageSource() {
        return systemParameterDAO.getImageSource();
    }

    public String getPrinterName() {
        return systemParameterDAO.getPrinterName();
    }

    public Integer getPrinterCopy() {
        return systemParameterDAO.getPrintCopy();
    }
}
