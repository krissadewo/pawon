/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.impl;

import java.sql.PreparedStatement;
import org.dailycode.pawon.core.dao.LanguageDAO;
import org.dailycode.pawon.core.entity.Language;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.dailycode.pawon.core.AppCore;
import org.dailycode.pawon.core.common.LanguageCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kris
 * @created Feb 12, 2013
 */
@Repository
public class LanguageDAOImpl implements LanguageDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Language save(Language entity) {
        String sql = "INSERT INTO language("
                + "active, "
                + "code, "
                + "name, "
                + "default_language, "
                + "image) "
                + "VALUES(?,?,?,?,?) ";
        try {
            jdbcTemplate.update(sql, new Object[]{
                entity.isActive(),
                entity.getLanguageCode().name(),
                entity.getName(),
                entity.isDefaultLanguage(),
                entity.getImage()
            });
            return entity;
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;
    }

    @Override
    public Language update(Language entity) {
        String sql = "UPDATE language "
                + "SET active = ?, "
                + "code = ?, "
                + "name = ?, "
                + "default_language = ?, "
                + "image = ? "
                + "WHERE id = ? ";
        try {
            jdbcTemplate.update(sql, new Object[]{
                entity.isActive(),
                entity.getLanguageCode().name(),
                entity.getName(),
                entity.isDefaultLanguage(),
                entity.getImage(),
                entity.getId()
            });
            return entity;
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;
    }

    @Override
    public void updateDefaultLanguage(final List<Language> languages) {
        String sql = "UPDATE language "
                + "SET default_language = false "
                + "WHERE id = ? ";
        try {
            jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
                @Override
                public void setValues(PreparedStatement ps, int i) throws SQLException {
                    ps.setLong(1, languages.get(i).getId());
                }

                @Override
                public int getBatchSize() {
                    return languages.size();
                }
            });
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }

    }

    @Override
    public List<Language> getAll() {
        String sql = "SELECT *FROM language WHERE status_delete = false";
        return jdbcTemplate.query(sql, new LanguageRowMapper());
    }

    @Override
    public List<Language> getActiveLanguage() {
        String sql = "SELECT *FROM language WHERE status_delete = false AND active = true";
        return jdbcTemplate.query(sql, new LanguageRowMapper());
    }

    @Override
    public Language getById(Long id) {
        String sql = "SELECT *FROM languange WHERE id = ? AND status_delete = false";
        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{id}, new LanguageRowMapper());
        } catch (EmptyResultDataAccessException e) {
        } catch (DataAccessException e) {
            AppCore.getLogger(this).error(e);
        }
        return null;
    }

    @Override
    public int count() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean delete(Long id) {
        String sql = "UPDATE language "
                + "SET status_delete  = ? "
                + "WHERE id = ? ";
        try {
            jdbcTemplate.update(sql, new Object[]{true, id});
            return true;
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return false;
    }

    class LanguageRowMapper implements RowMapper<Language> {

        @Override
        public Language mapRow(ResultSet rs, int rowNum) throws SQLException {
            Language language = new Language();
            language.setId(rs.getLong("id"));
            language.setLanguageCode(LanguageCode.valueOf(rs.getString("code")));
            language.setName(rs.getString("name"));
            language.setDefaultLanguage(rs.getBoolean("default_language"));
            language.setActive(rs.getBoolean("active"));
            language.setImage(rs.getString("image"));
            language.setStatusDelete(rs.getBoolean("status_delete"));
            return language;
        }
    }
}
