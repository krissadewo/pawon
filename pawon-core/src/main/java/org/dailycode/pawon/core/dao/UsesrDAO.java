/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao;

import org.dailycode.pawon.core.entity.Users;

/**
 *
 * @author kris
 */
public interface UsesrDAO extends BaseDAO<Users> {

    Users getUserByUsernameAndPassword(Users user);

    public Users findByUsername(String username);
}
