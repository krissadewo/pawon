/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao;

import org.dailycode.pawon.core.entity.SystemParameter;

/**
 *
 * @author kris
 */
public interface SystemParameterDAO extends BaseDAO<SystemParameter> {

    public String getImageURL();

    public String getImageSource();

    public String getPrinterName();

    public Integer getPrintCopy();
}
