/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dailycode.pawon.core.dao;

import org.dailycode.pawon.core.entity.Image;

/**
 *
 * @author kris
 * @created Feb 14, 2013
 */
public interface ImageDAO extends BaseDAO<Image> {

}
