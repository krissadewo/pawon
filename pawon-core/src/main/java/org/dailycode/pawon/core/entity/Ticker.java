/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.entity;

import java.io.Serializable;

/**
 *
 * @author kris
 */
public class Ticker implements Serializable {

    private Long id;
    private String message;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Ticker{" + "id=" + id + ", message=" + message + '}';
    }
}
