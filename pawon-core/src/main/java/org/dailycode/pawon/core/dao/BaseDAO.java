package org.dailycode.pawon.core.dao;

import java.util.List;

/**
 *
 * @author Kris Sadewo <krissadewo@ossys.com>
 */
public interface BaseDAO<T> {

    T save(final T entity);

    T update(final T entity);

    boolean delete(final Long id);

    /**
     * Get all data from db without any parameter
     * <p>Equal with SELECT *FROM t</p>
     *
     * @return
     */
    List<T> getAll();

    /**
     * Get data with specific ID/PK
     *
     * @param id
     * @return
     */
    T getById(final Long id);

    int count();
}
