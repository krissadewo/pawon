/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.service;

import java.util.Date;
import java.util.List;
import org.dailycode.pawon.core.dao.ReportDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author kris
 */
@Service
public class ReportService {

    @Autowired
    private ReportDAO reportDAO;

    public List getPurchaseReport(Date startDate, Date endDate) {
        return reportDAO.getPurchaseReport(startDate, endDate);
    }
}
