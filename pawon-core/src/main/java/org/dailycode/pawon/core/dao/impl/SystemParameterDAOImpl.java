/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.dailycode.pawon.core.AppCore;
import org.dailycode.pawon.core.dao.SystemParameterDAO;
import org.dailycode.pawon.core.entity.SystemParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kris
 */
@Repository
public class SystemParameterDAOImpl implements SystemParameterDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public SystemParameter save(SystemParameter entity) {
        String sql = "INSERT INTO system_parameter("
                + "image_url,"
                + "image_source,"
                + "print_copy,"
                + "printer_name) "
                + "VALUES(?,?,?,?) ";
        try {
            jdbcTemplate.update(sql, new Object[]{
                entity.getImageURL(),
                entity.getImageSource(),
                entity.getPrintCopy(),
                entity.getPrinterName()});
            return entity;
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;

    }

    @Override
    public SystemParameter update(SystemParameter entity) {
        String sql = "UPDATE system_parameter SET "
                + "image_url = ?, "
                + "image_source = ?,"
                + "print_copy = ? ,"
                + "printer_name = ? "
                + "WHERE id = ?";
        try {
            jdbcTemplate.update(sql, new Object[]{
                entity.getImageURL(),
                entity.getImageSource(),
                entity.getPrintCopy(),
                entity.getPrinterName(),
                entity.getId()});
            return entity;
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;
    }

    @Override
    public boolean delete(Long id) {
        String sql = "DELETE FROM system_parameter WHERE id = ?";
        try {
            jdbcTemplate.update(sql, new Object[]{id});
            return true;
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return false;
    }

    @Override
    public List<SystemParameter> getAll() {
        String sql = "SELECT *FROM system_parameter";
        return jdbcTemplate.query(sql, new SystemParameterRowMapper());
    }

    @Override
    public SystemParameter getById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int count() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getImageURL() {
        String sql = "SELECT image_url FROM system_parameter";
        try {
            return jdbcTemplate.queryForObject(sql, String.class);
        } catch (EmptyResultDataAccessException exception) {
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;
    }

    @Override
    public String getImageSource() {
        String sql = "SELECT image_source FROM system_parameter";
        try {
            return jdbcTemplate.queryForObject(sql, String.class);
        } catch (EmptyResultDataAccessException exception) {
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;
    }

    @Override
    public String getPrinterName() {
        String sql = "SELECT printer_name FROM system_parameter";
        try {
            return jdbcTemplate.queryForObject(sql, String.class);
        } catch (EmptyResultDataAccessException exception) {
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;
    }

    @Override
    public Integer getPrintCopy() {
        String sql = "SELECT print_copy FROM system_parameter";
        try {
            return jdbcTemplate.queryForObject(sql, Integer.class);
        } catch (EmptyResultDataAccessException exception) {
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;
    }

    class SystemParameterRowMapper implements RowMapper<SystemParameter> {

        @Override
        public SystemParameter mapRow(ResultSet rs, int rowNum) throws SQLException {
            SystemParameter systemParameter = new SystemParameter();
            systemParameter.setId(rs.getLong("id"));
            systemParameter.setImageURL(rs.getString("image_url"));
            systemParameter.setImageSource(rs.getString("image_source"));
            systemParameter.setPrintCopy(rs.getInt("print_copy"));
            return systemParameter;
        }
    }
}
