package org.dailycode.pawon.core.common;

public enum Role {

    ROLE_ADMIN,
    ROLE_GENERAL;

    public static Role getSection(String pName) {
        if (pName == null) {
            return null;
        }
        for (Role role : Role.values()) {
            if (pName.toLowerCase().contains(role.name().toLowerCase())) {
                return role;
            }
        }
        return null;
    }
}
