package org.dailycode.pawon.core;

import org.apache.log4j.Logger;

/**
 *
 * @author kris
 */
public class AppCore {

    public static Logger getLogger(Object o) {
        return Logger.getLogger(o.getClass());
    }
}
