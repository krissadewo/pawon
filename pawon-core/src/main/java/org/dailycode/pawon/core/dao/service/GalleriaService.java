/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.service;

import java.util.List;
import org.dailycode.pawon.core.dao.GalleriaDAO;
import org.dailycode.pawon.core.dao.SystemParameterDAO;
import org.dailycode.pawon.core.entity.Galleria;
import org.dailycode.pawon.core.helper.ImageHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author kris
 */
@Service
public class GalleriaService {

    @Autowired
    private GalleriaDAO promoDAO;
    @Autowired
    private SystemParameterDAO systemParameterDAO;

    public Galleria saveOrUpdate(Galleria promo) {
        if (promo.getImageData() != null) {
            ImageHandler.saveImageOnServer(promo.getImageData(), promo.getImage(), systemParameterDAO.getImageSource(), false);
        }
        if (promo.getId() == null) {
            return promoDAO.save(promo);
        }
        return promoDAO.update(promo);
    }

    public boolean delete(Galleria promo) {
        return promoDAO.delete(promo.getId());
    }

    /**
     *
     * @param isActive all active promo
     * @return All promo with active or not
     */
    public List<Galleria> getAll(boolean isActive) {
        if (isActive) {
            return promoDAO.getAllActive();
        }
        return promoDAO.getAll();
    }
}
