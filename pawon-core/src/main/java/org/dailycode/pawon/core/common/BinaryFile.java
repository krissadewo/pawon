//$URL: $
//$Id: $
package org.dailycode.pawon.core.common;

import java.io.Serializable;

/**
 * Common interface for binary data
 */
public interface BinaryFile extends Serializable {

  /**
   * Binary data
   * @return Attribute value.
   */
  byte[] getBinaryData();

  /**
   * set Binary data
   * @param pValue Attribute value.
   */
  void setBinaryData(byte[] pValue);

  /**
   * File name. Required.
   * @return Attribute value.
   */
  String getFileName();

  /**
   * File name. Required.
   * @param pValue Attribute value.
   */
  void setFileName(String pValue);

  /**
   * File size (in Bytes). Required.
   * @return Attribute value.
   */
  int getFileSize();

  /**
   * File size (in Bytes). Required.
   * @param pValue Attribute value.
   */
  void setFileSize(int pValue);

  /**
   * Mime Type/Content Type. Required.
   * @return Attribute value.
   */
  String getMimeType();

  /**
   * Mime Type/Content Type. Required.
   * @param pValue Attribute value.
   */
  void setMimeType(String pValue);

}
