/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.service;

import org.dailycode.pawon.core.common.Section;
import org.dailycode.pawon.core.dao.LocalizedInformationDAO;
import org.dailycode.pawon.core.dao.ItemOptionDAO;
import org.dailycode.pawon.core.entity.Item;
import org.dailycode.pawon.core.entity.ItemOption;
import org.dailycode.pawon.core.entity.LocalizedInformation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kris
 * @created Mar 5, 2013
 */
@Service
public class ItemOptionService {

    @Autowired
    private ItemOptionDAO itemOptionDAO;
    @Autowired
    private LocalizedInformationDAO localizedInformationDAO;

    @Transactional
    public ItemOption saveOrUpdate(ItemOption itemOption) {
        if (itemOption.getId() == null) {
            itemOption = itemOptionDAO.save(itemOption);
            if (itemOption != null) {
                for (LocalizedInformation localizedInformation : itemOption.getLocalizedInformations()) {
                    localizedInformation.setItemOption(itemOption);
                    localizedInformationDAO.save(localizedInformation);
                }
            }
            return itemOption;
        } else {
            itemOption = itemOptionDAO.update(itemOption);
            for (LocalizedInformation localizedInformation : itemOption.getLocalizedInformations()) {
                localizedInformation.setItemOption(itemOption);
                if (localizedInformationDAO.getById(localizedInformation.getId()) == null) {
                    localizedInformationDAO.save(localizedInformation);
                } else {
                    localizedInformationDAO.update(localizedInformation);
                }
            }
            return itemOption;
        }
    }

    @Transactional
    public void delete(Long id) {
        deleteChildCategory(id);
        itemOptionDAO.delete(id);

    }

    @Transactional
    private void deleteChildCategory(Long id) {
        itemOptionDAO.delete(id);
        for (ItemOption ic : itemOptionDAO.getByParent(id)) {
            deleteChildCategory(ic.getId());
        }
    }

    /**
     *
     * @param purchaseSection
     * @return
     */
    public List<ItemOption> getByPurchaseSection(Section purchaseSection) {
        return itemOptionDAO.getByPurchaseSection(purchaseSection);
    }

    /**
     *
     * @param id
     * @return
     */
    public List<ItemOption> getByParent(Long id) {
        return itemOptionDAO.getByParent(id);
    }

    /**
     *
     * @param id
     * @param isActive
     * @return
     */
    public List<ItemOption> getByParent(Long id, boolean isActive) {
        return itemOptionDAO.getByParent(id, isActive);
    }

    /**
     *
     * @param id
     * @param languageId
     * @param isActive
     * @return
     */
    public List<ItemOption> getByParent(Long id, Long languageId, boolean isActive) {
        return itemOptionDAO.getByParent(id, languageId, isActive);
    }

    public ItemOption getById(Long id) {
        return itemOptionDAO.getById(id);
    }

    public ItemOption getById(Long id, Long languageId) {
        return itemOptionDAO.getById(id, languageId);
    }

    /**
     * Option dikelompokan didalam sebuah kategori option... misal kategori
     * option tingkat kepedasan bisa terdiri dari pedas sekali, cukup dll....
     *
     * @param id
     * @param languageId
     * @return
     */
    public List<ItemOption> getByItem(Long id, Long languageId) {
        return itemOptionDAO.getByItem(id, languageId);
    }

    public List<ItemOption> getByItem(Item item) {
        return itemOptionDAO.getByItem(item);
    }

    /**
     *
     * @param purchaseSection
     * @param pItemOption
     * @param topLevel Level is a length of categories<br> Top level mean : If
     * selected category C, show Only the top level of C (A > B) else if top
     * level is false then show the leven like (A > B > C)
     * @return parent of all categories with specific format (A > B > C)
     */
    public ItemOption getOrganizedParent(Section purchaseSection, ItemOption pItemOption, boolean topLevel) {
        ItemOption itemOption = getByIdAndPurchaseSection(pItemOption.getId(), purchaseSection);

        if (itemOption != null) {
            StringBuilder sb = new StringBuilder();
            String organizedParent = getParent(itemOption.getId(), sb);

            String[] results = organizedParent.split(">");
            StringBuilder itemCategoryName = new StringBuilder();


            int level = results.length;

            if (topLevel) {
                level -= 1;
            }

            for (int i = 0; i < level; i++) {
                itemCategoryName.append(results[i]);
                itemCategoryName.append(" > ");
            }

            if (itemCategoryName.length() > 0) {
                itemOption.setName(itemCategoryName.toString().substring(0, itemCategoryName.length() - 2));
            }
        }

        return itemOption;
    }

    private String getParent(Long parentId, StringBuilder sb) {
        ItemOption itemOption = itemOptionDAO.getById(parentId);
        if (itemOption != null) {
            getParent(itemOption.getParentId(), sb);
            sb.append(itemOption.getName());
            sb.append(">");
            return sb.toString();
        }
        return null;
    }

    private ItemOption getByIdAndPurchaseSection(Long id, Section purchaseSection) {
        return itemOptionDAO.getByIdAndPurchaseSection(id, purchaseSection);
    }

    public List<ItemOption> getByParentAndPurchaseSection(Long id, Section purchaseSection) {
        return itemOptionDAO.getByParentAndPurchaseSection(id, purchaseSection);
    }
}
