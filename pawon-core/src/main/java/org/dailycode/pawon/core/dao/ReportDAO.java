/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author kris
 */
public interface ReportDAO {

    public List getPurchaseReport(Date startDate, Date endDate);
}
