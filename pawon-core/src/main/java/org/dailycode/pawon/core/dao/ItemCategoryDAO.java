/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao;

import java.util.List;
import org.dailycode.pawon.core.common.Section;
import org.dailycode.pawon.core.entity.ItemCategory;

/**
 *
 * @author Kris Sadewo
 * @date Feb 7, 2013
 */
public interface ItemCategoryDAO extends BaseDAO<ItemCategory> {

    Integer count(Long parentId, Section section);

    /**
     *
     * @param parentId
     * @return
     */
    List<ItemCategory> getByParent(Long parentId);

    /**
     *
     * @param parentId
     * @param languageId
     * @return
     */
    List<ItemCategory> getByParent(Long parentId, Long languageId);

    /**
     *
     * @param parentId
     * @param section
     * @return
     */
    List<ItemCategory> getByParentAndSection(Long parentId, Section section);

    /**
     *
     * @param id
     * @param section
     * @return
     */
    ItemCategory getByIdAndSection(Long id, Section section);

    /**
     *
     * @param section
     * @return
     */
    List<ItemCategory> getBySection(Section section);

    /**
     *
     * @param section
     * @param languageId
     * @return
     */
    List<ItemCategory> getBySection(Section section, Long languageId);

    /**
     * Batch delete
     * @param itemCategorys 
     */
    void delete(List<ItemCategory> itemCategorys);
}
