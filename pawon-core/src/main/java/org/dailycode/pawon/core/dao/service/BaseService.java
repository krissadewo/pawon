/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Kris Sadewo
 */
@Service
public class BaseService {

    @Autowired
    private LanguageService languageService;
    @Autowired
    private ItemCategoryService itemCategoryService;
    @Autowired
    private LocalizedInformationService localizedInformationService;
    @Autowired
    private ItemService itemService;
    @Autowired
    private ItemOptionService itemOptionService;
    @Autowired
    private PurchaseService purchaseService;
    @Autowired
    private RoomService roomService;
    @Autowired
    private PurchaseItemService purchaseItemService;
    @Autowired
    private PurchaseOptionService purchaseOptionService;
    @Autowired
    private SystemParameterService systemParameterService;
    @Autowired
    private UserService userService;
    @Autowired
    private ReportService reportService;
    @Autowired
    private GalleriaService galleriaService;
    @Autowired
    private TickerService tickerService;

    public TickerService getTickerService() {
        return tickerService;
    }

    public GalleriaService getGalleriaService() {
        return galleriaService;
    }

    public void setPromoService(GalleriaService promoService) {
        this.galleriaService = promoService;
    }

    public ReportService getReportService() {
        return reportService;
    }

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

    public UserService getUserService() {
        return userService;
    }

    public SystemParameterService getSystemParameterService() {
        return systemParameterService;
    }

    public PurchaseOptionService getPurchaseOptionService() {
        return purchaseOptionService;
    }

    public PurchaseItemService getPurchaseItemService() {
        return purchaseItemService;
    }

    public RoomService getRoomService() {
        return roomService;
    }

    public PurchaseService getPurchaseService() {
        return purchaseService;
    }

    public ItemOptionService getItemOptionService() {
        return itemOptionService;
    }

    public ItemService getItemService() {
        return itemService;
    }

    public LocalizedInformationService getLocalizedInformationService() {
        return localizedInformationService;
    }

    public LanguageService getLanguageService() {
        return languageService;
    }

    public ItemCategoryService getItemCategoryService() {
        return itemCategoryService;
    }
}
