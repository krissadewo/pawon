/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao;

import org.dailycode.pawon.core.entity.Room;

/**
 *
 * @author kris
 * @created Mar 19, 2013
 */
public interface RoomDAO extends BaseDAO<Room> {

    Room getByName(String name);
}
