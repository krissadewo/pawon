/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.dailycode.pawon.core.common.Section;

/**
 *
 * @author kris
 * @created Mar 4, 2013
 */
public class ItemOption implements Serializable {

    private Long id;
    private boolean active = true;
    private String name;
    private Long parentId;
    private Integer orderNumber;
    private Section section;
    private Double price;
    private LocalizedInformation localizedInformation;
    private List<LocalizedInformation> localizedInformations = new ArrayList<>();

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public List<LocalizedInformation> getLocalizedInformations() {
        return localizedInformations;
    }

    public void setLocalizedInformations(List<LocalizedInformation> localizedInformations) {
        this.localizedInformations = localizedInformations;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public LocalizedInformation getLocalizedInformation() {
        return localizedInformation;
    }

    public void setLocalizedInformation(LocalizedInformation localizedInformation) {
        this.localizedInformation = localizedInformation;
    }

    @Override
    public String toString() {
        return "ItemOption{" + "id=" + id + ", active=" + active + ", name=" + name + ", parentId=" + parentId + ", orderNumber=" + orderNumber + ", section=" + section + ", price=" + price + ", localizedInformation=" + localizedInformation + ", localizedInformations=" + localizedInformations + '}';
    }
}
