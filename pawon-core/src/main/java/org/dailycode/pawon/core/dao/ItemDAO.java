/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao;

import org.dailycode.pawon.core.common.Section;
import org.dailycode.pawon.core.entity.Item;
import java.util.List;

/**
 *
 * @author kris
 * @created Feb 24, 2013
 */
public interface ItemDAO extends BaseDAO<Item> {

    /**
     *
     * @param section
     * @param start
     * @param pageSize
     * @param entity
     * @return
     */
    List<Item> search(Section section, Integer start, Integer pageSize, Item entity);

    /**
     *
     * @param section
     * @param start
     * @param pageSize
     * @param sortField
     * @param orderBy
     * @return
     */
    List<Item> getBySection(Section section, Integer start, Integer pageSize, String sortField, String orderBy);

    /**
     *
     * @param categoryId
     * @return
     */
    List<Item> getByCategory(Long categoryId);

    /**
     *
     * @param categoryId
     * @param languageId
     * @return
     */
    List<Item> getByCategory(Long categoryId, Long languageId);

    /**
     *
     * @param section
     * @param entity
     * @return
     */
    int count(Section section, Item entity);

    /**
     *
     * @param purchaseSection
     * @return
     */
    int count(Section purchaseSection);

    /**
     *
     * @param start
     * @param end
     * @param sortField
     * @param orderBy
     * @return
     */
    List<Item> getAll(final Integer start, final Integer pageSize, final String sortField, final String orderBy);

    /**
     *
     * @param item
     */
    void saveItem2Option(Item item);

    /**
     *
     * @param item
     */
    void deleteItem2Option(Item item);

    /**
     *
     * @param id
     * @param languageId
     * @return
     */
    Item getById(Long id, Long languageId);

    void delete(List<Item> items);

    Item getByCode(String code);
}
