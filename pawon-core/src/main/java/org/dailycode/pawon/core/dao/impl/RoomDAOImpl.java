/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.dailycode.pawon.core.AppCore;
import org.dailycode.pawon.core.dao.RoomDAO;
import org.dailycode.pawon.core.entity.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kris
 * @created Mar 19, 2013
 */
@Repository
public class RoomDAOImpl implements RoomDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Room save(Room entity) {
        String sql = "INSERT INTO room("
                + "name,"
                + "description) "
                + "VALUES(?,?)";
        try {
            jdbcTemplate.update(sql, new Object[]{
                entity.getName(),
                entity.getDescription()
            });
            return entity;
        } catch (Exception exception) {
            AppCore.getLogger(this).error(exception);
        }

        return null;
    }

    @Override
    public Room update(Room entity) {
        String sql = "UPDATE room SET "
                + "name = ?,"
                + "description = ? "
                + "WHERE id = ?";
        try {
            jdbcTemplate.update(sql, new Object[]{
                entity.getName(),
                entity.getDescription(),
                entity.getId()
            });
            return entity;
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }

        return null;
    }

    @Override
    public List<Room> getAll() {
        String sql = "SELECT *FROM room "
                + "WHERE status_delete = false "
                + "ORDER BY id DESC ";
        return jdbcTemplate.query(sql, new RoomRowMapper());
    }

    @Override
    public Room getById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int count() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Room getByName(String name) {
        String sql = "SELECT *FROM room WHERE name = ? AND status_delete = false ";
        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{name}, new RoomRowMapper());
        } catch (EmptyResultDataAccessException exception) {
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;
    }

    @Override
    public boolean delete(Long id) {
        String sql = "UPDATE room SET "
                + "status_delete = true "
                + "WHERE id = ?";
        try {
            jdbcTemplate.update(sql, new Object[]{id});
            return true;
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }

        return false;
    }

    class RoomRowMapper implements RowMapper<Room> {

        @Override
        public Room mapRow(ResultSet rs, int rowNum) throws SQLException {
            Room table = new Room();
            table.setId(rs.getLong("id"));
            table.setName(rs.getString("name"));
            table.setDescription(rs.getString("description"));
            return table;
        }
    }
}
