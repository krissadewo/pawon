/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.service;

import org.dailycode.pawon.core.dao.LanguageDAO;
import org.dailycode.pawon.core.entity.Language;
import java.util.List;
import org.dailycode.pawon.core.dao.SystemParameterDAO;
import org.dailycode.pawon.core.helper.ImageHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author kris
 * @created Feb 12, 2013
 */
@Service
public class LanguageService {

    @Autowired
    private LanguageDAO languageDAO;
    @Autowired
    private SystemParameterDAO systemParameterDAO;

    public Language saveOrUpdate(Language language) {
        if (language.getImageData() != null) {
            ImageHandler.saveImageOnServer(language.getImageData(), language.getImage(), systemParameterDAO.getImageSource(), true);
        }

        if (language.isDefaultLanguage()) {
            languageDAO.updateDefaultLanguage(getAll());
        }

        if (language.getId() == null) {
            return languageDAO.save(language);
        }
        return languageDAO.update(language);
    }

    public List<Language> getAll() {
        return languageDAO.getAll();
    }

    public List<Language> getActiveLanguage() {
        return languageDAO.getActiveLanguage();
    }

    public boolean delete(Long id) {
        return languageDAO.delete(id);
    }
}
