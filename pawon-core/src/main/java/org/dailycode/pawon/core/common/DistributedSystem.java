package org.dailycode.pawon.core.common;

/**
 * ENUM representation supported DistributionSystem
 * @author Gusti Andika
 *
 */
public enum DistributedSystem {
	PMS,
	KEDISE;
	
	public static DistributedSystem getDistributedSystem(String pName){
		if (pName == null) {
			return null;
		}
		for (DistributedSystem distSystem : DistributedSystem.values()) {
			if (pName.toLowerCase().contains(distSystem.name().toLowerCase())) {
				return distSystem;
			}
		}
		return null;
	}
}
