package org.dailycode.pawon.core.entity;

import java.io.Serializable;

/**
 * The purchase item represents all things customers can purchase, except for
 * the movie itself
 */
public class Image implements Serializable {

    private Long id;
    private String imageName;
    private String logoName;
    private byte[] binaryData;
    private Long fileSize;
    private String contentType;
    private String format;

    public String getFormat() {
        String[] result = getImageName().split("\\.");
        if (result.length > 0) {
            this.format = result[result.length - 1];
        }
        return format;
    }

    public byte[] getBinaryData() {
        return binaryData;
    }

    public void setBinaryData(byte[] binaryData) {
        this.binaryData = binaryData;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getLogoName() {
        return logoName;
    }

    public void setLogoName(String logoName) {
        this.logoName = logoName;
    }

    @Override
    public String toString() {
        return "Image{" + "id=" + id + ", imageName=" + imageName + ", logoName=" + logoName + ", binaryData=" + binaryData + ", fileSize=" + fileSize + ", contentType=" + contentType + ", format=" + format + '}';
    }
}
