/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao;

import java.util.List;
import org.dailycode.pawon.core.entity.PurchaseItem;

/**
 *
 * @author kris
 * @created Mar 19, 2013
 */
public interface PurchaseItemDAO extends BaseDAO<PurchaseItem> {

    List<PurchaseItem> getByPurchase(Long purchaseId);

    boolean deleteByPurchase(Long purchaseId);
}
