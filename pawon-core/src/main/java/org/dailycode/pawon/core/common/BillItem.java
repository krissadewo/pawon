package org.dailycode.pawon.core.common;

import java.io.Serializable;
import java.util.Date;

public class BillItem implements Comparable<BillItem>, Serializable {

	/**  */
	private static final long serialVersionUID = 1185219723704711730L;
	private Double price;
	private String description;
	private String departmentCode;
	private Date date;
	private int quantity;
	private boolean display = true;
	private String folioNumber;
	
	public Date getDate() {
		return date;
	}

	public String getDepartmentCode() {
		return departmentCode;
	}

	public String getDescription() {
		return description;
	}

	public String getFolioNumber() {
		return folioNumber;
	}
	public Double getPrice() {
		return price;
	}

	public int getQuantity() {
		return quantity;
	}

	public boolean isDisplay() {
		return display;
	}
	public void setDate(Date date) {
		this.date = date;
	}

	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDisplay(boolean display) {
		this.display = display;
	}
	public void setFolioNumber(String folioNumber) {
		this.folioNumber = folioNumber;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int compareTo(BillItem o) {
		if (this.getDate().before(o.getDate())) {
			return -1;
		}
		return 1;
	}

}
