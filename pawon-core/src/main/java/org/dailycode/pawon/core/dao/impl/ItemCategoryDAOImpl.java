/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.impl;

import org.dailycode.pawon.core.common.Section;
import org.dailycode.pawon.core.dao.ItemCategoryDAO;
import org.dailycode.pawon.core.entity.ItemCategory;
import org.dailycode.pawon.core.helper.ImageHandler;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import org.dailycode.pawon.core.AppCore;
import org.dailycode.pawon.core.entity.LocalizedInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Kris Sadewo
 * @date Feb 7, 2013
 */
@Repository
public class ItemCategoryDAOImpl implements ItemCategoryDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public ItemCategory save(final ItemCategory entity) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        try {
            jdbcTemplate.update(new PreparedStatementCreator() {
                String sql = "INSERT INTO item_category("
                        + "name, "
                        + "active, "
                        + "image,"
                        + "logo,"
                        + "section,"
                        + "source,"
                        + "order_number,"
                        + "parent_id) "
                        + "VALUES(?,?,?,?,?,?,?,?)";

                @Override
                public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                    PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                    ps.setString(1, entity.getName());
                    ps.setBoolean(2, entity.isActive());
                    ps.setString(3, entity.getImage());
                    ps.setString(4, entity.getLogo());
                    ps.setInt(5, entity.getSection().ordinal());
                    ps.setString(6, entity.getSource());
                    ps.setInt(7, entity.getOrderNumber());
                    ps.setLong(8, entity.getParentId());
                    return ps;
                }
            }, keyHolder);

            entity.setId(keyHolder.getKey().longValue());
            return entity;
        } catch (Exception exception) {
            AppCore.getLogger(this).error(exception);
        }
        return null;
    }

    @Override
    public ItemCategory update(ItemCategory entity) {
        String sql = "UPDATE item_category SET "
                + "name = ?, "
                + "active = ?, "
                + "image = ?,"
                + "logo = ?,"
                + "section = ?,"
                + "source = ?,"
                + "order_number = ?,"
                + "parent_id = ? "
                + "WHERE id = ?";
        try {
            jdbcTemplate.update(sql, new Object[]{
                entity.getName(),
                entity.isActive(),
                entity.getImage(),
                entity.getLogo(),
                entity.getSection().ordinal(),
                entity.getSource(),
                entity.getOrderNumber(),
                entity.getParentId(),
                entity.getId()
            });

            return entity;
        } catch (Exception exception) {
            AppCore.getLogger(this).error(exception);
        }
        return null;
    }

    @Override
    public List<ItemCategory> getAll() {
        String sql = "SELECT ic.* "
                + "FROM item_category ic "
                + "WHERE ic.status_delete = false"
                + "ORDER BY ic.order_number,ic.id ASC";
        return jdbcTemplate.query(sql, new ItemCategoryRowMapper());
    }

    @Override
    public List<ItemCategory> getBySection(Section section) {
        String sql = "SELECT ic.* "
                + "FROM item_category ic "
                + "WHERE section = ? AND ic.status_delete = false "
                + "ORDER BY ic.order_number,ic.id ASC ";
        return jdbcTemplate.query(sql, new Object[]{section.ordinal()}, new ItemCategoryRowMapper());
    }

    @Override
    public List<ItemCategory> getBySection(Section section, Long languageId) {
        String sql = "SELECT ic.* ,li.title ,li.description "
                + "FROM item_category ic "
                + "INNER JOIN local_information li ON li.item_category_id = ic.id "
                + "WHERE section = ? AND li.language_id = ? AND ic.status_delete= false "
                + "ORDER BY ic.order_number,ic.id ASC ";
        return jdbcTemplate.query(sql, new Object[]{section.ordinal(), languageId}, new ItemCategoryRowMapper());
    }

    @Override
    public ItemCategory getById(Long id) {
        String sql = "SELECT ic.* "
                + "FROM item_category ic "
                + "WHERE ic.id = ? AND ic.status_delete = false";
        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{id}, new ItemCategoryRowMapper());
        } catch (EmptyResultDataAccessException exception) {
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }

        return null;
    }

    @Override
    public List<ItemCategory> getByParent(Long parentId) {
        String sql = "SELECT ic.* "
                + "FROM item_category ic "
                + "WHERE ic.parent_id = ? AND ic.status_delete = false "
                + "ORDER BY ic.order_number, ic.id ASC ";
        try {
            return jdbcTemplate.query(sql, new Object[]{parentId}, new ItemCategoryRowMapper());
        } catch (EmptyResultDataAccessException exception) {
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;
    }

    @Override
    public List<ItemCategory> getByParent(Long parentId, Long languageId) {
        String sql = "SELECT ic.* ,li.title ,li.description "
                + "FROM item_category ic "
                + "INNER JOIN local_information li ON li.item_category_id = ic.id "
                + "WHERE ic.parent_id = ? AND li.language_id = ? AND ic.status_delete = false "
                + "ORDER BY ic.order_number, ic.id ASC";
        try {
            return jdbcTemplate.query(sql, new Object[]{parentId, languageId}, new ItemCategoryRowMapper());
        } catch (EmptyResultDataAccessException exception) {
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;
    }

    @Override
    public List<ItemCategory> getByParentAndSection(Long parentId, Section purchaseSection) {
        String sql = "SELECT ic.* "
                + "FROM item_category ic "
                + "WHERE ic.parent_id = ? AND section = ? AND ic.status_delete = false "
                + "ORDER BY ic.order_number, ic.id ASC ";
        try {
            return jdbcTemplate.query(sql, new Object[]{parentId, purchaseSection.ordinal()}, new ItemCategoryRowMapper());
        } catch (EmptyResultDataAccessException exception) {
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;
    }

    @Override
    public ItemCategory getByIdAndSection(Long id, Section purchaseSection) {
        String sql = "SELECT ic.* "
                + "FROM item_category ic "
                + "WHERE ic.id = ? AND section = ? AND ic.status_delete = false "
                + "ORDER BY ic.order_number, ic.id ASC ";
        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{id, purchaseSection.ordinal()}, new ItemCategoryRowMapper());
        } catch (EmptyResultDataAccessException exception) {
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;
    }

    @Override
    public Integer count(Long parentId, Section purchaseSection) {
        String sql = "SELECT COUNT(id) FROM item_category "
                + "WHERE parent_id = ? AND section = ? AND ic.status_delete = false";
        return jdbcTemplate.queryForInt(sql, new Object[]{parentId, purchaseSection.ordinal()});
    }

    @Override
    public int count() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean delete(Long id) {
        String sql = "UPDATE item_category set status_delete = true WHERE id = ?";
        try {
            jdbcTemplate.update(sql, new Object[]{id});
            return true;
        } catch (Exception exception) {
            AppCore.getLogger(this).error(exception);
        }
        return false;
    }

    @Override
    public void delete(final List<ItemCategory> itemCategorys) {
        String sql = "UPDATE item_category set status_delete = true WHERE id = ?";
        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setLong(1, itemCategorys.get(i).getId());
            }

            @Override
            public int getBatchSize() {
                return itemCategorys.size();
            }
        });
    }

    class ItemCategoryRowMapper implements RowMapper<ItemCategory> {

        @Override
        public ItemCategory mapRow(ResultSet rs, int rowNum) throws SQLException {
            ResultSetMetaData rsmd = rs.getMetaData();
            ItemCategory itemCategory = new ItemCategory();
            LocalizedInformation localizedInformation = new LocalizedInformation();

            for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                switch (rsmd.getColumnLabel(i)) {
                    case "id":
                        itemCategory.setId(rs.getLong("id"));
                        break;
                    case "active":
                        itemCategory.setActive(rs.getBoolean("active"));
                        break;
                    case "name":
                        itemCategory.setName(rs.getString("name"));
                        break;
                    case "purchase_item":
                        itemCategory.setSection(Section.values()[rs.getInt("section")]);
                        break;
                    case "parent_id":
                        itemCategory.setParentId(rs.getLong("parent_id"));
                        break;
                    case "order_number":
                        itemCategory.setOrderNumber(rs.getInt("order_number"));
                        break;
                    case "image":
                        itemCategory.setImage(rs.getString("image"));
                        break;
                    case "logo":
                        itemCategory.setLogo(rs.getString("logo"));
                        break;
                    case "title":
                        localizedInformation.setTitle(rs.getString("title"));
                        break;
                    case "description":
                        localizedInformation.setDescription(rs.getString("description"));
                        break;
                    default:
                }
            }

            itemCategory.setLocalizedInformation(localizedInformation);

            return itemCategory;
        }
    }
}
