/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao;

import org.dailycode.pawon.core.common.Section;
import org.dailycode.pawon.core.entity.Item;
import org.dailycode.pawon.core.entity.ItemCategory;
import org.dailycode.pawon.core.entity.ItemOption;
import java.util.List;

/**
 *
 * @author kris
 * @created Mar 4, 2013
 */
public interface ItemOptionDAO extends BaseDAO<ItemOption> {

    Integer count(Long parentId, Section purchaseSection);

    /**
     *
     * @param parentId
     * @return
     */
    List<ItemOption> getByParent(Long parentId);

    /**
     *
     * @param parentId
     * @param isActive
     * @return
     */
    List<ItemOption> getByParent(Long parentId, boolean isActive);

    /**
     *
     * @param parentId
     * @param languageId
     * @param isActive
     * @return
     */
    List<ItemOption> getByParent(Long parentId, Long languageId, boolean isActive);

    /**
     *
     * @param parentId
     * @param purchaseSection
     * @return
     */
    List<ItemOption> getByParentAndPurchaseSection(Long parentId, Section purchaseSection);

    /**
     *
     * @param id
     * @param purchaseSection
     * @return
     */
    ItemOption getByIdAndPurchaseSection(Long id, Section purchaseSection);

    /**
     *
     * @param purchaseSection
     * @return
     */
    List<ItemOption> getByPurchaseSection(Section purchaseSection);

    /**
     *
     * @param item
     * @return
     */
    List<ItemOption> getByItem(Item item);

    /**
     *
     * @param item
     * @param languageId
     * @return
     */
    List<ItemOption> getByItem(Long itemId, Long languageId);

    /**
     *
     * @param id
     * @param languageId
     * @return
     */
    ItemOption getById(Long id, Long languageId);

    /**
     *
     * @param itemOptions
     */
    void delete(final List<ItemOption> itemOptions);
}
