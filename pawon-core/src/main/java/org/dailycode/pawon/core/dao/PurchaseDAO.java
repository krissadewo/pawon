/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao;

import java.util.Date;
import java.util.List;
import org.dailycode.pawon.core.common.PurchaseStatus;
import org.dailycode.pawon.core.entity.Purchase;

/**
 *
 * @author kris
 * @created Mar 19, 2013
 */
public interface PurchaseDAO extends BaseDAO<Purchase> {

    /**
     *
     * @param roomId
     * @param purchaseStatus
     * @return
     */
    Purchase getByRoom(Long roomId, PurchaseStatus purchaseStatus);

    /**
     *
     * @param roomName
     * @param purchaseStatus
     * @return
     */
    Purchase getByRoom(String roomName, PurchaseStatus purchaseStatus);

    /**
     *
     * @param purchaseStatus
     * @return
     */
    List<Purchase> getActivePurchases(PurchaseStatus purchaseStatus);

    /**
     *
     * @param start
     * @param end
     * @param startDate
     * @param endDate
     * @return
     */
    List<Purchase> search(Integer start, Integer end, Date startDate, Date endDate);

    /**
     *
     * @param start
     * @param end
     * @param startDate
     * @param endDate
     * @param roomName
     * @param orderNumber
     * @return
     */
    List<Purchase> search(Integer start, Integer end, Date startDate, Date endDate, String roomName, String orderNumber);

    /**
     *
     * @param startDate
     * @param endDate
     * @return
     */
    int count(Date startDate, Date endDate);

    /**
     *
     * @param start
     * @param end
     * @param sortField
     * @param orderBy
     * @return
     */
    List<Purchase> getAll(Integer start, Integer end, String sortField, String orderBy);

    /**
     *
     * @param startDate
     * @param endDate
     * @param roomName
     * @param orderNumber
     * @return
     */
    int count(Date startDate, Date endDate, String roomName, String orderNumber);
}
