/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import org.dailycode.pawon.core.AppCore;
import org.dailycode.pawon.core.dao.PurchaseItemDAO;
import org.dailycode.pawon.core.entity.Item;
import org.dailycode.pawon.core.entity.Purchase;
import org.dailycode.pawon.core.entity.PurchaseItem;
import org.dailycode.pawon.core.entity.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kris
 * @created Mar 19, 2013
 */
@Repository
public class PurchaseItemDAOImpl implements PurchaseItemDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public PurchaseItem save(final PurchaseItem entity) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        try {
            jdbcTemplate.update(new PreparedStatementCreator() {
                String sql = "INSERT INTO purchase_item("
                        + "name,"
                        + "price, "
                        + "discount,"
                        + "item_id,"
                        + "purchase_id) "
                        + "VALUES(?,?,?,?,?)";

                @Override
                public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                    PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                    ps.setString(1, entity.getName());
                    ps.setDouble(2, entity.getPrice());
                    ps.setDouble(3, entity.getDiscount());
                    ps.setLong(4, entity.getItem().getId());
                    ps.setLong(5, entity.getPurchase().getId());
                    return ps;
                }
            }, keyHolder);
            entity.setId(keyHolder.getKey().longValue());
            return entity;
        } catch (Exception exception) {
            AppCore.getLogger(this).error(exception);
        }
        return null;
    }

    @Override
    public PurchaseItem update(PurchaseItem entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(Long id) {
        String sql = "DELETE FROM purchase_item where id = ?";
        try {
            jdbcTemplate.update(sql, new Object[]{id});
            return true;
        } catch (Exception exception) {
            AppCore.getLogger(this).error(exception);
        }
        return false;
    }

    @Override
    public boolean deleteByPurchase(Long purchaseId) {
        String sql = "DELETE FROM purchase_item WHERE purchase_id = ?";
        try {
            jdbcTemplate.update(sql, new Object[]{purchaseId});
            return true;
        } catch (Exception exception) {
            AppCore.getLogger(this).error(exception);
        }
        return false;
    }

    @Override
    public List<PurchaseItem> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PurchaseItem getById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int count() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<PurchaseItem> getByPurchase(Long purchaseId) {
        String sql = "SELECT "
                + "r.id as room_id,"
                + "r.name as room_name,"
                + "i.name as item_name,"
                + "pi.* "
                + "FROM purchase_item pi "
                + "INNER JOIN purchase p ON p.id = pi.purchase_id "
                + "INNER JOIN item i ON i.id = pi.item_id "
                + "INNER JOIN room r ON r.id = p.room_id "
                + "WHERE p.id = ? ";
        return jdbcTemplate.query(sql, new Object[]{purchaseId}, new PurchaseItemRowMapper());
    }

    class PurchaseItemRowMapper implements RowMapper<PurchaseItem> {

        @Override
        public PurchaseItem mapRow(ResultSet rs, int rowNum) throws SQLException {
            Room room = new Room();
            room.setId(rs.getLong("room_id"));
            room.setName(rs.getString("room_name"));

            Purchase purchase = new Purchase();
            purchase.setId(rs.getLong("purchase_id"));

            Item item = new Item();
            item.setId(rs.getLong("item_id"));
            item.setName(rs.getString("item_name"));
            item.setDiscount(rs.getDouble("discount"));

            PurchaseItem purchaseItem = new PurchaseItem();
            purchaseItem.setId(rs.getLong("id"));
            purchaseItem.setName(rs.getString("name"));
            purchaseItem.setPrice(rs.getDouble("price"));
            purchaseItem.setDiscount(rs.getDouble("discount"));
            purchaseItem.setItem(item);
            purchaseItem.setPurchase(purchase);

            return purchaseItem;
        }
    }
}
