/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.service;

import java.util.List;
import org.dailycode.pawon.core.dao.PurchaseDAO;
import org.dailycode.pawon.core.dao.PurchaseItemDAO;
import org.dailycode.pawon.core.dao.PurchaseOptionDAO;
import org.dailycode.pawon.core.entity.PurchaseItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kris
 * @created Mar 20, 2013
 */
@Service
public class PurchaseItemService {

    @Autowired
    private PurchaseItemDAO purchaseItemDAO;
    @Autowired
    private PurchaseDAO purchaseDAO;
    @Autowired
    private PurchaseOptionDAO purchaseOptionDAO;

    public List<PurchaseItem> getByPurchase(Long id) {
        return purchaseItemDAO.getByPurchase(id);
    }

    @Transactional
    public boolean delete(Long purchaseItemId, Long purchaseId) {
        purchaseOptionDAO.deleteByPurchaseItem(purchaseItemId);
        purchaseItemDAO.delete(purchaseItemId);
        //If purchase item on purchase has been empty we should delete the purchase from db
        if (purchaseItemDAO.getByPurchase(purchaseId).isEmpty()) {
            purchaseDAO.delete(purchaseId);
        }
        return true;
    }
}
