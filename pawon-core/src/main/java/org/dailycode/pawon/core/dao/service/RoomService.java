/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.service;

import java.util.List;
import org.dailycode.pawon.core.dao.RoomDAO;
import org.dailycode.pawon.core.entity.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author kris
 * @created Mar 19, 2013
 */
@Service
public class RoomService {

    @Autowired
    private RoomDAO roomDAO;

    public Room saveOrUpdate(Room table) {
        if (table.getId() == null) {
            return roomDAO.save(table);
        }
        return roomDAO.update(table);
    }

    public boolean delete(Long id) {
        return roomDAO.delete(id);
    }

    public Room getByName(String name) {
        return roomDAO.getByName(name);
    }

    public List<Room> getAll() {
        return roomDAO.getAll();
    }
}
