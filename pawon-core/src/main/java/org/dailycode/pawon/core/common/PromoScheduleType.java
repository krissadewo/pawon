package org.dailycode.pawon.core.common;

public enum PromoScheduleType {
	ONCE, ONCE_PER_DAY, ALWAYS
}
