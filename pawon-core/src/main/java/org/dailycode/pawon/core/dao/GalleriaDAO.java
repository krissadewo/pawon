/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao;

import java.util.List;
import org.dailycode.pawon.core.entity.Galleria;

/**
 *
 * @author kris
 */
public interface GalleriaDAO extends BaseDAO<Galleria> {

    public List<Galleria> getAllActive();
}
