/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;
import org.dailycode.pawon.core.AppCore;
import org.dailycode.pawon.core.common.PurchaseStatus;
import org.dailycode.pawon.core.dao.PurchaseDAO;
import org.dailycode.pawon.core.entity.Purchase;
import org.dailycode.pawon.core.entity.Room;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kris
 * @created Mar 19, 2013
 */
@Repository
public class PurchaseDAOImpl implements PurchaseDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Purchase save(final Purchase entity) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        try {
            jdbcTemplate.update(new PreparedStatementCreator() {
                String sql = "INSERT INTO purchase("
                        + "date_purchase,"
                        + "order_number, "
                        + "room_id) "
                        + "VALUES(?,?,?)";

                @Override
                public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                    PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                    ps.setDate(1, new java.sql.Date(entity.getDatePurchase().getTime()));
                    ps.setLong(2, new DateTime().getMillisOfDay());
                    ps.setLong(3, entity.getRoom().getId());
                    return ps;
                }
            }, keyHolder);
            entity.setId(keyHolder.getKey().longValue());
            return entity;
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
            return null;
        }
    }

    @Override
    public Purchase update(Purchase entity) {
        String sql = "UPDATE purchase SET status = ? WHERE id = ?";
        try {
            jdbcTemplate.update(sql, new Object[]{entity.getPurchaseStatus().ordinal(), entity.getId()});
            return entity;
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;
    }

    @Override
    public boolean delete(Long purchaseItemId) {
        String sql = "DELETE FROM purchase where id = ?";
        try {
            jdbcTemplate.update(sql, new Object[]{purchaseItemId});
            return true;
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return false;
    }

    @Override
    public List<Purchase> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Purchase getById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int count() {
        String sql = "SELECT COUNT(id) FROM purchase "
                + "WHERE status = " + PurchaseStatus.COMPLETE.ordinal() + " ";
        return jdbcTemplate.queryForInt(sql);
    }

    @Override
    public Purchase getByRoom(Long roomId, PurchaseStatus purchaseStatus) {
        String sql = "SELECT p.*,r.id AS room_id, r.name, r.description "
                + "FROM purchase p "
                + "INNER JOIN room r ON r.id = p.room_id "
                + "WHERE room_id = ? AND status = ?";
        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{roomId, purchaseStatus.ordinal()}, new PurchaseRowMapper());
        } catch (EmptyResultDataAccessException exception) {
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;
    }

    @Override
    public Purchase getByRoom(String roomName, PurchaseStatus purchaseStatus) {
        String sql = "SELECT p.*,r.id AS room_id, r.name, r.description "
                + "FROM purchase p "
                + "INNER JOIN room r ON r.id = p.room_id "
                + "WHERE r.name = ? AND status = ?";
        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{roomName, purchaseStatus.ordinal()}, new PurchaseRowMapper());
        } catch (EmptyResultDataAccessException exception) {
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;
    }

    @Override
    public List<Purchase> getActivePurchases(PurchaseStatus purchaseStatus) {
        String sql = "SELECT p.*,r.id AS room_id, r.name, r.description "
                + "FROM purchase p "
                + "INNER JOIN room r ON r.id = p.room_id "
                + "WHERE status = ?";
        return jdbcTemplate.query(sql, new Object[]{purchaseStatus.ordinal()}, new PurchaseRowMapper());
    }

    @Override
    public int count(Date startDate, Date endDate) {
        String sql = "SELECT COUNT(id) FROM purchase "
                + "WHERE date_purchase BETWEEN ? AND  ? "
                + "AND status = " + PurchaseStatus.COMPLETE.ordinal();
        return jdbcTemplate.queryForInt(sql, new Object[]{startDate, endDate});
    }

    @Override
    public List<Purchase> search(Integer start, Integer end, Date startDate, Date endDate) {
        String sql = "SELECT p.*,r.id AS room_id, r.name, r.description "
                + "FROM purchase p "
                + "INNER JOIN room r ON r.id = p.room_id "
                + "WHERE status = " + PurchaseStatus.COMPLETE.ordinal() + " "
                + "AND date_purchase BETWEEN ? AND ? "
                + "LIMIT ?,? ";
        return jdbcTemplate.query(sql, new Object[]{startDate, endDate, start, end}, new PurchaseRowMapper());
    }

    @Override
    public int count(Date startDate, Date endDate, String roomName, String orderNumber) {
        String sql = "SELECT COUNT(p.id) FROM purchase p "
                + "INNER JOIN room r ON r.id=p.room_id "
                + "WHERE p.date_purchase BETWEEN ? AND  ? "
                + "AND r.name LIKE ? "
                + "AND p.order_number LIKE ? "
                + "AND p.status = " + PurchaseStatus.COMPLETE.ordinal();
        return jdbcTemplate.queryForInt(sql, new Object[]{startDate, endDate, "%" + roomName + "%", "%" + orderNumber + "%"});
    }

    @Override
    public List<Purchase> search(Integer start, Integer end, Date startDate, Date endDate, String roomName, String orderNumber) {
        String sql = "SELECT p.*,r.id AS room_id, r.name, r.description "
                + "FROM purchase p "
                + "INNER JOIN room r ON r.id = p.room_id "
                + "WHERE status = " + PurchaseStatus.COMPLETE.ordinal() + " "
                + "AND r.name LIKE ? "
                + "AND p.order_number LIKE ? "
                + "AND date_purchase BETWEEN ? AND ? "
                + "LIMIT ?,? ";
        return jdbcTemplate.query(sql, new Object[]{"%" + roomName + "%", "%" + orderNumber + "%", startDate, endDate, start, end}, new PurchaseRowMapper());
    }

    @Override
    public List<Purchase> getAll(Integer start, Integer end, String sortField, String orderBy) {
        String sql = "SELECT p.*,room.id AS room_id, room.name, room.description "
                + "FROM purchase p "
                + "INNER JOIN room ON room.id = p.room_id "
                + "WHERE status =  " + PurchaseStatus.COMPLETE.ordinal() + " "
                + "ORDER BY " + sortField + " " + orderBy + " "
                + "LIMIT ?,? ";

        return jdbcTemplate.query(sql, new Object[]{start, end}, new PurchaseRowMapper());
    }

    class PurchaseRowMapper implements RowMapper<Purchase> {

        @Override
        public Purchase mapRow(ResultSet rs, int rowNum) throws SQLException {
            Room room = new Room();
            room.setId(rs.getLong("room_id"));
            room.setName(rs.getString("name"));
            room.setDescription(rs.getString("description"));

            Purchase purchase = new Purchase();
            purchase.setId(rs.getLong("id"));
            purchase.setDatePurchase(rs.getDate("date_purchase"));
            purchase.setPurchaseStatus(PurchaseStatus.values()[rs.getInt("status")]);
            purchase.setOrderNumber(rs.getLong("order_number"));
            purchase.setRoom(room);
            return purchase;
        }
    }
}
