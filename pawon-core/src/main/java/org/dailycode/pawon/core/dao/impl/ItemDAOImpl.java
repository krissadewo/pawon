/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.impl;

import org.dailycode.pawon.core.common.Section;
import org.dailycode.pawon.core.dao.ItemDAO;
import org.dailycode.pawon.core.entity.Item;
import org.dailycode.pawon.core.entity.ItemCategory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import org.dailycode.pawon.core.AppCore;
import org.dailycode.pawon.core.entity.LocalizedInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kris
 * @created Feb 24, 2013
 */
@Repository
public class ItemDAOImpl implements ItemDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    //private static final Log LOG = LogFactory.getLog(this);

    @Override
    public Item save(final Item entity) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        try {
            jdbcTemplate.update(new PreparedStatementCreator() {
                String sql = "INSERT INTO item("
                        + "active,"
                        + "code,"
                        + "name,"
                        + "image,"
                        + "price,"
                        + "discount,"
                        + "order_number,"
                        + "item_category_id) "
                        + "VALUES(?,?,?,?,?,?,?,?)";

                @Override
                public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                    PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                    ps.setBoolean(1, entity.isActive());
                    ps.setString(2, entity.getCode());
                    ps.setString(3, entity.getName());
                    ps.setString(4, entity.getImage());
                    ps.setDouble(5, entity.getPrice());
                    ps.setDouble(6, entity.getDiscount());
                    ps.setInt(7, entity.getOrderNumber());
                    ps.setLong(8, entity.getItemCategory().getId());
                    return ps;
                }
            }, keyHolder);

            entity.setId(keyHolder.getKey().longValue());

            //Save item to option
            if (entity.getItemOptions() != null) {
                saveItem2Option(entity);
            }

            return entity;
        } catch (Exception exception) {
            AppCore.getLogger(this).error(exception);
        }

        return null;
    }

    @Override
    public Item update(final Item entity) {
        String sqlUpdateItem = "UPDATE item SET "
                + "active = ?,"
                + "code = ?,"
                + "name = ?,"
                + "image = ?,"
                + "price = ?,"
                + "discount = ?,"
                + "order_number = ?,"
                + "item_category_id = ? "
                + "WHERE id = ?";
        try {
            jdbcTemplate.update(sqlUpdateItem, new Object[]{
                entity.isActive(),
                entity.getCode(),
                entity.getName(),
                entity.getImage(),
                entity.getPrice(),
                entity.getDiscount(),
                entity.getOrderNumber(),
                entity.getItemCategory().getId(),
                entity.getId()});

            //delete before
            deleteItem2Option(entity);

            //Save item to option
            if (entity.getItemOptions() != null) {
                saveItem2Option(entity);
            }
            return entity;
        } catch (Exception exception) {
            AppCore.getLogger(this).error(exception);
        }

        return null;
    }

    @Override
    public void saveItem2Option(final Item item) {
        String sqlSaveItem2Option = "INSERT INTO item_2_option(item_id, item_option_id) VALUES(?,?)";
        jdbcTemplate.batchUpdate(sqlSaveItem2Option, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setLong(1, item.getId());
                ps.setLong(2, item.getItemOptions().get(i).getId());
            }

            @Override
            public int getBatchSize() {
                return item.getItemOptions().size();
            }
        });
    }

    @Override
    public void deleteItem2Option(final Item item) {
        String sqlSaveItem2Option = "DELETE FROM item_2_option WHERE item_id = ?";
        jdbcTemplate.batchUpdate(sqlSaveItem2Option, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setLong(1, item.getId());
            }

            @Override
            public int getBatchSize() {
                return item.getItemOptions().size();
            }
        });
    }

    @Override
    public List<Item> getAll() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Item> getAll(Integer start, Integer end, String sortField, String orderBy) {
        String sql = "SELECT i.*, ic.name AS category_name "
                + "FROM item i "
                + "INNER JOIN item_category ic ON ic.id = i.item_category_id "
                + "WHERE i.status_delete = false "
                + "LIMIT ?, ?";
        return jdbcTemplate.query(sql, new Object[]{start, end}, new ItemRowMapper());
    }

    @Override
    public List<Item> search(Section purchaseSection, Integer start, Integer pageSize, Item entity) {
        String sql = "SELECT i.*, ic.name AS category_name "
                + "FROM item i "
                + "INNER JOIN item_category ic ON ic.id = i.item_category_id "
                + "WHERE i.code LIKE ?  "
                + "AND i.name LIKE ? "
                + "AND i.status_delete = false "
                + "LIMIT ?,?";

        return jdbcTemplate.query(sql, new Object[]{
            "%" + entity.getCode() + "%",
            "%" + entity.getName() + "%",
            start,
            pageSize}, new ItemRowMapper());
    }

    @Override
    public Item getById(Long id) {
        String sql = "SELECT i.*, ic.name AS category_name "
                + "FROM item i "
                + "INNER JOIN item_category ic ON ic.id = i.item_category_id "
                + "AND i.status_delete = false "
                + "WHERE i.id = ? ";
        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{id}, new ItemRowMapper());
        } catch (EmptyResultDataAccessException exception) {
        } catch (DataAccessException exception) {
            AppCore.getLogger(this).error(exception);
        }
        return null;
    }

    @Override
    public Item getById(Long id, Long languageId) {
        String sql = "SELECT i.*, ic.name AS category_name ,li.title ,li.description "
                + "FROM item i "
                + "INNER JOIN item_category ic ON ic.id = i.item_category_id "
                + "INNER JOIN local_information li ON li.item_id = i.id "
                + "WHERE i.id = ?  AND li.language_id = ? AND i.status_delete = false";
        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{id, languageId}, new ItemRowMapper());
        } catch (EmptyResultDataAccessException exception) {
        } catch (DataAccessException exception) {
            AppCore.getLogger(this).error(exception);
        }
        return null;
    }

    @Override
    public List<Item> getBySection(Section section, Integer start, Integer pageSize, String sortField, String orderBy) {
        //Order by can't use prepared statement param, my be this is bug
        String sql = "SELECT i.*, ic.name AS category_name "
                + "FROM item i "
                + "INNER JOIN item_category ic ON ic.id = i.item_category_id "
                + "WHERE ic.section = ? AND i.status_delete = false "
                + "ORDER BY " + sortField + " " + orderBy + " "
                + "LIMIT ?,? ";

        return jdbcTemplate.query(sql, new Object[]{section.ordinal(), start, pageSize}, new ItemRowMapper());
    }

    @Override
    public int count(Section purchaseSection, Item entity) {
        String sql = "SELECT COUNT(i.id) FROM item i "
                + "INNER JOIN item_category ic ON ic.id = i.item_category_id "
                + "WHERE ic.section = ? "
                + "AND i.code LIKE ? "
                + "AND i.name LIKE ? "
                + "AND i.status_delete = false ";

        return jdbcTemplate.queryForInt(sql, new Object[]{
            purchaseSection.ordinal(),
            "%" + entity.getCode() + "%",
            "%" + entity.getName() + "%"});
    }

    @Override
    public int count(Section purchaseSection) {
        String sql = "SELECT COUNT(i.id) FROM item i "
                + "INNER JOIN item_category ic ON ic.id = i.item_category_id "
                + "WHERE ic.section = ? AND i.status_delete = false";

        return jdbcTemplate.queryForInt(sql, new Object[]{purchaseSection.ordinal()});
    }

    @Override
    public int count() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Item> getByCategory(Long categoryId) {
        String sql = "SELECT i.*, ic.name AS category_name "
                + "FROM item i "
                + "INNER JOIN item_category ic ON ic.id = i.item_category_id "
                + "WHERE ic.id = ? "
                + "AND i.status_delete = false "
                + "ORDER BY i.name ASC ";

        return jdbcTemplate.query(sql, new Object[]{categoryId}, new ItemRowMapper());
    }

    @Override
    public List<Item> getByCategory(Long categoryId, Long languageId) {
        String sql = "SELECT i.*, ic.name AS category_name ,li.title ,li.description "
                + "FROM item i "
                + "INNER JOIN item_category ic ON ic.id = i.item_category_id "
                + "INNER JOIN local_information li ON li.item_id = i.id "
                + "WHERE ic.id = ?  AND li.language_id = ? "
                + "AND i.status_delete = false "              
                + "ORDER BY i.name ASC ";

        return jdbcTemplate.query(sql, new Object[]{categoryId, languageId}, new ItemRowMapper());
    }

    @Override
    public boolean delete(Long id) {
        String sql = "UPDATE item set status_delete = true, code = x WHERE id = ?";
        try {
            jdbcTemplate.update(sql, new Object[]{id});
            return true;
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return false;
    }

    @Override
    public void delete(final List<Item> items) {
        String sql = "UPDATE item SET status_delete = true, code = x WHERE id = ?";
        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setLong(1, items.get(i).getId());
            }

            @Override
            public int getBatchSize() {
                return items.size();
            }
        });
    }

    @Override
    public Item getByCode(String code) {
        String sql = "SELECT i.*, ic.name AS category_name "
                + "FROM item i "
                + "INNER JOIN item_category ic ON ic.id = i.item_category_id "
                + "WHERE i.code = ? ";
        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{code}, new ItemRowMapper());
        } catch (EmptyResultDataAccessException e) {
        } catch (DataAccessException e) {
            AppCore.getLogger(this).error(e);
        }
        return null;
    }

    class ItemRowMapper implements RowMapper<Item> {

        @Override
        public Item mapRow(ResultSet rs, int rowNum) throws SQLException {
            ResultSetMetaData rsmd = rs.getMetaData();
            Item item = new Item();
            ItemCategory itemCategory = new ItemCategory();
            LocalizedInformation localizedInformation = new LocalizedInformation();

            for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                switch (rsmd.getColumnLabel(i)) {
                    case "id":
                        item.setId(rs.getLong("id"));
                        break;
                    case "code":
                        item.setCode(rs.getString("code"));
                        break;
                    case "name":
                        item.setName(rs.getString("name"));
                        break;
                    case "image":
                        item.setImage(rs.getString("image"));
                        break;
                    case "active":
                        item.setActive(rs.getBoolean("active"));
                        break;
                    case "price":
                        item.setPrice(rs.getDouble("price"));
                        break;
                    case "discount":
                        item.setDiscount(rs.getDouble("discount"));
                        break;
                    case "order_number":
                        item.setOrderNumber(rs.getInt("order_number"));
                        break;
                    case "item_category_id":
                        itemCategory.setId(rs.getLong("item_category_id"));
                        break;
                    case "category_name":
                        itemCategory.setName(rs.getString("category_name"));
                        break;
                    case "title":
                        localizedInformation.setTitle(rs.getString("title"));
                        break;
                    case "description":
                        localizedInformation.setDescription(rs.getString("description"));
                        break;
                }
            }

            item.setItemCategory(itemCategory);
            item.setLocalizedInformation(localizedInformation);
            return item;
        }
    }
}
