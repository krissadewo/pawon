/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.service;

import java.util.Date;
import java.util.List;
import org.dailycode.pawon.core.dao.UsesrDAO;
import org.dailycode.pawon.core.entity.Users;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 *
 * @author kris
 */
@Service
public class UserService {

    @Autowired
    private UsesrDAO userDAO;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public Users saveOrUpdate(Users user) {
        String salt = String.valueOf(Math.random() * new Date().getTime());
        user.setSalt(salt);

        StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();
        if (user.getId() == null) {
            user.setPassword(encryptor.encryptPassword(user.getPassword()));
            return userDAO.save(user);
        } else {
            //Jika password nya tidak dirubah maka simpan tanpa perlu di encrypt
            if (!user.getPassword().equals(userDAO.getById(user.getId()).getPassword())) {
                user.setPassword(passwordEncoder.encodePassword(user.getPassword(), salt));
            }
            return userDAO.update(user);
        }
    }

    public boolean delete(Long id) {
        return userDAO.delete(id);
    }

    public Users login(Users user) {
        Users u = userDAO.getUserByUsernameAndPassword(user);
        if (u != null) {
            StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();
            if (encryptor.checkPassword(user.getPassword(), u.getPassword())) {
                return u;
            }
        }
        return null;
    }

    public List<Users> getAll() {
        return userDAO.getAll();
    }

    public Users findByUsername(String username) {
        return userDAO.findByUsername(username);
    }
}
