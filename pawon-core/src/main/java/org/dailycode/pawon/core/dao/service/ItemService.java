/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.service;

import java.util.List;
import org.dailycode.pawon.core.common.Section;
import org.dailycode.pawon.core.dao.ItemDAO;
import org.dailycode.pawon.core.dao.LocalizedInformationDAO;
import org.dailycode.pawon.core.dao.SystemParameterDAO;
import org.dailycode.pawon.core.entity.Item;
import org.dailycode.pawon.core.entity.LocalizedInformation;
import org.dailycode.pawon.core.helper.ImageHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kris
 * @created Feb 26, 2013
 */
@Service
public class ItemService {

    @Autowired
    private ItemDAO itemDAO;
    @Autowired
    private LocalizedInformationDAO localizedInformationDAO;
    @Autowired
    private SystemParameterDAO systemParameterDAO;

    @Transactional
    public Item saveOrUpdate(Item item) {
        if (item.getId() == null) {
            item = itemDAO.save(item);
            if (item != null) {
                for (LocalizedInformation localizedInformation : item.getLocalizedInformations()) {
                    localizedInformation.setItem(item);
                    localizedInformationDAO.save(localizedInformation);
                }

                if (item.getImageData() != null) {
                    ImageHandler.saveImageOnServer(item.getImageData(), item.getImage(), systemParameterDAO.getImageSource(), true);
                }
            }
            return item;
        } else {
            item = itemDAO.update(item);
            if (item != null) {
                for (LocalizedInformation localizedInformation : item.getLocalizedInformations()) {
                    localizedInformation.setItem(item);
                    if (localizedInformationDAO.getById(localizedInformation.getId()) == null) {
                        localizedInformationDAO.save(localizedInformation);
                    } else {
                        localizedInformationDAO.update(localizedInformation);
                    }
                }

                if (item.getImageData() != null) {
                    ImageHandler.saveImageOnServer(item.getImageData(), item.getImage(), systemParameterDAO.getImageSource(), true);
                }
            }
            return item;
        }
    }

    public List<Item> getAll(Integer start, Integer end, String sortField, String orderBy) {
        return itemDAO.getAll(start, end, sortField, orderBy);
    }

    public void delete(List<Item> items) {
        itemDAO.delete(items);
    }

    public List<Item> getBySection(Section section, Integer start, Integer end, String sortField, String orderBy) {
        return itemDAO.getBySection(section, start, end, sortField, orderBy);
    }

    public List<Item> getByCategory(Long categoryId) {
        return itemDAO.getByCategory(categoryId);
    }

    public List<Item> getByCategory(Long categoryId, Long languageId) {
        return itemDAO.getByCategory(categoryId, languageId);
    }

    public Item getById(Long id, Long languageId) {
        return itemDAO.getById(id, languageId);
    }

    public Item getById(Long id) {
        return itemDAO.getById(id);
    }

    public List<Item> search(Section section, int first, int pageSize, Item item) {
        return itemDAO.search(section, first, pageSize, item);
    }

    public int count(Section section, Item item) {
        return itemDAO.count(section, item);
    }

    public int count(Section section) {
        return itemDAO.count(section);
    }

    public Item getByCode(String code) {
        return itemDAO.getByCode(code);
    }
}
