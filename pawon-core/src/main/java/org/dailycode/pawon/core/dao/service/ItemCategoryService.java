/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.service;

import java.util.ArrayList;
import java.util.List;
import org.dailycode.pawon.core.common.Section;
import org.dailycode.pawon.core.dao.ItemCategoryDAO;
import org.dailycode.pawon.core.dao.ItemDAO;
import org.dailycode.pawon.core.dao.LocalizedInformationDAO;
import org.dailycode.pawon.core.dao.SystemParameterDAO;
import org.dailycode.pawon.core.entity.ItemCategory;
import org.dailycode.pawon.core.entity.LocalizedInformation;
import org.dailycode.pawon.core.helper.ImageHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kris
 * @created Feb 12, 2013
 */
@Service
public class ItemCategoryService {

    @Autowired
    private ItemCategoryDAO itemCategoryDAO;
    @Autowired
    private LocalizedInformationDAO localizedInformationDAO;
    @Autowired
    private ItemDAO itemDAO;
    @Autowired
    private SystemParameterDAO systemParameterDAO;

    @Transactional
    public ItemCategory saveOrUpdate(ItemCategory itemCategory) {
        if (itemCategory.getId() == null) {
            itemCategory = itemCategoryDAO.save(itemCategory);
            if (itemCategory != null) {
                for (LocalizedInformation localizedInformation : itemCategory.getLocalizedInformations()) {
                    localizedInformation.setItemCategory(itemCategory);
                    localizedInformationDAO.save(localizedInformation);
                }

                if (itemCategory.getImageData() != null) {
                    ImageHandler.saveImageOnServer(itemCategory.getImageData(), itemCategory.getImage(), systemParameterDAO.getImageSource(), true);
                }
                if (itemCategory.getLogoData() != null) {
                    ImageHandler.saveImageOnServer(itemCategory.getLogoData(), itemCategory.getLogo(), systemParameterDAO.getImageSource(), true);
                }
            }
            return itemCategory;
        } else {
            itemCategory = itemCategoryDAO.update(itemCategory);
            for (LocalizedInformation localizedInformation : itemCategory.getLocalizedInformations()) {
                localizedInformation.setItemCategory(itemCategory);
                if (localizedInformationDAO.getById(localizedInformation.getId()) == null) {
                    localizedInformationDAO.save(localizedInformation);
                } else {
                    localizedInformationDAO.update(localizedInformation);
                }
            }

            if (itemCategory.getImageData() != null) {
                ImageHandler.saveImageOnServer(itemCategory.getImageData(), itemCategory.getImage(), systemParameterDAO.getImageSource(), true);
            }
            if (itemCategory.getLogoData() != null) {
                ImageHandler.saveImageOnServer(itemCategory.getLogoData(), itemCategory.getLogo(), systemParameterDAO.getImageSource(), true);
            }
            return itemCategory;
        }
    }

    public void delete(List<ItemCategory> itemCategorys) {
        itemCategoryDAO.delete(itemCategorys);
    }

    @Transactional
    public void delete(Long id) {
        deleteChildCategory(id);
        itemCategoryDAO.delete(id);
        itemDAO.delete(itemDAO.getByCategory(id));
    }

    @Transactional
    private void deleteChildCategory(Long id) {
        itemDAO.delete(itemDAO.getByCategory(id));
        itemCategoryDAO.delete(id);
        for (ItemCategory ic : itemCategoryDAO.getByParent(id)) {
            deleteChildCategory(ic.getId());
        }
    }

    public Integer countByParentAndPurchaseSection(Long parentId, Section section) {
        return itemCategoryDAO.count(parentId, section);
    }

    public List<ItemCategory> getAll() {
        return itemCategoryDAO.getAll();
    }

    public List<ItemCategory> getBySection(Section section) {
        return itemCategoryDAO.getBySection(section);
    }

    public List<ItemCategory> getBySection(Section section, Long languageId) {
        return itemCategoryDAO.getBySection(section, languageId);
    }

    public List<ItemCategory> getByParent(Long parentId) {
        return itemCategoryDAO.getByParent(parentId);
    }

    public List<ItemCategory> getByParent(Long parentId, Long languageId) {
        return itemCategoryDAO.getByParent(parentId, languageId);
    }

    public List<ItemCategory> getByParentAndSection(Long parentId, Section section) {
        return itemCategoryDAO.getByParentAndSection(parentId, section);
    }

    public ItemCategory getByIdAndSection(Long id, Section section) {
        return itemCategoryDAO.getByIdAndSection(id, section);
    }

    public ItemCategory getById(Long id) {
        return itemCategoryDAO.getById(id);
    }

    public boolean hasChild(Long id) {
        if (itemCategoryDAO.getByParent(id).size() > 0) {
            return true;
        }
        return false;
    }

    public List<ItemCategory> getOrganizedParent(Section section) {
        List<ItemCategory> results = new ArrayList<>();
        for (ItemCategory itemCategory : getBySection(section)) {
            StringBuilder sb = new StringBuilder();
            String organizedParent = getParent(itemCategory.getId(), sb);
            itemCategory.setName(organizedParent.substring(0, organizedParent.length() - 2));
            results.add(itemCategory);
        }
        return results;
    }

    /**
     *
     * @param section
     * @param pItemCategory
     * @param topLevel Level is a length of categories<br> Top level mean : If
     * selected category C, show Only the top level of C (A > B) else if top
     * level is false then show the leven like (A > B > C)
     * @return parent of all categories with specific format (A > B > C)
     */
    public ItemCategory getOrganizedParent(Section section, ItemCategory pItemCategory, boolean topLevel) {
        ItemCategory itemCategory = getByIdAndSection(pItemCategory.getId(), section);

        if (itemCategory != null) {
            StringBuilder sb = new StringBuilder();
            String organizedParent = getParent(itemCategory.getId(), sb);

            String[] results = organizedParent.split(">");
            StringBuilder itemCategoryName = new StringBuilder();


            int level = results.length;

            if (topLevel) {
                level -= 1;
            }

            for (int i = 0; i < level; i++) {
                itemCategoryName.append(results[i]);
                itemCategoryName.append(" > ");
            }

            if (itemCategoryName.length() > 0) {
                itemCategory.setName(itemCategoryName.toString().substring(0, itemCategoryName.length() - 2));
            }
        }

        return itemCategory;
    }

    private String getParent(Long parentId, StringBuilder sb) {
        ItemCategory itemCategory = itemCategoryDAO.getById(parentId);
        if (itemCategory != null) {
            getParent(itemCategory.getParentId(), sb);
            sb.append(itemCategory.getName());
            sb.append(">");
            return sb.toString();
        }
        return null;
    }
}
