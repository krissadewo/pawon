/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.impl;

import java.sql.PreparedStatement;
import org.dailycode.pawon.core.common.Section;
import org.dailycode.pawon.core.dao.ItemOptionDAO;
import org.dailycode.pawon.core.entity.Item;
import org.dailycode.pawon.core.entity.ItemOption;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;
import org.dailycode.pawon.core.AppCore;
import org.dailycode.pawon.core.entity.LocalizedInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kris
 * @created Mar 4, 2013
 */
@Repository
public class ItemOptionDAOImpl implements ItemOptionDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public ItemOption save(ItemOption entity) {
        String sql = "INSERT INTO item_option("
                + "active,"
                + "name,"
                + "section,"
                + "price,"
                + "order_number,"
                + "parent_id) "
                + "VALUES(?,?,?,?,?,?)";
        try {
            jdbcTemplate.update(sql, new Object[]{
                entity.isActive(),
                entity.getName(),
                entity.getSection().ordinal(),
                entity.getPrice(),
                entity.getOrderNumber(),
                entity.getParentId()});
            return entity;
        } catch (Exception exception) {
            AppCore.getLogger(this).error(exception);
        }
        return null;
    }

    @Override
    public ItemOption update(ItemOption entity) {
        String sql = "UPDATE item_option SET "
                + "active = ?, "
                + "name = ?, "
                + "price = ?,"
                + "order_number = ?,"
                + "parent_id = ? "
                + "WHERE id = ?";
        try {
            jdbcTemplate.update(sql, new Object[]{
                entity.isActive(),
                entity.getName(),
                entity.getPrice(),
                entity.getOrderNumber(),
                entity.getParentId(),
                entity.getId()});
            return entity;
        } catch (Exception exception) {
            AppCore.getLogger(this).error(exception);
        }
        return null;
    }

    @Override
    public List<ItemOption> getAll() {
        String sql = "SELECT io.* "
                + "FROM item_option io "
                + "WHERE io.status_delete = false "
                + "ORDER BY io.order_number, io.id ASC ";

        return jdbcTemplate.query(sql, new ItemOptionRowMapper());

    }

    @Override
    public ItemOption getById(Long id) {
        String sql = "SELECT io.* "
                + "FROM item_option io "
                + "WHERE io.id = ? AND io.status_delete = false ";
        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{id}, new ItemOptionRowMapper());
        } catch (EmptyResultDataAccessException exception) {
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;
    }

    @Override
    public int count() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Integer count(Long parentId, Section purchaseSection) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<ItemOption> getByParent(Long parentId) {
        String sql = "SELECT io.* "
                + "FROM item_option io "
                + "WHERE io.parent_id = ? "
                + "AND io.status_delete = false "
                + "ORDER BY io.order_number, io.id ASC ";
        return jdbcTemplate.query(sql, new Object[]{parentId}, new ItemOptionRowMapper());
    }

    @Override
    public List<ItemOption> getByParent(Long parentId, boolean isActive) {
        String sql = "SELECT io.* "
                + "FROM item_option io "
                + "WHERE io.parent_id = ? "
                + "AND active = ? "
                + "AND io.status_delete = false "
                + "ORDER BY io.order_number, io.id ASC ";

        return jdbcTemplate.query(sql, new Object[]{parentId, isActive}, new ItemOptionRowMapper());

    }

    @Override
    public List<ItemOption> getByParent(Long parentId, Long languageId, boolean isActive) {
        String sql = "SELECT io.*, li.title, li.description  "
                + "FROM item_option io "
                + "INNER JOIN local_information li ON li.item_option_id = io.id "
                + "WHERE io.parent_id = ? "
                + "AND  li.language_id = ? "
                + "AND active = ? "
                + "AND io.status_delete = false "
                + "ORDER BY io.order_number, io.id ASC ";

        return jdbcTemplate.query(sql, new Object[]{parentId, languageId, isActive}, new ItemOptionRowMapper());

    }

    @Override
    public List<ItemOption> getByParentAndPurchaseSection(Long parentId, Section purchaseSection) {
        String sql = "SELECT io.* "
                + "FROM item_option io "
                + "WHERE io.parent_id = ? AND io.section = ? "
                + "AND io.status_delete = false "
                + "ORDER BY io.order_number, io.id ASC ";
        return jdbcTemplate.query(sql, new Object[]{parentId, purchaseSection.ordinal()}, new ItemOptionRowMapper());

    }

    @Override
    public ItemOption getByIdAndPurchaseSection(Long id, Section purchaseSection) {
        String sql = "SELECT io.* "
                + "FROM item_option io "
                + "WHERE io.id = ? AND section = ? "
                + "AND io.status_delete = false "
                + "ORDER BY io.order_number, io.id ASC ";
        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{id, purchaseSection.ordinal()}, new ItemOptionRowMapper());
        } catch (EmptyResultDataAccessException exception) {
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;
    }

    @Override
    public List<ItemOption> getByPurchaseSection(Section purchaseSection) {
        String sql = "SELECT io.* "
                + "FROM item_option io "
                + "WHERE section = ? AND io.status_delete = false "
                + "ORDER BY io.order_number,io.id ASC ";
        return jdbcTemplate.query(sql, new Object[]{purchaseSection.ordinal()}, new ItemOptionRowMapper());
    }

    @Override
    public List<ItemOption> getByItem(Item item) {
        String sql = "SELECT *FROM item_option io "
                + "INNER JOIN item_2_option i2o ON i2o.item_option_id = io.id "
                + "WHERE i2o.item_id = ? AND io.status_delete = false";
        return jdbcTemplate.query(sql, new Object[]{item.getId()}, new ItemOptionRowMapper());
    }

    @Override
    public List<ItemOption> getByItem(Long itemId, Long languageId) {
        String sql = "SELECT io.*, li.title, li.description "
                + "FROM item_option io "
                + "INNER JOIN item_2_option i2o ON i2o.item_option_id = io.id "
                + "INNER JOIN local_information li ON li.item_option_id = io.id "
                + "WHERE i2o.item_id = ? AND li.language_id = ? AND io.status_delete = false "
                + "ORDER BY io.order_number ";
        return jdbcTemplate.query(sql, new Object[]{itemId, languageId}, new ItemOptionRowMapper());
    }

    @Override
    public ItemOption getById(Long id, Long languageId) {
        String sql = "SELECT io.*, li.title, li.description "
                + "FROM item_option io "
                + "INNER JOIN local_information li ON li.item_option_id = io.id "
                + "WHERE io.id = ? AND li.language_id = ? AND io.status_delete = false ";
        return jdbcTemplate.queryForObject(sql, new Object[]{id, languageId}, new ItemOptionRowMapper());
    }

    @Override
    public boolean delete(Long id) {
        String sql = "UPDATE item_option set status_delete = true WHERE id = ?";
        try {
            jdbcTemplate.update(sql, new Object[]{id});
            return true;
        } catch (Exception exception) {
            AppCore.getLogger(this).error(exception);
        }
        return false;
    }

    @Override
    public void delete(final List<ItemOption> itemOptions) {
        String sql = "UPDATE item_option set status_delete = true WHERE id = ?";
        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setLong(1, itemOptions.get(i).getId());
            }

            @Override
            public int getBatchSize() {
                return itemOptions.size();
            }
        });
    }

    class ItemOptionRowMapper implements RowMapper<ItemOption> {

        @Override
        public ItemOption mapRow(ResultSet rs, int rowNum) throws SQLException {
            ResultSetMetaData rsmd = rs.getMetaData();
            ItemOption option = new ItemOption();
            LocalizedInformation localizedInformation = new LocalizedInformation();

            for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                switch (rsmd.getColumnLabel(i)) {
                    case "active":
                        option.setActive(rs.getBoolean("active"));
                        break;
                    case "id":
                        option.setId(rs.getLong("id"));
                        break;
                    case "name":
                        option.setName(rs.getString("name"));
                        break;
                    case "section":
                        option.setSection(Section.values()[rs.getInt("section")]);
                        break;
                    case "price":
                        option.setPrice(rs.getDouble("price"));
                        break;
                    case "order_number":
                        option.setOrderNumber(rs.getInt("order_number"));
                        break;
                    case "parent_id":
                        option.setParentId(rs.getLong("parent_id"));
                        break;
                    case "title":
                        localizedInformation.setTitle(rs.getString("title"));
                        break;
                    case "description":
                        localizedInformation.setDescription(rs.getString("description"));
                        break;
                }
            }

            option.setLocalizedInformation(localizedInformation);
            return option;
        }
    }
}
