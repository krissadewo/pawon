/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.service;

import org.dailycode.pawon.core.dao.LocalizedInformationDAO;
import org.dailycode.pawon.core.entity.Item;
import org.dailycode.pawon.core.entity.ItemCategory;
import org.dailycode.pawon.core.entity.ItemOption;
import org.dailycode.pawon.core.entity.Language;
import org.dailycode.pawon.core.entity.LocalizedInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author kris
 * @created Feb 21, 2013
 */
@Service
public class LocalizedInformationService   {

    @Autowired
    private LocalizedInformationDAO localizedInformationDAO;

    public LocalizedInformation getByItemCategoryAndLanguage(ItemCategory itemCategory, Language language) {
        return localizedInformationDAO.getByItemCategoryAndLanguage(itemCategory, language);
    }

    public LocalizedInformation getByItemAndLanguage(Item item, Language language) {
        return localizedInformationDAO.getByItemAndLanguage(item, language);
    }

    public LocalizedInformation getByItemOptionAndLanguage(ItemOption itemOption, Language language) {
        return localizedInformationDAO.getByItemOptionAndLanguage(itemOption, language);
    }
}
