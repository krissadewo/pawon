/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.service;

import java.util.Date;
import java.util.List;
import org.dailycode.pawon.core.common.PurchaseStatus;
import org.dailycode.pawon.core.dao.ItemOptionDAO;
import org.dailycode.pawon.core.dao.PurchaseDAO;
import org.dailycode.pawon.core.dao.PurchaseItemDAO;
import org.dailycode.pawon.core.dao.PurchaseOptionDAO;
import org.dailycode.pawon.core.entity.Item;
import org.dailycode.pawon.core.entity.ItemOption;
import org.dailycode.pawon.core.entity.Purchase;
import org.dailycode.pawon.core.entity.PurchaseItem;
import org.dailycode.pawon.core.entity.PurchaseOption;
import org.dailycode.pawon.core.entity.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kris
 * @created Mar 19, 2013
 */
@Service
public class PurchaseService {

    @Autowired
    private PurchaseDAO purchaseDAO;
    @Autowired
    private PurchaseItemDAO purchaseItemDAO;
    @Autowired
    private PurchaseOptionDAO purchaseOptionDAO;
    @Autowired
    private ItemOptionDAO itemOptionDAO;

    /**
     * Save purchase one by one, set status to be ordered
     *
     * @param item
     * @param room
     * @param options
     * @param languageId
     * @return
     */
    @Transactional
    public Purchase saveOrUpdate(Item item, Room room, String[] options, Long languageId) {
        //check purchase on db  
        Purchase purchase = purchaseDAO.getByRoom(room.getId(), PurchaseStatus.ORDERED);
        if (purchase == null) {
            purchase = new Purchase();
            purchase.setDatePurchase(new Date());
            purchase.setRoom(room);
            purchase.setPurchaseStatus(PurchaseStatus.ORDERED);
            purchase = purchaseDAO.save(purchase);
        }

        PurchaseItem purchaseItem = new PurchaseItem();
        purchaseItem.setItem(item);
        purchaseItem.setName(item.getLocalizedInformation().getTitle());
        purchaseItem.setPrice(item.getPrice());
        purchaseItem.setDiscount(item.getDiscount());
        purchaseItem.setPurchase(purchase);
        purchaseItem = purchaseItemDAO.save(purchaseItem);

        if (!options[0].equals("-1")) {
            for (String optionId : options) {
                ItemOption itemOption = itemOptionDAO.getById(Long.valueOf(optionId), languageId);
                PurchaseOption purchaseOption = new PurchaseOption();
                purchaseOption.setName(itemOption.getLocalizedInformation().getTitle());
                purchaseOption.setItemOption(itemOption);
                purchaseOption.setPrice(itemOption.getPrice());
                purchaseOption.setPurchaseItem(purchaseItem);
                purchaseOptionDAO.save(purchaseOption);
            }
        }

        return purchase;
    }

    @Transactional
    public boolean delete(Long purchaseId) {
        purchaseOptionDAO.deleteByPurchaseItem(purchaseItemDAO.getByPurchase(purchaseId).get(0).getId());
        purchaseItemDAO.deleteByPurchase(purchaseId);
        return purchaseDAO.delete(purchaseId);

    }

    /**
     * Update purchase with status process
     *
     * @param purchase
     * @return
     */
    @Transactional
    public Purchase saveOrUpdate(Purchase purchase) {
        return purchaseDAO.update(purchase);
    }

    public Purchase getByRoom(Long roomId, PurchaseStatus purchaseStatus) {
        return purchaseDAO.getByRoom(roomId, purchaseStatus);
    }

    public Purchase getByRoom(String roomName, PurchaseStatus purchaseStatus) {
        return purchaseDAO.getByRoom(roomName, purchaseStatus);
    }

    public List<Purchase> getActivePurchase(PurchaseStatus purchaseStatus) {
        return purchaseDAO.getActivePurchases(purchaseStatus);
    }

    public List<Purchase> search(Integer start, Integer end, Date startDate, Date endDate) {
        return purchaseDAO.search(start, end, startDate, endDate);
    }

    public List<Purchase> search(Integer start, Integer end, Date startDate, Date endDate, String roomName, String orderNumber) {
        return purchaseDAO.search(start, end, startDate, endDate, roomName, orderNumber);
    }

    public int count(Date startDate, Date endDate) {
        return purchaseDAO.count(startDate, endDate);
    }

    public int count(Date startDate, Date endDate, String roomName, String orderNumber) {
        return purchaseDAO.count(startDate, endDate, roomName, orderNumber);
    }

    public int count() {
        return purchaseDAO.count();
    }

    public List<Purchase> getAll(Integer start, Integer end, String sortField, String orderBy) {
        return purchaseDAO.getAll(start, end, sortField, orderBy);
    }
}
