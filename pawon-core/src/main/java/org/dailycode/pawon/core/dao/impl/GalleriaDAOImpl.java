/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.dailycode.pawon.core.AppCore;
import org.dailycode.pawon.core.dao.GalleriaDAO;
import org.dailycode.pawon.core.entity.Galleria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kris
 */
@Repository
public class GalleriaDAOImpl implements GalleriaDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Galleria save(Galleria entity) {
        String sql = "INSERT INTO galleria("
                + "active,"
                + "name,"
                + "image,"
                + "interv) "
                + "VALUES(?,?,?,?)";
        try {
            jdbcTemplate.update(sql, new Object[]{
                entity.isActive(),
                entity.getName(),
                entity.getImage(),
                entity.getInterval()});
            return entity;
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;

    }

    @Override
    public Galleria update(Galleria entity) {
        String sql = "UPDATE galleria SET "
                + "active = ?,"
                + "name = ?,"
                + "image = ?,"
                + "interv = ? "
                + "WHERE id = ? ";
        try {
            jdbcTemplate.update(sql, new Object[]{
                entity.isActive(),
                entity.getName(),
                entity.getImage(),
                entity.getInterval(),
                entity.getId()});
            return entity;
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;
    }

    @Override
    public boolean delete(Long id) {
        String sql = "UPDATE galleria "
                + "SET status_delete  = ? "
                + "WHERE id = ? ";
        try {
            jdbcTemplate.update(sql, new Object[]{true, id});
            return true;
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return false;
    }

    @Override
    public List<Galleria> getAll() {
        String sql = "SELECT *FROM galleria WHERE status_delete = false";
        return jdbcTemplate.query(sql, new PromoRowMapper());
    }

    @Override
    public List<Galleria> getAllActive() {
        String sql = "SELECT *FROM galleria WHERE status_delete = false AND active = true";
        return jdbcTemplate.query(sql, new PromoRowMapper());
    }

    @Override
    public Galleria getById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int count() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    class PromoRowMapper implements RowMapper<Galleria> {

        @Override
        public Galleria mapRow(ResultSet rs, int rowNum) throws SQLException {
            Galleria galleria = new Galleria();
            galleria.setId(rs.getLong("id"));
            galleria.setActive(rs.getBoolean("active"));
            galleria.setName(rs.getString("name"));
            galleria.setImage(rs.getString("image"));
            galleria.setStatusDelete(rs.getBoolean("status_delete"));
            galleria.setInterval(rs.getInt("interv"));
            return galleria;
        }
    }
}
