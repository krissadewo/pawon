package org.dailycode.pawon.core.entity;

import java.io.Serializable;

/**
 *
 * @author kris
 */
public class Authority implements Serializable {

    private Integer id;
    private String role;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Authorities{" + "id=" + id + ", role=" + role + '}';
    }
}
