/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.dailycode.pawon.core.AppCore;
import org.dailycode.pawon.core.dao.TickerDAO;
import org.dailycode.pawon.core.entity.Ticker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kris
 */
@Repository
public class TickerDAOImpl implements TickerDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Ticker save(Ticker entity) {
        String sql = "INSERT INTO ticker(message) VALUES(?)";
        try {
            jdbcTemplate.update(sql, new Object[]{entity.getMessage()});
            return entity;
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;
    }

    @Override
    public Ticker update(Ticker entity) {
        String sql = "UPDATE ticker SET message = ? WHERE id = ?";
        try {
            jdbcTemplate.update(sql, new Object[]{entity.getMessage(), entity.getId()});
            return entity;
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return null;
    }

    @Override
    public boolean delete(Long id) {
        String sql = "DELETE FROM ticker WHERE id = ?";
        try {
            jdbcTemplate.update(sql, new Object[]{id});
            return true;
        } catch (DataAccessException dae) {
            AppCore.getLogger(this).error(dae);
        }
        return false;
    }

    @Override
    public List<Ticker> getAll() {
        String sql = "SELECT *FROM ticker";
        return jdbcTemplate.query(sql, new TickerRowMapper());
    }

    @Override
    public Ticker getById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int count() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    class TickerRowMapper implements RowMapper<Ticker> {

        @Override
        public Ticker mapRow(ResultSet rs, int rowNum) throws SQLException {
            Ticker ticker = new Ticker();
            ticker.setId(rs.getLong("id"));
            ticker.setMessage(rs.getString("message"));
            return ticker;
        }
    }
}
