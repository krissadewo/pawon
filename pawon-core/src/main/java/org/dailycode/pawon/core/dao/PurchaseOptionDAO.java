/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao;

import java.util.List;
import org.dailycode.pawon.core.entity.PurchaseItem;
import org.dailycode.pawon.core.entity.PurchaseOption;

/**
 *
 * @author kris
 * @created Mar 19, 2013
 */
public interface PurchaseOptionDAO extends BaseDAO<PurchaseOption> {

    List<PurchaseOption> getByPurchaseItem(Long id);

    void deleteByPurchaseItem(Long purchaseItemId);
}
