/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao;

import java.util.List;
import org.dailycode.pawon.core.entity.Language;

/**
 *
 * @author kris
 * @created Feb 12, 2013
 */
public interface LanguageDAO extends BaseDAO<Language> {

    public List<Language> getActiveLanguage();

    public void updateDefaultLanguage(List<Language> languages);
}
