/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.core.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.dailycode.pawon.core.common.PurchaseStatus;
import org.dailycode.pawon.core.dao.ReportDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kris
 */
@Repository
public class ReportDAOImpl implements ReportDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List getPurchaseReport(Date startDate, Date endDate) {
        String sql = "SELECT "
                + "SUM(pi.price-(pi.discount/100*pi.price)) AS total_purchase_item,"
                + "SUM(po.price) AS total_purchase_option,"
                + "IFNULL(DATE_FORMAT(p.date_purchase,'%d/%m/%Y'),'') AS date_purchase "
                + "FROM purchase p  "
                + "INNER JOIN purchase_item pi ON pi.purchase_id = p.id "
                + "LEFT JOIN purchase_option po ON po.purchase_item_id = pi.id "
                + "WHERE p.date_purchase BETWEEN ? AND ? AND p.status = " + PurchaseStatus.COMPLETE.ordinal() + " "
                + "GROUP BY p.date_purchase";

        return jdbcTemplate.queryForList(sql, new Object[]{startDate, endDate});
    }
}
