package org.dailycode.pawon.core.entity;

import org.dailycode.pawon.core.common.Section;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author kris
 */
public class ItemObject implements Serializable {

    private Long id;
    private String code;
    private String name;
    private String image;
    private String logo;
    private byte[] imageData;
    private byte[] logoData;
    private boolean active = true;
    private Section section;
    private int orderNumber;
    private List<LocalizedInformation> localizedInformations = new ArrayList<>();
    private LocalizedInformation localizedInformation;
    private String source;
    private boolean statusDelete;

    public ItemObject(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public ItemObject() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public byte[] getImageData() {
        return imageData;
    }

    public void setImageData(byte[] imageData) {
        this.imageData = imageData;
    }

    public byte[] getLogoData() {
        return logoData;
    }

    public void setLogoData(byte[] logoData) {
        this.logoData = logoData;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public List<LocalizedInformation> getLocalizedInformations() {
        return localizedInformations;
    }

    public void setLocalizedInformations(List<LocalizedInformation> localizedInformations) {
        this.localizedInformations = localizedInformations;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public LocalizedInformation getLocalizedInformation() {
        return localizedInformation;
    }

    public void setLocalizedInformation(LocalizedInformation localizedInformation) {
        this.localizedInformation = localizedInformation;
    }

    public boolean isStatusDelete() {
        return statusDelete;
    }

    public void setStatusDelete(boolean statusDelete) {
        this.statusDelete = statusDelete;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ItemObject other = (ItemObject) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.image, other.image)) {
            return false;
        }
        if (!Objects.equals(this.logo, other.logo)) {
            return false;
        }
        if (!Arrays.equals(this.imageData, other.imageData)) {
            return false;
        }
        if (!Arrays.equals(this.logoData, other.logoData)) {
            return false;
        }
        if (this.active != other.active) {
            return false;
        }
        if (this.section != other.section) {
            return false;
        }
        if (this.orderNumber != other.orderNumber) {
            return false;
        }
        if (!Objects.equals(this.localizedInformations, other.localizedInformations)) {
            return false;
        }
        if (!Objects.equals(this.source, other.source)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ItemObject{" + "id=" + id + ", code=" + code + ", name=" + name + ", image=" + image + ", logo=" + logo + ", imageData=" + imageData + ", logoData=" + logoData + ", active=" + active + ", purchaseSection=" + section + ", orderNumber=" + orderNumber + ", localizedInformations=" + localizedInformations + ", source=" + source + '}';
    }
}
