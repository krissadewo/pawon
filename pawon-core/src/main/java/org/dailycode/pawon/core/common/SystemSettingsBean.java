package org.dailycode.pawon.core.common;

/**
 * Before was used as system setting form backend bean.
 */
@Deprecated
public class SystemSettingsBean {

	protected boolean postMiniBarOrderToNC = true;
	protected boolean postMiniBarOrderToPms = false;
	protected boolean postMiniBarOrderToPrinter = false;
	protected boolean postRoomOrderToNC = true;
	protected boolean postRoomOrderToPms = false;
	protected boolean postRoomOrderToPrinter = false;
	protected boolean postUAPurchaseToNC = true;
	protected boolean postUAPurchaseToPms = true;
	protected boolean postUAPurchaseToPrinter = false;
	protected boolean postBookingRequestToNC = true;
	protected boolean postBookingRequestToPms = false;
	protected boolean postBookingRequestToPrinter = false;
	
	/**
	 * @return the postMiniBarOrderToNC
	 */
	public boolean isPostMiniBarOrderToNC() {
		return postMiniBarOrderToNC;
	}
	/**
	 * @param postMiniBarOrderToNC the postMiniBarOrderToNC to set
	 */
	public void setPostMiniBarOrderToNC(boolean postMiniBarOrderToNC) {
		this.postMiniBarOrderToNC = postMiniBarOrderToNC;
	}
	/**
	 * @return the postMiniBarOrderToPms
	 */
	public boolean isPostMiniBarOrderToPms() {
		return postMiniBarOrderToPms;
	}
	/**
	 * @param postMiniBarOrderToPms the postMiniBarOrderToPms to set
	 */
	public void setPostMiniBarOrderToPms(boolean postMiniBarOrderToPms) {
		this.postMiniBarOrderToPms = postMiniBarOrderToPms;
	}
	/**
	 * @return the postMiniBarOrderToPrinter
	 */
	public boolean isPostMiniBarOrderToPrinter() {
		return postMiniBarOrderToPrinter;
	}
	/**
	 * @param postMiniBarOrderToPrinter the postMiniBarOrderToPrinter to set
	 */
	public void setPostMiniBarOrderToPrinter(boolean postMiniBarOrderToPrinter) {
		this.postMiniBarOrderToPrinter = postMiniBarOrderToPrinter;
	}
	/**
	 * @return the postRoomOrderToNC
	 */
	public boolean isPostRoomOrderToNC() {
		return postRoomOrderToNC;
	}
	/**
	 * @param postRoomOrderToNC the postRoomOrderToNC to set
	 */
	public void setPostRoomOrderToNC(boolean postRoomOrderToNC) {
		this.postRoomOrderToNC = postRoomOrderToNC;
	}
	/**
	 * @return the postRoomOrderToPms
	 */
	public boolean isPostRoomOrderToPms() {
		return postRoomOrderToPms;
	}
	/**
	 * @param postRoomOrderToPms the postRoomOrderToPms to set
	 */
	public void setPostRoomOrderToPms(boolean postRoomOrderToPms) {
		this.postRoomOrderToPms = postRoomOrderToPms;
	}
	/**
	 * @return the postRoomOrderToPrinter
	 */
	public boolean isPostRoomOrderToPrinter() {
		return postRoomOrderToPrinter;
	}
	/**
	 * @param postRoomOrderToPrinter the postRoomOrderToPrinter to set
	 */
	public void setPostRoomOrderToPrinter(boolean postRoomOrderToPrinter) {
		this.postRoomOrderToPrinter = postRoomOrderToPrinter;
	}
	/**
	 * @return the postUAPurchaseToNC
	 */
	public boolean isPostUAPurchaseToNC() {
		return postUAPurchaseToNC;
	}
	/**
	 * @param postUAPurchaseToNC the postUAPurchaseToNC to set
	 */
	public void setPostUAPurchaseToNC(boolean postUAPurchaseToNC) {
		this.postUAPurchaseToNC = postUAPurchaseToNC;
	}
	/**
	 * @return the postUAPurchaseToPms
	 */
	public boolean isPostUAPurchaseToPms() {
		return postUAPurchaseToPms;
	}
	/**
	 * @param postUAPurchaseToPms the postUAPurchaseToPms to set
	 */
	public void setPostUAPurchaseToPms(boolean postUAPurchaseToPms) {
		this.postUAPurchaseToPms = postUAPurchaseToPms;
	}
	/**
	 * @return the postUAPurchaseToPrinter
	 */
	public boolean isPostUAPurchaseToPrinter() {
		return postUAPurchaseToPrinter;
	}
	/**
	 * @param postUAPurchaseToPrinter the postUAPurchaseToPrinter to set
	 */
	public void setPostUAPurchaseToPrinter(boolean postUAPurchaseToPrinter) {
		this.postUAPurchaseToPrinter = postUAPurchaseToPrinter;
	}
	/**
	 * @return the postBookingRequestToNC
	 */
	public boolean isPostBookingRequestToNC() {
		return postBookingRequestToNC;
	}
	/**
	 * @param postBookingRequestToNC the postBookingRequestToNC to set
	 */
	public void setPostBookingRequestToNC(boolean postBookingRequestToNC) {
		this.postBookingRequestToNC = postBookingRequestToNC;
	}
	/**
	 * @return the postBookingRequestToPms
	 */
	public boolean isPostBookingRequestToPms() {
		return postBookingRequestToPms;
	}
	/**
	 * @param postBookingRequestToPms the postBookingRequestToPms to set
	 */
	public void setPostBookingRequestToPms(boolean postBookingRequestToPms) {
		this.postBookingRequestToPms = postBookingRequestToPms;
	}
	/**
	 * @return the postBookingRequestToPrinter
	 */
	public boolean isPostBookingRequestToPrinter() {
		return postBookingRequestToPrinter;
	}
	/**
	 * @param postBookingRequestToPrinter the postBookingRequestToPrinter to set 
	 */
	public void setPostBookingRequestToPrinter(boolean postBookingRequestToPrinter) {
		this.postBookingRequestToPrinter = postBookingRequestToPrinter;
	}
	
	
}
