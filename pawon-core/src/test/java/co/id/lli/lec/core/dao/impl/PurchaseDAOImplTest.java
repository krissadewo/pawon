/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.id.lli.lec.core.dao.impl;

import org.dailycode.pawon.core.dao.PurchaseDAO;
import org.dailycode.pawon.core.dao.PurchaseItemDAO;
import org.dailycode.pawon.core.entity.PurchaseItem;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author kris
 * @created Mar 20, 2013
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class PurchaseDAOImplTest {

    @Autowired
    private PurchaseDAO purchaseDAO;
    @Autowired
    private PurchaseItemDAO purchaseItemDAO;

    //@Test
    public void getActivePurchase() {
//        System.out.println(purchaseDAO.getActivePurchases());
    }

    //@Test
    public void getPurchaseItemByPurchase() {
        System.out.println(purchaseItemDAO.getByPurchase(7l));
    }

    @Test
    public void getPurchaseByRoom() {
//        System.out.println(purchaseDAO.getByRoom(1l,false));
    }

    //@Test
    public void getPurchaseById() {
        System.out.println(purchaseItemDAO.getById(1l));
    }
}
