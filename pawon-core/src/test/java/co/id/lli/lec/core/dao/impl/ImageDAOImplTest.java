/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package co.id.lli.lec.core.dao.impl;

import org.dailycode.pawon.core.dao.ImageDAO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author kris
 * @created Feb 23, 2013
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class ImageDAOImplTest {

    @Autowired
    private ImageDAO imageDAO;
    
    @Test
    public void getAll(){
        System.out.println(imageDAO.getAll());
    }
}
