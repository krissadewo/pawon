/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.id.lli.lec.core.dao.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import org.dailycode.pawon.core.dao.ReportDAO;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author kris
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class ReportDAOImplTest {

    @Autowired
    private ReportDAO reportDAO;

    @Test
    public void getPurchaseReport() {
        for (Object object : reportDAO.getPurchaseReport(new DateTime().minusDays(31).toDate(), new Date())) {
            Map map = (Map) object;

            Double priceOption = 0d;
            if (map.get("total_purchase_option") != null) {
                priceOption = ((BigDecimal) map.get("total_purchase_option")).doubleValue();
            }

            Number date = Integer.valueOf(map.get("date_purchase").toString().substring(0, 2));
            Number month = Integer.valueOf(map.get("date_purchase").toString().substring(3, 5));

            System.out.println(month);

            System.out.println((Double) map.get("total_purchase_item") + priceOption + " : " + map.get("date_purchase"));
        }
    }
}
