/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.id.lli.lec.core.dao.service;

import java.util.ArrayList;
import java.util.List;
import org.dailycode.pawon.core.common.Section;
import org.dailycode.pawon.core.dao.service.ItemService;
import org.dailycode.pawon.core.entity.Item;
import org.dailycode.pawon.core.entity.ItemCategory;
import org.dailycode.pawon.core.entity.LocalizedInformation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author kris
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class ItemServiceTest {

    @Autowired
    private ItemService itemService;

    @Test
    public void save() {
        Item item = new Item();
        item.setName("test");
        item.setSection(Section.RESTAURANT);
        item.setPrice(20000d);
        ItemCategory category = new ItemCategory();
        category.setId(2l);
        item.setItemCategory(category);
        
        LocalizedInformation information  = new LocalizedInformation();
        information.setId(1l);
        List<LocalizedInformation> localizedInformations = new ArrayList<>();
        localizedInformations.add(information);
        item.setLocalizedInformations(localizedInformations);
        
        itemService.saveOrUpdate(item);
    }
}
