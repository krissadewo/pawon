/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.id.lli.lec.core.dao.impl;

import org.dailycode.pawon.core.common.Section;
import org.dailycode.pawon.core.dao.ImageDAO;
import org.dailycode.pawon.core.dao.ItemDAO;
import org.dailycode.pawon.core.entity.Item;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author kris
 * @created Feb 23, 2013
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class ItemDAOImplTest {

    @Autowired
    private ItemDAO itemDAO;

    //@Test
    public void getAll() {
        for (Item item : itemDAO.getBySection(Section.RESTAURANT, 0, 10, "name", "desc".toString())) {
            System.out.println(item.getId() + " : " + item.getName() + " : " + item.getPrice());
        }
    }

    //@Test
    public void getByCategoryAndLanguage() {
        for (Item item : itemDAO.getByCategory(1l, 1l)) {
            System.out.println(item.getId() + " : " + item.getName() + " : " + item.getPrice());
        }
    }

    @Test
    public void getById() {
        System.out.println(itemDAO.getById(2988l));
    }
}
