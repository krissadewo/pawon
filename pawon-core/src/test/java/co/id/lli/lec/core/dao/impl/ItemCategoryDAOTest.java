/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.id.lli.lec.core.dao.impl;

import org.dailycode.pawon.core.dao.ItemCategoryDAO;
import org.dailycode.pawon.core.entity.ItemCategory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author kris
 * @created Feb 13, 2013
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class ItemCategoryDAOTest {

    @Autowired
    private ItemCategoryDAO categoryDAO;

    @Test
    public void getAll() {
        for (ItemCategory itemCategory : categoryDAO.getAll()) {
//            StringBuilder sb = new StringBuilder();
//            String x = getParent(itemCategory.getId(), sb);
//            System.out.println(x.substring(0, x.length() - 2));
            System.out.println(itemCategory.getImage().toString());
        }
    }

    private String getParent(Long parentId, StringBuilder sb) {
        ItemCategory itemCategory = categoryDAO.getById(parentId);
        if (itemCategory != null) {
            getParent(itemCategory.getParentId(), sb);
            sb.append(itemCategory.getName());
            sb.append(" > ");
            return sb.toString();
        }
        return null;
    }
}
