/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.id.lli.lec.core.dao.service;

import org.dailycode.pawon.core.dao.service.ItemCategoryService;
import org.dailycode.pawon.core.common.Section;
import org.dailycode.pawon.core.entity.ItemCategory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author kris
 * @created Feb 13, 2013
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class ItemCategoryServiceTest {

    @Autowired
    private ItemCategoryService categoryService;

    //@Test
    public void getOrginizedParent() {
        ItemCategory itemCategory = new ItemCategory();
        itemCategory.setId(13l);
        itemCategory.setSection(Section.RESTAURANT);
        System.out.println(categoryService.getOrganizedParent(itemCategory.getSection(), itemCategory, true).getId());
    }

    @Test
    public void delete() {
        categoryService.delete(3l);
    }
}
