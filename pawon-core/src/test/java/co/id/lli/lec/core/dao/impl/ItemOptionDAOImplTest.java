/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.id.lli.lec.core.dao.impl;

import org.dailycode.pawon.core.dao.ItemOptionDAO;
import org.dailycode.pawon.core.entity.ItemOption;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author kris
 * @created Mar 21, 2013
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class ItemOptionDAOImplTest {

    @Autowired
    private ItemOptionDAO itemOptionDAO;

    //@Test
    public void getByItem() {
        for (ItemOption itemOption : itemOptionDAO.getByItem(4l, 2l)) {
            System.out.println(itemOption.getId() + " : " + itemOption.getName() + " : " + itemOption.getPrice());
        }
    }

    @Test
    public void getById() {
        System.out.println(itemOptionDAO.getById(1l, 1l));
    }
}
