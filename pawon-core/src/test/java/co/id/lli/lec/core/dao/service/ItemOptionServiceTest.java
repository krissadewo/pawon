/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.id.lli.lec.core.dao.service;

import co.id.lli.lec.core.dao.impl.*;
import org.dailycode.pawon.core.common.Section;
import org.dailycode.pawon.core.dao.ImageDAO;
import org.dailycode.pawon.core.dao.ItemDAO;
import org.dailycode.pawon.core.dao.ItemOptionDAO;
import org.dailycode.pawon.core.dao.service.ItemOptionService;
import org.dailycode.pawon.core.dao.service.ItemService;
import org.dailycode.pawon.core.entity.Item;
import org.dailycode.pawon.core.entity.ItemOption;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author kris
 * @created Feb 23, 2013
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class ItemOptionServiceTest {

    @Autowired
    private ItemOptionService ItemOptionService;

    @Test
    public void getByItem() {
        for (ItemOption itemOption : ItemOptionService.getByItem(4l, 2l)) {
            System.out.println(itemOption.getId() + " : " + itemOption.getName() + " : " + itemOption.getPrice());
        }
    }
}
