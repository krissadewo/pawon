/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.id.lli.lec.core.dao.impl;

import org.dailycode.pawon.core.dao.SystemParameterDAO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author kris
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class SystemParameterDAOImplTest {

    @Autowired
    private SystemParameterDAO systemParameterDAO;

    @Test
    public void get() {
        System.out.println(systemParameterDAO.getImageSource() + " : " + systemParameterDAO.getImageURL());
    }
}
