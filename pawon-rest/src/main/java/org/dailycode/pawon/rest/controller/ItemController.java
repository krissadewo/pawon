/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.rest.controller;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.dailycode.pawon.rest.AppRest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author kris
 * @created Mar 14, 2013
 */
@Controller
@RequestMapping("/item")
public class ItemController extends BaseController {

    @RequestMapping(value = "/categoryId/{categoryId}/languageId/{languageId}", method = RequestMethod.GET)
    public Map<String, Object> getByCategory(@PathVariable Long categoryId, @PathVariable Long languageId) {
        return AppRest.model(getItemService().getByCategory(categoryId, languageId), String.valueOf(HttpServletResponse.SC_OK));
    }

    @RequestMapping(value = "/itemId/{itemId}/languageId/{languageId}", method = RequestMethod.GET)
    public Map<String, Object> getById(@PathVariable Long itemId, @PathVariable Long languageId) {
        return AppRest.model(getItemService().getById(itemId, languageId), String.valueOf(HttpServletResponse.SC_OK));
    }

    @RequestMapping(value = "/itemId/{itemId}/options/{options}", method = RequestMethod.GET)
    public Map<String, Object> savePurchase(@PathVariable Long itemId, @PathVariable String[] options) {
        //response.setHeader("Access-Control-Allow-Origin", "*");
        System.out.println(itemId);
        for (String o : options) {
            System.out.println(o);
        }
        return AppRest.model(true, String.valueOf(HttpServletResponse.SC_OK));
    }
}
