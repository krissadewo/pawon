/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.rest.controller;

import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.dailycode.pawon.core.common.Section;
import org.dailycode.pawon.core.dao.service.BaseService;
import org.dailycode.pawon.rest.AppRest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author kris
 * @created Mar 12, 2013
 */
@Controller
public class BaseController extends BaseService {

    @RequestMapping(value = "/language", method = RequestMethod.GET)
    public Map<String, Object> getByLanguage(HttpServletResponse response) {
        return AppRest.model(getLanguageService().getAll(), String.valueOf(HttpServletResponse.SC_OK));
    }
}
